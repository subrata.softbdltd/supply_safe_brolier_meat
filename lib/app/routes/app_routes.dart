part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const SPLASH_SCREEN = _Paths.SPLASH_SCREEN;
  static const ON_BOARD_SCREEN = _Paths.ON_BOARD_SCREEN;
  static const LOGIN_SCREEN = _Paths.LOGIN_SCREEN;
  static const PROFILE = _Paths.PROFILE;
  static const OTP_SCREEN = _Paths.OTP_SCREEN;
  static const FARMER_PROFILE = _Paths.FARMER_PROFILE;
  static const BROILER_DETAILS = _Paths.BROILER_DETAILS;
  static const PLANT_MAIN_NAV = _Paths.PLANT_MAIN_NAV;
  static const PLANT_HOME = _Paths.PLANT_HOME;
  static const PLANT_STOCKS = _Paths.PLANT_STOCKS;
  static const PLANT_REQUEST = _Paths.PLANT_REQUEST;
  static const PLANT_PROFILE = _Paths.PLANT_PROFILE;
  static const PLANT_TRANSATION_HISTORY = _Paths.PLANT_TRANSATION_HISTORY;
  static const PLANT_STOCK_DETAILS = _Paths.PLANT_STOCK_DETAILS;
  static const PACKAGE_LIST = _Paths.PACKAGE_LIST;
  static const PACKAGE_REQUEST = _Paths.PACKAGE_REQUEST;
  static const OUTLET_MAIN_NAV = _Paths.OUTLET_MAIN_NAV;
  static const OUTLET_PROFILE = _Paths.OUTLET_PROFILE;
  static const OUTLET_SEND_PACKAGE_REQUEST = _Paths.OUTLET_SEND_PACKAGE_REQUEST;
  static const PLANT_REQUEST_DETAILS = _Paths.PLANT_REQUEST_DETAILS;
  static const OUTLET_PACKAGE_REQUEST_DETAILS =
      _Paths.OUTLET_PACKAGE_REQUEST_DETAILS;
  static const OUTLET_ORDER_CREATE = _Paths.OUTLET_ORDER_CREATE;
  static const OUTLET_ORDER_CREATE_LIST = _Paths.OUTLET_ORDER_CREATE_LIST;
  static const OUTLET_ORDER_LIST_DETAILS = _Paths.OUTLET_ORDER_LIST_DETAILS;
  static const ORDER_DETAILS = _Paths.ORDER_DETAILS;
  static const FORM_FILUP_SECTION = _Paths.FORM_FILUP_SECTION;
  static const PHOTO_GALLERY = _Paths.PHOTO_GALLERY;
  static const ADD_PHOTOS = _Paths.ADD_PHOTOS;
  static const PHOTO_VIEW = _Paths.PHOTO_VIEW;
  static const NOTIFICATION = _Paths.NOTIFICATION;
  static const NOTIFICATION_DETAILS = _Paths.NOTIFICATION_DETAILS;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const SPLASH_SCREEN = '/splash-screen';
  static const ON_BOARD_SCREEN = '/on-board-screen';
  static const LOGIN_SCREEN = '/login-screen';
  static const PROFILE = '/profile';
  static const OTP_SCREEN = '/otp-screen';
  static const FARMER_PROFILE = '/farmer-profile';
  static const BROILER_DETAILS = '/broiler-details';
  static const PLANT_MAIN_NAV = '/plant-main-nav';
  static const PLANT_HOME = '/plant-home';
  static const PLANT_STOCKS = '/plant-stocks';
  static const PLANT_REQUEST = '/plant-request';
  static const PLANT_PROFILE = '/plant-profile';
  static const PLANT_TRANSATION_HISTORY = '/plant-transation-history';
  static const PLANT_STOCK_DETAILS = '/plant-stock-details';
  static const PACKAGE_LIST = '/package-list';
  static const PACKAGE_REQUEST = '/package-request';
  static const OUTLET_MAIN_NAV = '/outlet-main-nav';
  static const OUTLET_PROFILE = '/outlet-profile';
  static const OUTLET_SEND_PACKAGE_REQUEST = '/outlet-send-package-request';
  static const PLANT_REQUEST_DETAILS = '/plant-request-details';
  static const OUTLET_PACKAGE_REQUEST_DETAILS =
      '/outlet-package-request-details';
  static const OUTLET_ORDER_CREATE = '/outlet-order-create';
  static const OUTLET_ORDER_CREATE_LIST = '/outlet-order-create-list';
  static const OUTLET_ORDER_LIST_DETAILS = '/outlet-order-list-details';
  static const ORDER_DETAILS = '/order-details';
  static const FORM_FILUP_SECTION = '/form-filup-section';
  static const PHOTO_GALLERY = '/photo-gallery';
  static const ADD_PHOTOS = '/add-photos';
  static const PHOTO_VIEW = '/photo-view';
  static const NOTIFICATION = '/notification';
  static const NOTIFICATION_DETAILS = '/notification-details';
}

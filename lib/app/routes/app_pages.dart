import 'package:get/get.dart';

import '../modules/Farmer/broiler_details/broiler_details_binding.dart';
import '../modules/Farmer/broiler_details/broiler_details_view.dart';
import '../modules/Farmer/farmer_profile/farmer_profile_binding.dart';
import '../modules/Farmer/farmer_profile/farmer_profile_view.dart';
import '../modules/Farmer/home/home_binding.dart';
import '../modules/Farmer/home/home_view.dart';
import '../modules/auth/login_screen/login_screen_binding.dart';
import '../modules/auth/login_screen/login_screen_view.dart';
import '../modules/auth/otp_screen/otp_screen_binding.dart';
import '../modules/auth/otp_screen/otp_screen_view.dart';
import '../modules/farmer/form_filup_section/form_filup_section_binding.dart';
import '../modules/farmer/form_filup_section/form_filup_section_view.dart';
import '../modules/notification_section/notification/notification_binding.dart';
import '../modules/notification_section/notification/notification_view.dart';
import '../modules/notification_section/notification_details/notification_details_binding.dart';
import '../modules/notification_section/notification_details/notification_details_view.dart';
import '../modules/on_board_screen/on_board_screen_binding.dart';
import '../modules/on_board_screen/on_board_screen_view.dart';
import '../modules/outlet/create_order/order_details/order_details_binding.dart';
import '../modules/outlet/create_order/order_details/order_details_view.dart';
import '../modules/outlet/create_order/outlet_order_create/outlet_order_create_binding.dart';
import '../modules/outlet/create_order/outlet_order_create/outlet_order_create_view.dart';
import '../modules/outlet/create_order/outlet_order_create_list/outlet_order_create_list_binding.dart';
import '../modules/outlet/create_order/outlet_order_create_list/outlet_order_create_list_view.dart';
import '../modules/outlet/create_order/outlet_order_list_details/outlet_order_list_details_binding.dart';
import '../modules/outlet/create_order/outlet_order_list_details/outlet_order_list_details_view.dart';
import '../modules/outlet/outlet_main_nav/outlet_main_nav_binding.dart';
import '../modules/outlet/outlet_main_nav/outlet_main_nav_view.dart';
import '../modules/outlet/outlet_package_request_details/outlet_package_request_details_binding.dart';
import '../modules/outlet/outlet_package_request_details/outlet_package_request_details_view.dart';
import '../modules/outlet/outlet_profile/outlet_profile_binding.dart';
import '../modules/outlet/outlet_profile/outlet_profile_view.dart';
import '../modules/outlet/outlet_send_package_request/outlet_send_package_request_binding.dart';
import '../modules/outlet/outlet_send_package_request/outlet_send_package_request_view.dart';
import '../modules/outlet/package_list/package_list_binding.dart';
import '../modules/outlet/package_list/package_list_view.dart';
import '../modules/outlet/package_request/package_request_binding.dart';
import '../modules/outlet/package_request/package_request_view.dart';
import '../modules/plant/gallery/add_photos/add_photos_binding.dart';
import '../modules/plant/gallery/add_photos/add_photos_view.dart';
import '../modules/plant/gallery/photo_gallery/photo_gallery_binding.dart';
import '../modules/plant/gallery/photo_gallery/photo_gallery_view.dart';
import '../modules/plant/gallery/photo_view/photo_view_binding.dart';
import '../modules/plant/gallery/photo_view/photo_view_view.dart';
import '../modules/plant/plant_home/plant_home_binding.dart';
import '../modules/plant/plant_home/plant_home_view.dart';
import '../modules/plant/plant_main_nav/plant_main_nav_binding.dart';
import '../modules/plant/plant_main_nav/plant_main_nav_view.dart';
import '../modules/plant/plant_profile/plant_profile_binding.dart';
import '../modules/plant/plant_profile/plant_profile_view.dart';
import '../modules/plant/plant_request/plant_request_binding.dart';
import '../modules/plant/plant_request/plant_request_view.dart';
import '../modules/plant/plant_request_details/plant_request_details_binding.dart';
import '../modules/plant/plant_request_details/plant_request_details_view.dart';
import '../modules/plant/plant_transation_history/plant_transation_history_binding.dart';
import '../modules/plant/plant_transation_history/plant_transation_history_view.dart';
import '../modules/plant/stock/plant_stock_details/plant_stock_details_binding.dart';
import '../modules/plant/stock/plant_stock_details/plant_stock_details_view.dart';
import '../modules/plant/stock/plant_stocks/plant_stocks_binding.dart';
import '../modules/plant/stock/plant_stocks/plant_stocks_view.dart';
import '../modules/splash_screen/splash_screen_binding.dart';
import '../modules/splash_screen/splash_screen_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.SPLASH_SCREEN;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => const HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.SPLASH_SCREEN,
      page: () => const SplashScreenView(),
      binding: SplashScreenBinding(),
    ),
    GetPage(
      name: _Paths.ON_BOARD_SCREEN,
      page: () => const OnBoardScreenView(),
      binding: OnBoardScreenBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN_SCREEN,
      page: () => const LoginScreenView(),
      binding: LoginScreenBinding(),
    ),
    GetPage(
      name: _Paths.OTP_SCREEN,
      page: () => const OtpScreenView(),
      binding: OtpScreenBinding(),
    ),
    GetPage(
      name: _Paths.FARMER_PROFILE,
      page: () => const FarmerProfileView(),
      binding: FarmerProfileBinding(),
    ),
    GetPage(
      name: _Paths.BROILER_DETAILS,
      page: () => const BroilerDetailsView(),
      binding: BroilerDetailsBinding(),
    ),
    GetPage(
      name: _Paths.PLANT_MAIN_NAV,
      page: () => const PlantMainNavView(),
      binding: PlantMainNavBinding(),
    ),
    GetPage(
      name: _Paths.PLANT_HOME,
      page: () => const PlantHomeView(),
      binding: PlantHomeBinding(),
    ),
    GetPage(
      name: _Paths.PLANT_STOCKS,
      page: () => const PlantStocksView(),
      binding: PlantStocksBinding(),
    ),
    GetPage(
      name: _Paths.PLANT_REQUEST,
      page: () => const PlantRequestView(),
      binding: PlantRequestBinding(),
    ),
    GetPage(
      name: _Paths.PLANT_PROFILE,
      page: () => const PlantProfileView(),
      binding: PlantProfileBinding(),
    ),
    GetPage(
      name: _Paths.PLANT_TRANSATION_HISTORY,
      page: () => const PlantTransationHistoryView(),
      binding: PlantTransationHistoryBinding(),
    ),
    GetPage(
      name: _Paths.PLANT_STOCK_DETAILS,
      page: () => const PlantStockDetailsView(),
      binding: PlantStockDetailsBinding(),
    ),
    GetPage(
      name: _Paths.PACKAGE_LIST,
      page: () => const PackageListView(),
      binding: PackageListBinding(),
    ),
    GetPage(
      name: _Paths.PACKAGE_REQUEST,
      page: () => const PackageRequestView(),
      binding: PackageRequestBinding(),
    ),
    GetPage(
      name: _Paths.OUTLET_MAIN_NAV,
      page: () => const OutletMainNavView(),
      binding: OutletMainNavBinding(),
    ),
    GetPage(
      name: _Paths.OUTLET_PROFILE,
      page: () => const OutletProfileView(),
      binding: OutletProfileBinding(),
    ),
    GetPage(
      name: _Paths.OUTLET_SEND_PACKAGE_REQUEST,
      page: () => const OutletSendPackageRequestView(),
      binding: OutletSendPackageRequestBinding(),
    ),
    GetPage(
      name: _Paths.PLANT_REQUEST_DETAILS,
      page: () => const PlantRequestDetailsView(),
      binding: PlantRequestDetailsBinding(),
    ),
    GetPage(
      name: _Paths.OUTLET_PACKAGE_REQUEST_DETAILS,
      page: () => const OutletPackageRequestDetailsView(),
      binding: OutletPackageRequestDetailsBinding(),
    ),
    GetPage(
      name: _Paths.OUTLET_ORDER_CREATE,
      page: () => const OutletOrderCreateView(),
      binding: OutletOrderCreateBinding(),
    ),
    GetPage(
      name: _Paths.OUTLET_ORDER_CREATE_LIST,
      page: () => const OutletOrderCreateListView(),
      binding: OutletOrderCreateListBinding(),
    ),
    GetPage(
      name: _Paths.OUTLET_ORDER_LIST_DETAILS,
      page: () => const OutletOrderListDetailsView(),
      binding: OutletOrderListDetailsBinding(),
    ),
    GetPage(
      name: _Paths.ORDER_DETAILS,
      page: () => const OrderDetailsView(),
      binding: OrderDetailsBinding(),
    ),
    GetPage(
      name: _Paths.FORM_FILUP_SECTION,
      page: () => const FormFilupSectionView(),
      binding: FormFilupSectionBinding(),
    ),
    GetPage(
      name: _Paths.PHOTO_GALLERY,
      page: () => const PhotoGalleryView(),
      binding: PhotoGalleryBinding(),
    ),
    GetPage(
      name: _Paths.ADD_PHOTOS,
      page: () => const AddPhotosView(),
      binding: AddPhotosBinding(),
    ),
    GetPage(
      name: _Paths.PHOTO_VIEW,
      page: () => const PhotoViewView(),
      binding: PhotoViewBinding(),
    ),
    GetPage(
      name: _Paths.NOTIFICATION_DETAILS,
      page: () => const NotificationDetailsView(),
      binding: NotificationDetailsBinding(),
    ),
    GetPage(
      name: _Paths.NOTIFICATION,
      page: () => const NotificationView(),
      binding: NotificationBinding(),
    ),
  ];
}

const String baseUrl = "https://payment.autospirebd.com/api";
const String imageShowUrl = "https://payment.autospirebd.com/storage/";
const String imageUrl = "";

//Profile
const String user = "/users";

const String sendOtpUrl = "$user/sent-otp";
const String otpVerificationUrl = "$user/verify-otp-and-login";
const String getUserDetailsUrl = "$user/get-details";
const String logOutUrl = "$user/log-out";
const String broilerListUrl = "/broiler-batches";
const String galleryUrl = "/gallery";
String broilerDetails(id) => "/broiler-batches/$id";
const String batchInstructionsUrl = "/batch-instructions";
const String packageStockUrl = "/package-stock";
const String stockUpdateDelete = "/package-stock/plant/update";
const String notificationListUrl = "/notices";
String notificationRead(int id) => "/notices/read/$id";
//Plant
const String plantTransactionsUrl = "/broiler-transactions";
String plantStatusUpdateUrl(id) =>
    "/package-request/$id/update-status-by-plant";
 String plantCancelStatusUpdateUrl(id) => "/package-request/$id/cancel-by-outlet";
const String plantTransactionsSummaryUrl =
    "/broiler-transactions/farmer-transactions-summary";

//Outlet:
const String packageRequestUrl = "/package-request";
const String getPlantUrl = "/package-request/get-plants-by-outlet";
const String sendRequestByOutletUrl = "/package-request/sent-by-outlet";
const String createOrderUrl = "/orders";

import 'package:supply_safe_brolier_meat/app/core/constants/asset_constants.dart';

class OnBoardingContent {
  final String image;
  final String description;
  final String bottomImage;

  OnBoardingContent({
    required this.image,
    required this.description,
    required this.bottomImage,
  });
}

List<OnBoardingContent> onBoardingContent = [
  OnBoardingContent(
    image: imageOne,
    // todo add description
    description: "",
    bottomImage: imageOne,
  ),
  OnBoardingContent(
    image: imageOne,
    description: "",
    bottomImage: imageOne,
  ),
  OnBoardingContent(
    image: imageOne,
    description: "",
    bottomImage: imageOne,
  ),
];

// Asset Path
const pngPath = "assets/png/";
const jpgPath = "assets/jpg/";
const svgPath = "assets/svg/";
const jsonPath = "assets/json/";
const lottiePath = "assets/lottie/";

//png
const logo = "assets/png/logo.png";
const chickenOneImage = "assets/png/chicken_one.png";
const chickenTwoImage = "assets/png/chicken_two.png";
const chickenThreeImage = "assets/png/chicken_three.png";
const dummyImage = "assets/png/dummy.png";
const historyImage = "assets/png/history.png";
const noImage = "assets/png/noImage.png";
const noDataFound = "assets/png/noDataFound.png";
const farmIcon = "assets/png/farmIcon.png";

//Plant PNG
const homeImage = "assets/png/home.png";
const transImage = "assets/png/trans.png";
const reqImage = "assets/png/req.png";
const userImage = "assets/png/user.png";
const transHisImage = "assets/png/transHis.png";
const selectedHomeImage = "assets/png/selectedHomeImage.png";
const selectedTransImage = "assets/png/selectedTransImage.png";
const selectedReqImage = "assets/png/selectedReqImage.png";
const selectedUserImage = "assets/png/selectedUserImage.png";
const shopImage = "assets/png/shop.png";
const String noDataFoundLottie = "${lottiePath}no_data_found.json";

//Outlet Png:
const capIcon = "${pngPath}cap_icon.png";
const homeIcon = "${pngPath}home_icon.png";
const jobIcon = "${pngPath}job_icon.png";
const settingIcon = "${pngPath}setting_icon.png";
const packageIcon = "${pngPath}package.png";
const photoIcon = "${pngPath}photo.png";
const orderImage = "${pngPath}order.png";
const outletImage = "${pngPath}outlet.png";
const iconOrderImage = "${pngPath}iconOrder.png";
const productImage = "${pngPath}product.png";

//All Lottie
const imageOne = "${lottiePath}imageOne.json";
const imageTwo = "${lottiePath}imageTwo.json";
const imageThree = "${lottiePath}imageThree.json";
const otpImage = "${lottiePath}otp.json";
const noDataFoundImage = "${lottiePath}noDataFound.json";

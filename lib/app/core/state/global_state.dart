import 'dart:developer';

import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_constants.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/Response_model.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/user_details_model.dart'
    as user_details;
import 'package:supply_safe_brolier_meat/app/network/api_client.dart';
import 'package:supply_safe_brolier_meat/app/routes/app_pages.dart';
import '../../network/api_url.dart';

class GlobalState extends GetxController {
  final userDetailsData = user_details.Data().obs;
  final isLoggedIn = false.obs;

  @override
  void onInit() async{
    super.onInit();
    await appHelper.getToken().then((value) => {
      logger.d("Token: ${value}"),
      log("Token: ${value}"),
    });
    getUserData();
  }

  getUserData() async {
    await appHelper.getToken().then((token) async {
      if (token != null && token != "") {
        isLoggedIn.value = true;
        var response = await ApiClient().getAPI(getUserDetailsUrl, getUserData);
        if (response != null) {
          user_details.UserDetailsModel userDetailsModel =
              user_details.userDetailsModelFromJson(response.toString());
          if (userDetailsModel.responseStatus?.success ?? false) {
            userDetailsData.value =
                userDetailsModel.data ?? user_details.Data();
            await appHelper
                .subscribeFirebaseTopic("${userDetailsData.value.phone}");
            logger.d("GlobalTopic ${appHelper
                .subscribeFirebaseTopic("${userDetailsData.value.phone}")}");
            logger.d("runtype ${userDetailsData.value.phone.runtimeType}");
          } else {
            appWidgets.showSimpleToast(userDetailsModel.responseStatus?.message,
                isError: true);
          }
        }
      } else {
        isLoggedIn.value = false;
      }
    });
  }

  logoutSection() async {
    var data = <String, dynamic>{};

    var response = await ApiClient().putAPI(logOutUrl, data, logoutSection);

    if (response != null) {
      ResponseModel responseModel = responseModelFromJson(response.toString());
      if (responseModel.responseStatus?.success ?? false) {
        appHelper.logout();


        Get.offAllNamed(Routes.LOGIN_SCREEN);
        appWidgets.showSimpleToast(responseModel.responseStatus?.message,
            isSuccess: true);
        await appHelper.unSubscribeFirebaseTopic("farmer");
        await appHelper.unSubscribeFirebaseTopic("plant");
        await appHelper.unSubscribeFirebaseTopic("outlet");
        await appHelper
            .subscribeFirebaseTopic(userDetailsData.value.phone ?? "");
      } else {
        appWidgets.showSimpleToast(responseModel.responseStatus?.message,
            isError: true);
      }
    }
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:soft_builder/constraints/my_colors.dart';

import 'package:soft_builder/constraints/my_text_style.dart';

class DrawerItem extends StatelessWidget {
  final dynamic onClick;
  final String imageAsset, text;
  final IconData icon;
  final double height;

  const DrawerItem(
    this.onClick, {
    Key? key,
    this.imageAsset = "",
    this.icon = Icons.person,
    this.text = "",
    this.height = 30,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Get.back(); // to close drawer
        onClick();
      },
      child: ListTile(
        leading: Icon(
          icon,
          color: Colors.black,
          size: height,
        ),
        title: Text(
          text,
          style: text16Style(
            color: MyColors.black,
            fontWeight: FontWeight.w600,
          ),
        ),
        titleAlignment: ListTileTitleAlignment.center,
        trailing: Icon(
          Icons.arrow_forward_ios,
          size: 20.h,
        ),
      ),
    );
  }
}

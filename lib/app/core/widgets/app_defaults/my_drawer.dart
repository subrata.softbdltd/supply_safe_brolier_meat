import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:soft_builder/constraints/my_colors.dart';
import 'package:soft_builder/soft_builder.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_colors.dart';
import 'package:supply_safe_brolier_meat/app/core/state/global_state.dart';
import 'package:supply_safe_brolier_meat/app/core/widgets/app_button.dart';
import 'package:supply_safe_brolier_meat/app/core/widgets/app_circle_network_image_viewer.dart';
import 'package:supply_safe_brolier_meat/app/core/widgets/app_defaults/my_drawer_item.dart';

import '../../../routes/app_pages.dart';
import '../../constants/app_constants.dart';
import '../../constants/asset_constants.dart';

class AppDrawer extends StatefulWidget {
  final String userName;
  final String userImage;

  const AppDrawer({
    Key? key,
    required this.userName,
    required this.userImage,
  }) : super(key: key);

  @override
  State<AppDrawer> createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: MyColors.background,
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          Container(
            padding: EdgeInsets.only(
              left: mainPaddingW,
              top: mainPaddingH * 3,
              bottom: mainPaddingH,
            ),
            color: const Color(0xff227B94),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(100.w),
                  child: AppCircleNetworkImageViewer(
                    widget.userImage,
                    80,
                    assetImage: dummyImage,
                  ),
                ),
                gapH12,
                Text(
                  widget.userName,
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  style: text18Style(
                    color: MyColors.white,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ],
            ),
          ),
          DrawerItem(
            () {
              Get.toNamed(Routes.HOME);
            },
            text: "হোম",
            icon: Icons.home,
          ),
          DrawerItem(
            () {
              Get.toNamed(Routes.FARMER_PROFILE);
            },
            text: "প্রোফাইল",
            icon: Icons.person,
          ),
          gapH20,
          Padding(
            padding: mainPaddingAll,
            child: AppButton(
              text: "লগআউট",
              textColor: AppColor.white,
              backgroundColor: MyColors.primaryBlueColor,
              onPressed: () {
                Get.find<GlobalState>().logoutSection();
              },
            ),
          )
        ],
      ),
    );
  }
}

// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
//
// import '../../constants/app_colors.dart';
//
// class AppDrawer extends StatelessWidget {
//   const AppDrawer({super.key});
//
//   @override
//   Widget build(BuildContext context) {
//     return Drawer(
//       width: 300,
//       shape: const RoundedRectangleBorder(
//         borderRadius: BorderRadius.only(
//           topRight: Radius.circular(0),
//           bottomRight: Radius.circular(0),
//         ),
//       ),
//       child: Container(
//         decoration: const BoxDecoration(
//           gradient: AppColor.drawerBackgroundGradient,
//         ),
//         child: Obx(() {
//           return ListView(
//             padding: EdgeInsets.zero,
//             children: [],
//           );
//         }),
//       ),
//     );
//   }
//
//   appDivider() {
//     return const Divider(
//       height: 30,
//       color: AppColor.white,
//     );
//   }
// }

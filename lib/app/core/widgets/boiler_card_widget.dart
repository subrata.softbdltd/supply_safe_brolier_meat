import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../constants/app_colors.dart';
import '../constants/app_constants.dart';
import '../constants/asset_constants.dart';
import '../constants/my_text_style.dart';

class BoilerCardWidget extends StatelessWidget {
  final String batchNo;
  final String startDate;
  final String dayCount;
  final String totalChicken;
  final String totalGoodBroiler;
  final String totalDamageBroiler;
  final Function() onTap;
  const BoilerCardWidget(
      {super.key,
      required this.batchNo,
      required this.startDate,
      required this.dayCount,
      required this.totalChicken,
      required this.totalGoodBroiler,
      required this.totalDamageBroiler,
      required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: defaultRadius,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.4),
            spreadRadius: 0.5,
            blurRadius: 1,
            offset: const Offset(0, 0.5), // changes position of shadow
          ),
        ],
      ),
      child: Material(
        borderRadius: defaultRadius,
        child: InkWell(
          borderRadius: defaultRadius,
          onTap: onTap,
          child: Ink(
            decoration: BoxDecoration(
              color: const Color(0xffAFD198).withOpacity(0.5),
              borderRadius: defaultRadius,
            ),
            padding: EdgeInsets.all(20.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "ব্যাচ নাম্বার: $batchNo" ?? "",
                  style: text20Style(fontWeight: FontWeight.w600),
                ),
                Text(
                  "শুরুর তারিখ: $startDate",
                  style: text14Style(),
                ),
                Text(
                  dayCount,
                  style: text14Style(),
                ),
                appWidgets.divider(
                  color: AppColor.gray.withOpacity(0.3),
                ),
                gapH12,
                Row(
                  children: [
                    chickenCountWidget(
                      count: totalChicken,
                      imageName: chickenOneImage,
                      borderColor: AppColor.gray,
                      imageColor: AppColor.gray,
                    ),
                    gapW8,
                    chickenCountWidget(
                      count: totalDamageBroiler,
                      imageName: chickenOneImage,
                      borderColor: AppColor.red,
                      imageColor: AppColor.red,
                    ),
                    gapW8,
                    chickenCountWidget(
                      count: totalGoodBroiler,
                      imageName: chickenOneImage,
                      borderColor: Colors.blue,
                      imageColor: Colors.blue,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget chickenCountWidget({
    required String count,
    required String imageName,
    required Color borderColor,
    required Color imageColor,
  }) {
    return Expanded(
      child: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(8.w),
        decoration: BoxDecoration(
          borderRadius: defaultBorder,
          border: Border.all(color: borderColor),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              imageName,
              height: 20,
              color: imageColor,
            ),
            gapW3,
            Text(
              count,
              style: text14Style(fontWeight: FontWeight.w500),
            )
          ],
        ),
      ),
    );
  }
}

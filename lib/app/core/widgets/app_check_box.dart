import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_colors.dart';

class AppCheckbox extends StatefulWidget {
  final bool value;
  final ValueChanged<bool> onChanged;

  AppCheckbox({required this.value, required this.onChanged});

  @override
  _CustomCheckboxState createState() => _CustomCheckboxState();
}

class _CustomCheckboxState extends State<AppCheckbox> {
  bool _value = false;

  @override
  void initState() {
    super.initState();
    _value = widget.value;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _value = !_value;
          widget.onChanged(_value);
        });
      },
      child: Container(
        width: 30.0,
        height: 30.0,
        decoration: BoxDecoration(
          border: Border.all(
            color: _value ? AppColor.primaryColor : Colors.grey,
            width: 2.0,
          ),
          borderRadius: BorderRadius.circular(4.0),
        ),
        child: _value
            ? Icon(
                Icons.check,
                size: 20.w,
                color: AppColor.primaryColor,
              )
            : null,
      ),
    );
  }
}

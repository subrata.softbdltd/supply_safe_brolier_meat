import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../constants/app_colors.dart';
import '../constants/app_constants.dart';
import '../constants/my_text_style.dart';

class AppEditText extends StatelessWidget {
  final TextEditingController controller;
  final TextInputType textInputType;
  final RegExp? regex;
  final String title;
  final String? hintText,
      helperText,
      validatorMSG,
      passwordForConfirm,
      errorMsg,
      minLengthMsg,
      invalidEmailMsg,
      invalidMobileNumber,
      conformPasswordDoesNotMatchMsg;
  final bool isEmail,
      enabled,
      isBangla,
      isReadonly,
      showCursor,
      isRequired,
      isPassword,
      needCapital,
      needTopSpace,
      isFilePicker,
      needToShowEye,
      doNeedToSaveUsernameOnGoogle,
      doNeedToSavePasswordOnGoogle,
      capitalLetter,
      isMobileNumber,
      isNumberKeyboard,
      isSignedNumberKeyboard,
      showValidatorMSG,
      isBanglaError,
      isFromDropdown,
      isSearch;
  final void Function(String)? onChanged;
  final int maxLine, maxLength, minLine;
  final int? validLength, minLength;

  final double topBottomPadding,
      editTextTopGap,
      topLeftRadius,
      topRightRadius,
      bottomLeftRadius,
      bottomRightRadius;
  final IconData? prefixIcon, suffixIcon;
  final Color? borderColor,
      errorBorderColor,
      enabledBorderColor,
      disabledBorderColor,
      focusedBorderColor;
  final Widget? prefixWidget;

  final FocusNode? focusNode, nextFocus;
  final TextStyle? titleStyle;
  dynamic inputFormatter, clickListener, eyeClick, fieldKey;
  final void Function()? onCloseClick;

  AppEditText({
    super.key,
    required this.title,
    required this.controller,
    this.regex,
    this.fieldKey,
    this.hintText,
    this.eyeClick,
    this.errorMsg,
    this.focusNode,
    this.nextFocus,
    this.minLength,
    this.onChanged,
    this.helperText,
    this.titleStyle,
    this.prefixIcon,
    this.suffixIcon,
    this.validLength,
    this.minLengthMsg,
    this.invalidEmailMsg,
    this.prefixWidget,
    this.invalidMobileNumber,
    this.clickListener,
    this.inputFormatter,
    this.focusedBorderColor,
    this.passwordForConfirm,
    this.conformPasswordDoesNotMatchMsg,
    this.topLeftRadius = 6,
    this.topRightRadius = 6,
    this.bottomLeftRadius = 1,
    this.bottomRightRadius = 1,
    this.minLine = 1,
    this.maxLine = 1,
    this.maxLength = 250,
    this.validatorMSG = "",
    this.topBottomPadding = 16,
    this.enabled = true,
    this.isEmail = false,
    this.isBangla = false,
    this.showCursor = true,
    this.isRequired = true,
    this.isReadonly = false,
    this.isPassword = false,
    this.needCapital = false,
    this.needTopSpace = true,
    this.isSearch = false,
    this.isFilePicker = false,
    this.capitalLetter = false,
    this.needToShowEye = false,
    this.isMobileNumber = false,
    this.isNumberKeyboard = false,
    this.isFromDropdown = false,
    this.editTextTopGap = 12,
    this.doNeedToSaveUsernameOnGoogle = false,
    this.doNeedToSavePasswordOnGoogle = false,
    this.isSignedNumberKeyboard = false,
    this.showValidatorMSG = false,
    this.borderColor,
    this.errorBorderColor,
    this.enabledBorderColor,
    this.disabledBorderColor,
    this.textInputType = TextInputType.text,
    this.isBanglaError = true,
    this.onCloseClick,
  });

  bool isVisible = false, needOldStyle = false;
  RegExp phoneNumberRegex = RegExp(r'^01\d{9}$');
  RegExp banglaRegex = RegExp(r'[ঀ-৿]+');
  RegExp emailRegex = RegExp(r'^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$');

  @override
  Widget build(BuildContext context) {
    if (textInputType == const TextInputType.numberWithOptions(signed: true) ||
        isMobileNumber ||
        isSignedNumberKeyboard) {
      inputFormatter = [
        FilteringTextInputFormatter.allow(RegExp("[0-9]")),
      ];
    }
    needOldStyle = isSearch;

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Visibility(
          visible: needTopSpace,
          child: appWidgets.gapH(editTextTopGap.h),
        ),
        TextFormField(
          key: fieldKey,
          autofillHints: doNeedToSaveUsernameOnGoogle
              ? const [AutofillHints.newUsername]
              : (doNeedToSavePasswordOnGoogle
                  ? const [AutofillHints.password]
                  : null),
          onTap: () {
            clickListener != null
                ? isFilePicker
                    ? clickListener(controller)
                    : clickListener()
                : null;
          },
          textCapitalization: needCapital
              ? TextCapitalization.characters
              : TextCapitalization.none,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          controller: controller,
          readOnly: isFromDropdown || isFilePicker || isReadonly,
          showCursor: isFromDropdown
              ? false
              : (!isReadonly && !isFilePicker && showCursor),
          obscureText: isPassword ? (isVisible ? false : true) : false,
          maxLines: maxLine,
          minLines: minLine,
          maxLength: isMobileNumber ? 11 : maxLength,
          inputFormatters: inputFormatter,
          onChanged: onChanged,
          focusNode: focusNode,
          style: text16Style(),
          onFieldSubmitted: (_) {
            focusNode?.unfocus();
            if (nextFocus != null) {
              FocusScope.of(context).requestFocus(nextFocus);
            }
          },
          keyboardType: isMobileNumber
              ? TextInputType.phone
              : isNumberKeyboard || isSignedNumberKeyboard
                  ? TextInputType.number
                  : isEmail
                      ? TextInputType.emailAddress
                      : TextInputType.text,
          validator: validator,
          decoration: decoration(),
        ),
      ],
    );
  }

  borderRadius() {
    return needOldStyle
        ? BorderRadius.circular(16)
        : BorderRadius.only(
            topLeft: Radius.circular(topLeftRadius),
            topRight: Radius.circular(topRightRadius),
            bottomLeft: Radius.circular(bottomLeftRadius),
            bottomRight: Radius.circular(bottomRightRadius),
          );
  }

  chooseFileDesign() {
    return Container(
      width: 120.w,
      decoration: BoxDecoration(
        color: AppColor.primaryColor,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(topLeftRadius),
          bottomLeft: Radius.circular(bottomLeftRadius),
        ),
      ),
      child: Center(
          child: Text(
        "Choose File",
        style: text14Style(color: AppColor.white),
      )),
    );
  }

  String? validator(String? value) {
    if (!isRequired) {
      return null;
    }
    if (value == null || value == "") {
      return isFilePicker
          ? isBanglaError
              ? "একটি ছবি বাছাই করুন"
              : "Pick a valid Image"
          : errorMsg ??
              (isBanglaError ? "ঘরটি পূরণ করতে হবে" : "Enter a value");
    } else if (regex != null) {
      if (!(regex?.hasMatch(value) ?? true)) {
        return isBanglaError ? "ইনপুটটি সঠিক নয়" : "Input is not valid";
      }
    } else if (isMobileNumber) {
      if (!phoneNumberRegex.hasMatch(value)) {
        return invalidMobileNumber ??
            (isBanglaError
                ? "প্রদত্ত মোবাইল নম্বরটি সঠিক নয়"
                : "Invalid Mobile Number");
      }
    } else if (validLength != null && validLength != value.length) {
      return validatorMSG;
    } else if (showValidatorMSG) {
      if (validatorMSG != "") {
        return validatorMSG;
      }
    } else if (passwordForConfirm != null) {
      if (passwordForConfirm != value) {
        return conformPasswordDoesNotMatchMsg ??
            (isBanglaError
                ? "পাসওয়ার্ড এবং কনফার্ম পাসওয়ার্ড মিলছে না"
                : "Password doesn't match");
      }
    } else if (isEmail) {
      if (!emailRegex.hasMatch(value)) {
        return invalidEmailMsg ??
            (isBanglaError
                ? "প্রদত্ত ই-মেইলটি সঠিক নয়"
                : "Invalid Email Address");
      }
    } else if (isPassword && minLength != null) {
      if (value.length < (minLength ?? 999)) {
        return minLengthMsg ??
            (isBanglaError
                ? "পাসওয়ার্ড অবশ্যই $minLength সংখ্যার হতে হবে"
                : "Password must be $minLength digits");
      }
    } else if (minLength != null) {
      if (value.length < (minLength ?? 999)) {
        return minLengthMsg ??
            (isBanglaError
                ? "ঘরটি কমপক্ষে $minLength ${isMobileNumber || isNumberKeyboard ? 'সংখ্যার' : 'বর্ণের'} হতে হবে"
                : "Field must be at least $minLength ${isMobileNumber || isNumberKeyboard ? 'Digits' : 'characters'}");
      }
    }

    return null;
  }

  decoration() {
    return InputDecoration(
      labelText: title == "" ? null : title,
      labelStyle: text16Style(),
      helperText: helperText,
      contentPadding: EdgeInsets.only(
        left: 16.w,
        right: 16.w,
        top: needOldStyle ? 18.h : topBottomPadding.h,
        bottom: needOldStyle ? 18.h : topBottomPadding.h,
      ),
      isDense: true,
      counterText: "",
      enabled: enabled,
      hintText: hintText,
      hintStyle: hintStyle,
      helperStyle: const TextStyle(
        color: AppColor.errorColor,
      ),
      filled: true,
      fillColor: enabled
          ? (isFromDropdown
              ? AppColor.dropdownEditTextBGColor
              : AppColor.editTextBGColor)
          : AppColor.disableColor,
      prefixIcon: prefixWidget ??
          (isSearch
              ? InkWell(
                  onTap: () {
                    if (onCloseClick != null) {
                      onCloseClick!();
                    }
                  },
                  child: const Icon(
                    Icons.search_outlined,
                    color: AppColor.inputColor,
                  ),
                )
              : isFilePicker
                  ? chooseFileDesign()
                  : (prefixIcon == null ? null : Icon(prefixIcon))),
      suffixIcon: isFromDropdown
          ? InkWell(
              onTap: () {
                if (onCloseClick != null) {
                  onCloseClick!();
                }
              },
              child: const Icon(
                Icons.cancel_outlined,
                color: AppColor.primaryColor,
              ),
            )
          : needToShowEye
              ? InkWell(
                  onTap: () {
                    eyeClick();
                  },
                  child: Icon(
                    isPassword ? Icons.visibility : Icons.visibility_off,
                    color: AppColor.primaryColor,
                  ),
                )
              : (suffixIcon == null
                  ? null
                  : Icon(
                      suffixIcon,
                      color: AppColor.primaryColor,
                    )),
      border: needOldStyle
          ? OutlineInputBorder(
              borderSide: BorderSide(
                color: borderColor ??
                    (isFromDropdown
                        ? AppColor.primaryColor
                        : AppColor.editTextBorderColor),
                width: 1,
              ),
              borderRadius: borderRadius(),
            )
          : UnderlineInputBorder(
              borderSide: BorderSide(
                color: borderColor ??
                    (isFromDropdown
                        ? AppColor.primaryColor
                        : AppColor.editTextBorderColor),
                width: 1,
              ),
              borderRadius: borderRadius(),
            ),
      enabledBorder: needOldStyle
          ? OutlineInputBorder(
              borderSide: BorderSide(
                color: enabledBorderColor ??
                    (isFromDropdown
                        ? AppColor.primaryColor
                        : AppColor.editTextBorderColor),
                width: 1,
              ),
              borderRadius: borderRadius(),
            )
          : UnderlineInputBorder(
              borderSide: BorderSide(
                color: enabledBorderColor ??
                    (isFromDropdown
                        ? AppColor.primaryColor
                        : AppColor.editTextBorderColor),
                width: 1,
              ),
              borderRadius: borderRadius(),
            ),
      focusedBorder: needOldStyle
          ? OutlineInputBorder(
              borderSide: BorderSide(
                color: focusedBorderColor ?? AppColor.editTextBorderColor,
                width: 1.0,
              ),
              borderRadius: borderRadius(),
            )
          : UnderlineInputBorder(
              borderSide: BorderSide(
                color: focusedBorderColor ?? AppColor.editTextBorderColor,
                width: 1.0,
              ),
              borderRadius: borderRadius(),
            ),
      focusedErrorBorder: needOldStyle
          ? OutlineInputBorder(
              borderSide: BorderSide(
                  width: 1,
                  color: (isFromDropdown
                      ? AppColor.primaryColor
                      : AppColor.errorColor)),
              borderRadius: borderRadius(),
            )
          : UnderlineInputBorder(
              borderSide: BorderSide(
                  width: 1,
                  color: (isFromDropdown
                      ? AppColor.primaryColor
                      : AppColor.errorColor)),
              borderRadius: borderRadius(),
            ),
      errorBorder: needOldStyle
          ? OutlineInputBorder(
              borderSide: BorderSide(
                color: errorBorderColor ??
                    (isFromDropdown
                        ? AppColor.primaryColor
                        : AppColor.errorColor),
                width: 1,
              ),
              borderRadius: borderRadius(),
            )
          : UnderlineInputBorder(
              borderSide: BorderSide(
                color: errorBorderColor ??
                    (isFromDropdown
                        ? AppColor.primaryColor
                        : AppColor.errorColor),
                width: 1,
              ),
              borderRadius: borderRadius(),
            ),
      disabledBorder: needOldStyle
          ? OutlineInputBorder(
              borderSide: BorderSide(
                color: disabledBorderColor ??
                    (isFromDropdown
                        ? AppColor.primaryColor
                        : AppColor.disableColor),
                width: 1,
              ),
              borderRadius: borderRadius(),
            )
          : UnderlineInputBorder(
              borderSide: BorderSide(
                color: disabledBorderColor ??
                    (isFromDropdown
                        ? AppColor.primaryColor
                        : AppColor.disableColor),
                width: 1,
              ),
              borderRadius: borderRadius(),
            ),
    );
  }
}

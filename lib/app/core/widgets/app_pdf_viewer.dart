import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:share_plus/share_plus.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'package:url_launcher/url_launcher.dart';

import '../constants/app_colors.dart';
import '../constants/app_constants.dart';
import '../constants/my_text_style.dart';

class MyPdfViewer extends StatefulWidget {
  final String title;
  final bool isPdf, isUnit8List, needUrl;
  final String pdfUrl;
  final Uint8List? pdfUrlUInt8List;

  const MyPdfViewer({
    super.key,
    this.title = "",
    this.pdfUrl = "https://www.africau.edu/images/default/sample.pdf",
    this.pdfUrlUInt8List,
    this.isPdf = true,
    this.needUrl = true,
    this.isUnit8List = false,
  });

  @override
  State<MyPdfViewer> createState() => _PdfViewerState();
}

class _PdfViewerState extends State<MyPdfViewer> {
  final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();
  final PdfViewerController _pdfViewerController = PdfViewerController();
  var permissionGranted = false;
  String webUrl = "https://file.nise.gov.bd/uploads/";

  String token = "";
  bool isTokenLoaded = false;

  @override
  void initState() {
    super.initState();
    logger.i("MyPDFUrl: ${widget.pdfUrl}");
    getToken();
  }

  Future<void> getToken() async {
    await appHelper.getToken().then((value) => {
          setState(() {
            token = value ?? "";
            isTokenLoaded = true;
            logger.i("MyPDFToken: $token");
          })
        });
  }

  Future<void> _launchInBrowser(url) async {
    if (!await launchUrl(
      url,
      mode: LaunchMode.externalApplication,
    )) {
      throw 'Could not launch $url';
    }
  }

  Future<File?> saveFile({fileName, url}) async {
    File? file = File('');

    if (Platform.isAndroid) {
      var status = await Permission.storage.status;
      if (status != PermissionStatus.granted) {
        status = await Permission.storage.request();
      }
      if (status.isGranted) {
        file = await createFile(fileName, url);
      }
    }
    return file;
  }

  String errorMessage = "";
  bool errorLoadingDoc = false;

  Future<File?> createFile(fileName, url) async {
    try {
      String dir = (await getApplicationDocumentsDirectory()).path;
      if (await File('$dir/$fileName').exists()) {
        logger.i("PDF File exist");
        return File('$dir/$fileName');
      }

      var request = await HttpClient().getUrl(Uri.parse(url));
      var response = await request.close();
      var bytes = await consolidateHttpClientResponseBytes(response);

      File file = File('$dir/$fileName');
      await file.writeAsBytes(bytes);

      return file;
    } catch (err) {
      errorMessage = "Error";
      logger.i(errorMessage);
      logger.w(err);
      return null;
    }
  }

  void _onShare(BuildContext context, {url}) async {
    if (Platform.isAndroid) {
      var file = await saveFile(
          fileName:
              "${widget.title}_${DateTime.now().millisecondsSinceEpoch.toString()}.pdf",
          url: url);
      if (file != null) {
        Share.shareXFiles([XFile(file.path)], text: 'pdf file');
      }
    } else {
      // // // share text
      Share.share(url);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: Container(
        padding: EdgeInsets.symmetric(horizontal: 8.w, vertical: 16.h),
        child: !errorLoadingDoc
            ? FloatingActionButton(
                heroTag: "pdf viewer",
                splashColor: AppColor.secondaryColor,
                tooltip: "Home",
                backgroundColor: Colors.white,
                foregroundColor: Colors.black,
                onPressed: () {
                  _launchInBrowser(Uri.parse(
                      "${widget.needUrl ? webUrl : ""}${widget.pdfUrl}")); //uri
                },
                child: Icon(
                  Icons.download,
                  color: AppColor.primaryColor,
                  semanticLabel: 'Download',
                  size: 40.r,
                ),
              )
            : null,
      ),
      appBar: AppBar(
        backgroundColor: AppColor.primaryColor,
        title: Text(widget.title),
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: const Icon(Icons.arrow_back_ios),
        ),
        actions: <Widget>[
          Visibility(
            visible: false,
            child: IconButton(
              icon: const Icon(
                Icons.share,
                color: Colors.white,
                semanticLabel: 'Share',
              ),
              onPressed: () {
                _onShare(context, url: "$webUrl${widget.pdfUrl}");
              },
            ),
          ),
        ],
      ),
      body: (widget.isPdf && widget.pdfUrl != "" && !errorLoadingDoc)
          ? isTokenLoaded
              ? SfPdfViewer.network(
                  widget.pdfUrl == ""
                      ? ""
                      : "${widget.needUrl ? webUrl : ""}${widget.pdfUrl}",
                  // widget.pdfUrl,
                  key: _pdfViewerKey,
                  headers: {"Authorization": "Bearer $token"},
                  controller: _pdfViewerController,
                  onDocumentLoadFailed: (value) {
                    logger.e("onDocumentLoadFailed value: ${value.error}");
                    logger.e(
                        "onDocumentLoadFailed value: ${value.error.runtimeType}");
                    logger
                        .e("onDocumentLoadFailed value1: ${value.description}");
                    setState(() {
                      if (value.error == "Error") {
                        errorLoadingDoc = true;
                      }
                    });
                  },
                  onDocumentLoaded: (value) {
                    logger.e("onDocumentLoaded value: $value");
                  },
                  // pageSpacing: 2,
                )
              : Container()
          : (widget.isUnit8List)
              ? SfPdfViewer.memory(
                  widget.pdfUrlUInt8List!,
                  key: _pdfViewerKey,
                  controller: _pdfViewerController,
                  // pageSpacing: 2,
                )
              : Padding(
                  padding: mainPaddingWidthOnly,
                  child: Center(
                    child: Text(
                      "There was an error opening this document. \nPlease contact the provider",
                      textAlign: TextAlign.center,
                      style: text18Style(fontSize: fontSize16, lineHeight: 1.3),
                    ),
                  ),
                ),
    );
  }
}

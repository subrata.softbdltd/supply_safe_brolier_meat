import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:soft_builder/constraints/my_text_style.dart';

import '../constants/app_colors.dart';
import '../constants/app_constants.dart';
import '../constants/asset_constants.dart';

class AppWidget {
  // IconData arrow_drop_down = IconData(0xe098, fontFamily: 'MaterialIcons');
  Widget gapH(double height) {
    return SizedBox(
      height: height.h,
    );
  }

  Widget gapW(double width) {
    return SizedBox(
      width: width.w,
    );
  }

  SnackbarController getSnackBar(
      {title = "Title",
      message = " Some message",
      int waitingTime = 2,
      int animationDuration = 500,
      snackPosition = SnackPosition.TOP,
      Color backgroundColor = AppColor.primaryColor,
      double backgroundColorOpacity = .7,
      colorText = AppColor.white}) {
    return Get.snackbar(title, message,
        snackPosition: snackPosition,
        duration: Duration(seconds: waitingTime),
        animationDuration: Duration(milliseconds: animationDuration),
        backgroundColor: backgroundColor.withOpacity(backgroundColorOpacity),
        colorText: colorText);
  }

  divider({double height = 10, Color color = AppColor.grey}) {
    return Divider(
      color: color,
      height: height,
      thickness: 0.5,
    );
  }

  showSnackBar(
      {required String message,
      isError = true,
      isSuccess = false,
      isWarning = false}) {
    return ScaffoldMessenger.of(Get.context!).showSnackBar(
      SnackBar(
        backgroundColor: isSuccess
            ? AppColor.green.withOpacity(.8)
            : isWarning
                ? AppColor.orangeLite.withOpacity(.8)
                : AppColor.red.withOpacity(.8),
        content: Text(
          message,
          style:
              text18Style(isWhiteColor: true, fontSize: 20, letterSpacing: 1.1),
        ),
      ),
    );
  }

  showSimpleDialog(
    String title,
    String body,
    retryClick, {
    buttonText = "Try Again",
    barrierDismissible = true,
  }) {
    if (Get.context == null) {
      return null;
    }
    return showDialog(
      context: Get.context!,
      barrierDismissible: barrierDismissible,
      builder: (context) => AlertDialog(
        title: Text(
          title,
          // style: textAppBarStyle(),
        ),
        content: Text(
          body,
          // style: textRegularStyle(),
        ),
        actions: <Widget>[
          TextButton(
              child: Text(buttonText),
              onPressed: () {
                Navigator.pop(context);
                if (buttonText != "Ok") {
                  try {
                    retryClick();
                    appHelper.showLoader();
                  } catch (e) {
                    appHelper.hideLoader();
                  }
                }
              })
        ],
      ),
    );
  }

  showPasswordChangeDialog() {
    if (Get.context == null) {
      return null;
    }
    return showDialog(
      context: Get.context!,
      barrierDismissible: false,
      builder: (context) => AlertDialog(
        title: const Text(
          "Change Your Default Password",
          // style: textAppBarStyle(),
        ),
        content: const Text(
          "Looks like you are using your default password. You have to change your password first.",
          // style: textRegularStyle(),
        ),
        actions: <Widget>[
          TextButton(
              child: const Text("Change Password"),
              onPressed: () {
                Navigator.pop(context);
                // Get.offNamed(Routes.PASSWORD_SETUP);
              })
        ],
      ),
    );
  }

  showSimpleToast(
    String? message, {
    String? title,
    bool isShort = false,
    bool isSuccess = false,
    bool isInfo = false,
    bool isError = true,
  }) {
    Get.snackbar(
      title ??
          (isSuccess
              ? "Success"
              : isInfo
                  ? "Info"
                  : "Error"),
      message ?? "",
      icon: Icon(
        (isSuccess
            ? Icons.check_circle
            : isInfo
                ? Icons.info
                : Icons.error),
        color: AppColor.white,
      ),
      shouldIconPulse: true,
      barBlur: 10,
      overlayBlur: 1,
      isDismissible: true,
      snackPosition: SnackPosition.TOP,
      backgroundColor: (isSuccess
          ? AppColor.successColor.withOpacity(.8)
          : isInfo
              ? AppColor.infoColor.withOpacity(.8)
              : AppColor.errorColor.withOpacity(.8)),
      margin: EdgeInsets.only(
        top: mainPaddingH,
        left: mainPaddingW,
        right: mainPaddingW,
      ),
      colorText: AppColor.white,
      duration: const Duration(seconds: 1),
    );
  }

  Widget nameInfo({title, description}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        gapH12,
        Text(
          title,
          style: text18Style(fontWeight: FontWeight.w600),
        ),
        gapH8,
        Text(
          description,
          style: text16Style(color: Colors.grey.shade800),
        ),
        gapH8,
      ],
    );
  }

  noInternetDialog() {
    return showDialog(
        context: Get.context!,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              'No Internet Connection',
              textAlign: TextAlign.center,
              style: text18Style(fontSize: 24),
            ),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Lottie.asset(
                  'assets/lottie/no_internet.json',
                  // Replace with your Lottie animation file path
                  width: 200,
                  height: 200,
                ),
                const SizedBox(height: 16),
                const Text(
                  'Please check your internet connection.',
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          );
        });
  }

  showDialogue(context, text, onClick) {
    return AwesomeDialog(
      context: context,
      dismissOnBackKeyPress: false,
      dialogType: DialogType.success,
      animType: AnimType.rightSlide,
      title: "Payment",
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: mainPaddingH),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              text,
              textAlign: TextAlign.center,
              style: text18Style(
                fontWeight: FontWeight.w300,
              ),
            ),
          ],
        ),
      ),
      dismissOnTouchOutside: false,
      btnOkOnPress: onClick,
      btnOkText: "Download",
    )..show();
  }

  String extractUrl(String url) {
    Uri uri = Uri.parse(url);
    List<String> pathSegments = uri.pathSegments;

    if (pathSegments.isNotEmpty) {
      String lastSegment = pathSegments.last;
      return lastSegment;
    }

    return '';
  }

  cardSection(
    title,
    subtitle,
    bgColor,
  ) {
    return Expanded(
      child: Card(
        color: bgColor, //Colors.orange.withOpacity(0.6),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),

        child: Container(
          padding: const EdgeInsets.all(12),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                title ?? "",
                style: text20Style(
                    color: AppColor.white,
                    fontWeight: FontWeight.w800,
                    fontSize: 22),
              ),
              Text(
                subtitle ?? "",
                textAlign: TextAlign.center,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: text16Style(
                    fontWeight: FontWeight.w800, color: AppColor.white),
              ),
            ],
          ),
        ), // Replace `data[index]['title']` with your API data
      ),
    );
  }

  Widget circularProgressBar() {
    return const Center(
      child: CircularProgressIndicator(
        color: AppColor.primaryColor,
      ),
    );
  }

  Widget noData() {
    return Center(
      child: Padding(
        padding: EdgeInsets.symmetric(
          vertical: mainPaddingH,
          horizontal: mainPaddingW,
        ),
        child: Column(
          children: [
            Lottie.asset(
              noDataFoundImage,
              height: 300.h,
              width: double.infinity,
            ),
            gapH6,
            Text(
              "কোন তথ্য পাওয়া যায়নি",
              style: text20Style(),
            )
          ],
        ),
      ),
    );
  }
  noDataFoundMsg() {
    return Center(
      child: Lottie.asset(
        noDataFoundLottie,
        width: Get.width * .8,
      ),
    );
  }
  Widget floatingActionButton({
    required String title,
    IconData icons = Icons.add,
    required Function() onTap,
  }) {
    return FloatingActionButton.extended(

      onPressed: onTap,
      label: Text(
        title,
        style: text16Style(color: AppColor.white, isWeight600: true),
      ),
      heroTag: null,
      icon: Container(
        decoration: const BoxDecoration(

          shape: BoxShape.circle,
        ),
        padding: mainPaddingSymmetric(3),
        child: Icon(
          icons,
          size: 15.w,
          color: AppColor.white,
        ),
      ),
      backgroundColor: Colors.red,
    );
  }
  simpleStatusAlertDialog({
    required headTitle,
    barrierDismissible = true,
    required VoidCallback yesTap,
    required VoidCallback noTap,
  }) {
    return showDialog(
      context: Get.context!,
      barrierDismissible: true,
      builder: (context) => AlertDialog(
        backgroundColor: AppColor.white,
        surfaceTintColor: AppColor.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        title: null,
        content: Container(
          width: double.maxFinite,
          padding: const EdgeInsets.symmetric(
            vertical: 10,
            horizontal: 0,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                headTitle,
                textAlign: TextAlign.center,
                style: text16Style(
                  isWeight600: true,
                  isPrimaryColor: true,
                ),
              ),
              gapH30,
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  dialogButton(
                    "না",
                    AppColor.red,
                    noTap,
                  ),
                  gapW20,
                  dialogButton(
                    "হ্যাঁ",
                    AppColor.green,
                    yesTap,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
  dialogButton(String title, Color backgroundColor, Function()? onTap) {
    return InkWell(
      onTap: onTap,
      borderRadius: BorderRadius.circular(8),
      child: Ink(
        decoration: BoxDecoration(
          color: backgroundColor,
          borderRadius: BorderRadius.circular(8),
        ),
        padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 8.h),
        child: Text(
          title,
          style: text14Style(
            isWeight600: true,
            isWhiteColor: true,
          ),
        ),
      ),
    );
  }
  Future<bool> appExitConfirmation({
    String? msg,
    bool needOnlyBack = false,
    EdgeInsetsGeometry? actionsPadding,
  }) async {
    bool? shouldClose = await showDialog(
      context: Get.context!,
      builder: (context) => AlertDialog(
        contentPadding: const EdgeInsets.only(top: 20, left: 20, right: 20),
        content: Text(
           "আপনি কি নিশ্চিত আপনি অ্যাপটি বন্ধ করতে চান?"
                  ,
          style: text14Style(),
        ),
        actionsPadding: actionsPadding,
        actions: [
          TextButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: Text(
              "না",
              style: text14Style(
                isPrimaryColor: true,
              ),
            ),
          ),
          TextButton(
            onPressed: () {
              if (needOnlyBack) {
                Navigator.of(context).pop(false);
                Navigator.of(context).pop(false);
              } else {
                SystemNavigator.pop();
              }
            },
            child: Text(
               "হ্যাঁ" ,
              style: text14Style(
                isPrimaryColor: true,
              ),
            ),
          ),
        ],
      ),
    );

    if ((shouldClose ?? false)) {
      Navigator.pop(Get.context!);
    }

    return false;
  }
}

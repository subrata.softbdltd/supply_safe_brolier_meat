class MyDropDownModel {
  String? id;
  String? title;
  dynamic extraData;

  MyDropDownModel({this.id, this.title, this.extraData});
}

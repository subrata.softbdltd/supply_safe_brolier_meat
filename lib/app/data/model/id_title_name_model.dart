import 'dart:convert';

IdTitleNameModel idTitleNameModelFromJson(String str) =>
    IdTitleNameModel.fromJson(json.decode(str));

String idTitleNameModelToJson(IdTitleNameModel data) =>
    json.encode(data.toJson());

class IdTitleNameModel {
  IdTitleNameModel({
    this.status,
    this.message,
    this.data,
    this.pagination,
  });

  IdTitleNameModel.fromJson(dynamic json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(Data.fromJson(v));
      });
    }
    pagination = json['pagination'] != null
        ? Pagination.fromJson(json['pagination'])
        : null;
  }

  bool? status;
  String? message;
  List<Data>? data;
  Pagination? pagination;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    if (pagination != null) {
      map['pagination'] = pagination?.toJson();
    }
    return map;
  }
}

Pagination paginationFromJson(String str) =>
    Pagination.fromJson(json.decode(str));

String paginationToJson(Pagination data) => json.encode(data.toJson());

class Pagination {
  Pagination({
    this.currentPage,
    this.from,
    this.lastPage,
    this.path,
    this.to,
    this.total,
  });

  Pagination.fromJson(dynamic json) {
    currentPage = json['currentPage'];
    from = json['from'];
    lastPage = json['lastPage'];
    path = json['path'];
    to = json['to'];
    total = json['total'];
  }

  num? currentPage;
  num? from;
  num? lastPage;
  String? path;
  num? to;
  num? total;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['currentPage'] = currentPage;
    map['from'] = from;
    map['lastPage'] = lastPage;
    map['path'] = path;
    map['to'] = to;
    map['total'] = total;
    return map;
  }
}

Data dataFromJson(String str) => Data.fromJson(json.decode(str));

String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    this.id,
    this.title,
    this.titleEn,
    this.titleBn,
    this.name,
    this.nameBn,
    this.nameEn,
  });

  Data.fromJson(dynamic json) {
    id = json['id'];
    title = json['title'];
    titleEn = json['title_en'];
    titleBn = json['title_bn'];
    name = json['name'];
    nameBn = json['name_bn'];
    nameEn = json['name_en'];
  }

  num? id;
  String? title;
  String? titleEn;
  String? titleBn;
  String? name;
  String? nameBn;
  String? nameEn;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['title'] = title;
    map['title_en'] = titleEn;
    map['title_bn'] = titleBn;
    map['name'] = name;
    map['name_bn'] = nameBn;
    map['name_en'] = nameEn;
    return map;
  }
}

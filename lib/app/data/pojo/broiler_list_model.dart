import 'dart:convert';

/// _response_status : {"success":true,"code":200,"error":"Unauthorized! Login First!","query_time":2}
/// data : [{"id":1,"farmer_id":1,"batch_no":"BCH00001","total_broiler":100,"total_damage_broiler":null,"start_date":"2024-03-31","end_date":"2024-04-29","is_data_submitted":0,"status":0,"created_by":1,"updated_by":1,"deleted_by":1,"deleted_at":null,"created_at":"2024-03-30T16:41:31.000000Z","updated_at":"2024-03-30T18:23:38.000000Z","farmer":{"id":1,"user_id":3,"name":"Sujan Das","photo":"upload/image/farmers/mjWDk1nyyWClIKYyrgD6xHYwpOEn4l8xIkpr1ucV.jpg","father_name":"TARAN DAS","mother_name":"SUPNA DAS","phone":"01843599324","nid":"","address":"Digor Pan Khali/ Hindu Para, 9 No Word","status":1,"created_by":1,"updated_by":null,"deleted_by":null,"deleted_at":null,"created_at":"2024-03-28T20:56:56.000000Z","updated_at":"2024-03-28T20:56:56.000000Z"}},{"id":2,"farmer_id":1,"batch_no":"BCH00002","total_broiler":150,"total_damage_broiler":null,"start_date":"2024-03-30","end_date":"2024-04-29","is_data_submitted":0,"status":1,"created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-03-30T18:04:38.000000Z","updated_at":"2024-03-30T18:04:38.000000Z","farmer":{"id":1,"user_id":3,"name":"Sujan Das","photo":"upload/image/farmers/mjWDk1nyyWClIKYyrgD6xHYwpOEn4l8xIkpr1ucV.jpg","father_name":"TARAN DAS","mother_name":"SUPNA DAS","phone":"01843599324","nid":"","address":"Digor Pan Khali/ Hindu Para, 9 No Word","status":1,"created_by":1,"updated_by":null,"deleted_by":null,"deleted_at":null,"created_at":"2024-03-28T20:56:56.000000Z","updated_at":"2024-03-28T20:56:56.000000Z"}}]

BroilerListModel broilerListModelFromJson(String str) =>
    BroilerListModel.fromJson(json.decode(str));
String broilerListModelToJson(BroilerListModel data) =>
    json.encode(data.toJson());

class BroilerListModel {
  BroilerListModel({
    this.responseStatus,
    this.data,
  });

  BroilerListModel.fromJson(dynamic json) {
    responseStatus = json['_response_status'] != null
        ? ResponseStatus.fromJson(json['_response_status'])
        : null;
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(Data.fromJson(v));
      });
    }
  }
  ResponseStatus? responseStatus;
  List<Data>? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (responseStatus != null) {
      map['_response_status'] = responseStatus?.toJson();
    }
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 1
/// farmer_id : 1
/// batch_no : "BCH00001"
/// total_broiler : 100
/// total_damage_broiler : null
/// start_date : "2024-03-31"
/// end_date : "2024-04-29"
/// is_data_submitted : 0
/// status : 0
/// created_by : 1
/// updated_by : 1
/// deleted_by : 1
/// deleted_at : null
/// created_at : "2024-03-30T16:41:31.000000Z"
/// updated_at : "2024-03-30T18:23:38.000000Z"
/// farmer : {"id":1,"user_id":3,"name":"Sujan Das","photo":"upload/image/farmers/mjWDk1nyyWClIKYyrgD6xHYwpOEn4l8xIkpr1ucV.jpg","father_name":"TARAN DAS","mother_name":"SUPNA DAS","phone":"01843599324","nid":"","address":"Digor Pan Khali/ Hindu Para, 9 No Word","status":1,"created_by":1,"updated_by":null,"deleted_by":null,"deleted_at":null,"created_at":"2024-03-28T20:56:56.000000Z","updated_at":"2024-03-28T20:56:56.000000Z"}

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    this.id,
    this.farmerId,
    this.batchNo,
    this.totalBroiler,
    this.totalDamageBroiler,
    this.startDate,
    this.endDate,
    this.isDataSubmitted,
    this.status,
    this.createdBy,
    this.updatedBy,
    this.deletedBy,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
    this.farmer,
  });

  Data.fromJson(dynamic json) {
    id = json['id'];
    farmerId = json['farmer_id'];
    batchNo = json['batch_no'];
    totalBroiler = json['total_broiler'];
    totalDamageBroiler = json['total_damage_broiler'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    isDataSubmitted = json['is_data_submitted'];
    status = json['status'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    deletedBy = json['deleted_by'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    farmer = json['farmer'] != null ? Farmer.fromJson(json['farmer']) : null;
  }
  int? id;
  dynamic farmerId;
  String? batchNo;
  dynamic totalBroiler;
  dynamic totalDamageBroiler;
  String? startDate;
  String? endDate;
  dynamic isDataSubmitted;
  dynamic status;
  dynamic createdBy;
  dynamic updatedBy;
  dynamic deletedBy;
  dynamic deletedAt;
  String? createdAt;
  String? updatedAt;
  Farmer? farmer;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['farmer_id'] = farmerId;
    map['batch_no'] = batchNo;
    map['total_broiler'] = totalBroiler;
    map['total_damage_broiler'] = totalDamageBroiler;
    map['start_date'] = startDate;
    map['end_date'] = endDate;
    map['is_data_submitted'] = isDataSubmitted;
    map['status'] = status;
    map['created_by'] = createdBy;
    map['updated_by'] = updatedBy;
    map['deleted_by'] = deletedBy;
    map['deleted_at'] = deletedAt;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    if (farmer != null) {
      map['farmer'] = farmer?.toJson();
    }
    return map;
  }
}

/// id : 1
/// user_id : 3
/// name : "Sujan Das"
/// photo : "upload/image/farmers/mjWDk1nyyWClIKYyrgD6xHYwpOEn4l8xIkpr1ucV.jpg"
/// father_name : "TARAN DAS"
/// mother_name : "SUPNA DAS"
/// phone : "01843599324"
/// nid : ""
/// address : "Digor Pan Khali/ Hindu Para, 9 No Word"
/// status : 1
/// created_by : 1
/// updated_by : null
/// deleted_by : null
/// deleted_at : null
/// created_at : "2024-03-28T20:56:56.000000Z"
/// updated_at : "2024-03-28T20:56:56.000000Z"

Farmer farmerFromJson(String str) => Farmer.fromJson(json.decode(str));
String farmerToJson(Farmer data) => json.encode(data.toJson());

class Farmer {
  Farmer({
    this.id,
    this.userId,
    this.name,
    this.photo,
    this.fatherName,
    this.motherName,
    this.phone,
    this.nid,
    this.address,
    this.status,
    this.createdBy,
    this.updatedBy,
    this.deletedBy,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
  });

  Farmer.fromJson(dynamic json) {
    id = json['id'];
    userId = json['user_id'];
    name = json['name'];
    photo = json['photo'];
    fatherName = json['father_name'];
    motherName = json['mother_name'];
    phone = json['phone'];
    nid = json['nid'];
    address = json['address'];
    status = json['status'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    deletedBy = json['deleted_by'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }
  dynamic id;
  dynamic userId;
  String? name;
  String? photo;
  String? fatherName;
  String? motherName;
  String? phone;
  String? nid;
  String? address;
  dynamic status;
  dynamic createdBy;
  dynamic updatedBy;
  dynamic deletedBy;
  dynamic deletedAt;
  String? createdAt;
  String? updatedAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['user_id'] = userId;
    map['name'] = name;
    map['photo'] = photo;
    map['father_name'] = fatherName;
    map['mother_name'] = motherName;
    map['phone'] = phone;
    map['nid'] = nid;
    map['address'] = address;
    map['status'] = status;
    map['created_by'] = createdBy;
    map['updated_by'] = updatedBy;
    map['deleted_by'] = deletedBy;
    map['deleted_at'] = deletedAt;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    return map;
  }
}

/// success : true
/// code : 200
/// error : "Unauthorized! Login First!"
/// query_time : 2

ResponseStatus responseStatusFromJson(String str) =>
    ResponseStatus.fromJson(json.decode(str));
String responseStatusToJson(ResponseStatus data) => json.encode(data.toJson());

class ResponseStatus {
  ResponseStatus({
    this.success,
    this.code,
    this.error,
    this.queryTime,
  });

  ResponseStatus.fromJson(dynamic json) {
    success = json['success'];
    code = json['code'];
    error = json['error'];
    queryTime = json['query_time'];
  }
  bool? success;
  dynamic code;
  String? error;
  dynamic queryTime;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['success'] = success;
    map['code'] = code;
    map['error'] = error;
    map['query_time'] = queryTime;
    return map;
  }
}

import 'dart:convert';
/// _response_status : {"success":true,"code":200,"query_time":0}
/// data : [{"id":3,"order_no":"02c9c980-b99a-4d85-9a51-cdc8f70bc2c6","customer_id":19,"outlet_id":1,"amount":"870.00","payable_amount":"830.00","discount_amount":"40.00","delivery_charge":"0.00","status":2,"payment_type":2,"delivery_type":2,"created_user":1,"date":"2024-06-20 11:32:38","delivery_address":null,"created_by":null,"updated_by":null,"created_at":"2024-06-20T05:32:38.000000Z","updated_at":"2024-06-20T05:32:38.000000Z","items":[{"id":2,"order_id":3,"package_id":1,"count":0,"price":"250.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-06-20T05:32:38.000000Z","updated_at":"2024-06-20T05:32:38.000000Z","package":{"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/VuE0ivOvCMLJM93WdkgcDJzzCRQPd7sHwZFruiJJ.png","type":2,"price":"250.00","discount":"10.00","descriptions":"Please buy my product.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-05-25T17:53:06.000000Z"}},{"id":3,"order_id":3,"package_id":2,"count":0,"price":"120.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-06-20T05:32:38.000000Z","updated_at":"2024-06-20T05:32:38.000000Z","package":{"id":2,"name":"New Chicken","thumbnail":"upload/image/packages/thumbnails/8my6OSooC93P2KO3uRCS3wyFkaPs5PQMMeOp1c4H.png","type":2,"price":"120.00","discount":"10.00","descriptions":"New pics","status":1,"weight":"8","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-19T17:33:57.000000Z","updated_at":"2024-05-25T17:48:19.000000Z"}}],"customer":{"id":19,"role_id":5,"name":"Guest","email":null,"phone":"01928392891","password":"","otp":null,"photo":null,"status":1,"created_by":null,"updated_by":null,"deleted_by":null,"deleted_at":null,"created_at":"2024-06-20T05:32:38.000000Z","updated_at":"2024-06-20T05:32:38.000000Z","otp_expired_at":null,"otp_verified_at":null}},{"id":4,"order_no":"c03bdd0e-bdcf-4856-9518-8d6e71e14f21","customer_id":20,"outlet_id":1,"amount":"370.00","payable_amount":"350.00","discount_amount":"20.00","delivery_charge":"0.00","status":2,"payment_type":2,"delivery_type":2,"created_user":1,"date":"2024-06-20 23:17:57","delivery_address":null,"created_by":null,"updated_by":null,"created_at":"2024-06-20T17:17:57.000000Z","updated_at":"2024-06-20T17:17:57.000000Z","items":[{"id":4,"order_id":4,"package_id":1,"count":0,"price":"250.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-06-20T17:17:57.000000Z","updated_at":"2024-06-20T17:17:57.000000Z","package":{"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/VuE0ivOvCMLJM93WdkgcDJzzCRQPd7sHwZFruiJJ.png","type":2,"price":"250.00","discount":"10.00","descriptions":"Please buy my product.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-05-25T17:53:06.000000Z"}},{"id":5,"order_id":4,"package_id":2,"count":0,"price":"120.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-06-20T17:17:57.000000Z","updated_at":"2024-06-20T17:17:57.000000Z","package":{"id":2,"name":"New Chicken","thumbnail":"upload/image/packages/thumbnails/8my6OSooC93P2KO3uRCS3wyFkaPs5PQMMeOp1c4H.png","type":2,"price":"120.00","discount":"10.00","descriptions":"New pics","status":1,"weight":"8","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-19T17:33:57.000000Z","updated_at":"2024-05-25T17:48:19.000000Z"}}],"customer":{"id":20,"role_id":5,"name":"Guest","email":null,"phone":"01987367726","password":"","otp":null,"photo":null,"status":1,"created_by":null,"updated_by":null,"deleted_by":null,"deleted_at":null,"created_at":"2024-06-20T17:17:57.000000Z","updated_at":"2024-06-20T17:17:57.000000Z","otp_expired_at":null,"otp_verified_at":null}},{"id":5,"order_no":"d9103cd5-efca-4a5a-8aa8-c6f497db8154","customer_id":20,"outlet_id":1,"amount":"370.00","payable_amount":"350.00","discount_amount":"20.00","delivery_charge":"0.00","status":2,"payment_type":2,"delivery_type":2,"created_user":1,"date":"2024-06-20 23:18:06","delivery_address":null,"created_by":null,"updated_by":null,"created_at":"2024-06-20T17:18:06.000000Z","updated_at":"2024-06-20T17:18:06.000000Z","items":[{"id":6,"order_id":5,"package_id":1,"count":0,"price":"250.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-06-20T17:18:06.000000Z","updated_at":"2024-06-20T17:18:06.000000Z","package":{"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/VuE0ivOvCMLJM93WdkgcDJzzCRQPd7sHwZFruiJJ.png","type":2,"price":"250.00","discount":"10.00","descriptions":"Please buy my product.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-05-25T17:53:06.000000Z"}},{"id":7,"order_id":5,"package_id":2,"count":0,"price":"120.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-06-20T17:18:06.000000Z","updated_at":"2024-06-20T17:18:06.000000Z","package":{"id":2,"name":"New Chicken","thumbnail":"upload/image/packages/thumbnails/8my6OSooC93P2KO3uRCS3wyFkaPs5PQMMeOp1c4H.png","type":2,"price":"120.00","discount":"10.00","descriptions":"New pics","status":1,"weight":"8","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-19T17:33:57.000000Z","updated_at":"2024-05-25T17:48:19.000000Z"}}],"customer":{"id":20,"role_id":5,"name":"Guest","email":null,"phone":"01987367726","password":"","otp":null,"photo":null,"status":1,"created_by":null,"updated_by":null,"deleted_by":null,"deleted_at":null,"created_at":"2024-06-20T17:17:57.000000Z","updated_at":"2024-06-20T17:17:57.000000Z","otp_expired_at":null,"otp_verified_at":null}},{"id":6,"order_no":"fd92e8f5-f099-4a61-938c-9f804e5c5433","customer_id":20,"outlet_id":1,"amount":"370.00","payable_amount":"350.00","discount_amount":"20.00","delivery_charge":"0.00","status":2,"payment_type":2,"delivery_type":2,"created_user":1,"date":"2024-06-20 23:18:12","delivery_address":null,"created_by":null,"updated_by":null,"created_at":"2024-06-20T17:18:12.000000Z","updated_at":"2024-06-20T17:18:12.000000Z","items":[{"id":8,"order_id":6,"package_id":1,"count":0,"price":"250.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-06-20T17:18:12.000000Z","updated_at":"2024-06-20T17:18:12.000000Z","package":{"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/VuE0ivOvCMLJM93WdkgcDJzzCRQPd7sHwZFruiJJ.png","type":2,"price":"250.00","discount":"10.00","descriptions":"Please buy my product.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-05-25T17:53:06.000000Z"}},{"id":9,"order_id":6,"package_id":2,"count":0,"price":"120.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-06-20T17:18:12.000000Z","updated_at":"2024-06-20T17:18:12.000000Z","package":{"id":2,"name":"New Chicken","thumbnail":"upload/image/packages/thumbnails/8my6OSooC93P2KO3uRCS3wyFkaPs5PQMMeOp1c4H.png","type":2,"price":"120.00","discount":"10.00","descriptions":"New pics","status":1,"weight":"8","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-19T17:33:57.000000Z","updated_at":"2024-05-25T17:48:19.000000Z"}}],"customer":{"id":20,"role_id":5,"name":"Guest","email":null,"phone":"01987367726","password":"","otp":null,"photo":null,"status":1,"created_by":null,"updated_by":null,"deleted_by":null,"deleted_at":null,"created_at":"2024-06-20T17:17:57.000000Z","updated_at":"2024-06-20T17:17:57.000000Z","otp_expired_at":null,"otp_verified_at":null}},{"id":7,"order_no":"5913ce5c-8752-4ca1-9d6a-3b3e6fd4662d","customer_id":20,"outlet_id":1,"amount":"370.00","payable_amount":"350.00","discount_amount":"20.00","delivery_charge":"0.00","status":2,"payment_type":2,"delivery_type":2,"created_user":1,"date":"2024-06-20 23:22:39","delivery_address":null,"created_by":null,"updated_by":null,"created_at":"2024-06-20T17:22:39.000000Z","updated_at":"2024-06-20T17:22:39.000000Z","items":[{"id":10,"order_id":7,"package_id":1,"count":0,"price":"250.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-06-20T17:22:39.000000Z","updated_at":"2024-06-20T17:22:39.000000Z","package":{"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/VuE0ivOvCMLJM93WdkgcDJzzCRQPd7sHwZFruiJJ.png","type":2,"price":"250.00","discount":"10.00","descriptions":"Please buy my product.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-05-25T17:53:06.000000Z"}},{"id":11,"order_id":7,"package_id":2,"count":0,"price":"120.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-06-20T17:22:40.000000Z","updated_at":"2024-06-20T17:22:40.000000Z","package":{"id":2,"name":"New Chicken","thumbnail":"upload/image/packages/thumbnails/8my6OSooC93P2KO3uRCS3wyFkaPs5PQMMeOp1c4H.png","type":2,"price":"120.00","discount":"10.00","descriptions":"New pics","status":1,"weight":"8","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-19T17:33:57.000000Z","updated_at":"2024-05-25T17:48:19.000000Z"}}],"customer":{"id":20,"role_id":5,"name":"Guest","email":null,"phone":"01987367726","password":"","otp":null,"photo":null,"status":1,"created_by":null,"updated_by":null,"deleted_by":null,"deleted_at":null,"created_at":"2024-06-20T17:17:57.000000Z","updated_at":"2024-06-20T17:17:57.000000Z","otp_expired_at":null,"otp_verified_at":null}},{"id":8,"order_no":"38ec470c-7169-43a5-86a2-91eb7135a1f2","customer_id":20,"outlet_id":1,"amount":"370.00","payable_amount":"350.00","discount_amount":"20.00","delivery_charge":"0.00","status":2,"payment_type":2,"delivery_type":2,"created_user":1,"date":"2024-06-20 23:23:14","delivery_address":null,"created_by":null,"updated_by":null,"created_at":"2024-06-20T17:23:14.000000Z","updated_at":"2024-06-20T17:23:14.000000Z","items":[{"id":12,"order_id":8,"package_id":1,"count":0,"price":"250.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-06-20T17:23:14.000000Z","updated_at":"2024-06-20T17:23:14.000000Z","package":{"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/VuE0ivOvCMLJM93WdkgcDJzzCRQPd7sHwZFruiJJ.png","type":2,"price":"250.00","discount":"10.00","descriptions":"Please buy my product.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-05-25T17:53:06.000000Z"}},{"id":13,"order_id":8,"package_id":2,"count":0,"price":"120.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-06-20T17:23:14.000000Z","updated_at":"2024-06-20T17:23:14.000000Z","package":{"id":2,"name":"New Chicken","thumbnail":"upload/image/packages/thumbnails/8my6OSooC93P2KO3uRCS3wyFkaPs5PQMMeOp1c4H.png","type":2,"price":"120.00","discount":"10.00","descriptions":"New pics","status":1,"weight":"8","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-19T17:33:57.000000Z","updated_at":"2024-05-25T17:48:19.000000Z"}}],"customer":{"id":20,"role_id":5,"name":"Guest","email":null,"phone":"01987367726","password":"","otp":null,"photo":null,"status":1,"created_by":null,"updated_by":null,"deleted_by":null,"deleted_at":null,"created_at":"2024-06-20T17:17:57.000000Z","updated_at":"2024-06-20T17:17:57.000000Z","otp_expired_at":null,"otp_verified_at":null}},{"id":9,"order_no":"7c3699d4-7f68-45bf-83b8-fd7dcb734ae3","customer_id":21,"outlet_id":1,"amount":"1350.00","payable_amount":"1270.00","discount_amount":"80.00","delivery_charge":"0.00","status":2,"payment_type":2,"delivery_type":2,"created_user":1,"date":"2024-06-26 00:12:54","delivery_address":null,"created_by":null,"updated_by":null,"created_at":"2024-06-25T18:12:54.000000Z","updated_at":"2024-06-25T18:12:54.000000Z","items":[{"id":14,"order_id":9,"package_id":1,"count":0,"price":"250.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-06-25T18:12:54.000000Z","updated_at":"2024-06-25T18:12:54.000000Z","package":{"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/VuE0ivOvCMLJM93WdkgcDJzzCRQPd7sHwZFruiJJ.png","type":2,"price":"250.00","discount":"10.00","descriptions":"Please buy my product.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-05-25T17:53:06.000000Z"}},{"id":15,"order_id":9,"package_id":2,"count":0,"price":"120.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-06-25T18:12:54.000000Z","updated_at":"2024-06-25T18:12:54.000000Z","package":{"id":2,"name":"New Chicken","thumbnail":"upload/image/packages/thumbnails/8my6OSooC93P2KO3uRCS3wyFkaPs5PQMMeOp1c4H.png","type":2,"price":"120.00","discount":"10.00","descriptions":"New pics","status":1,"weight":"8","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-19T17:33:57.000000Z","updated_at":"2024-05-25T17:48:19.000000Z"}}],"customer":{"id":21,"role_id":5,"name":"Guest","email":null,"phone":"01537457618","password":"","otp":null,"photo":null,"status":1,"created_by":null,"updated_by":null,"deleted_by":null,"deleted_at":null,"created_at":"2024-06-25T18:12:54.000000Z","updated_at":"2024-06-25T18:12:54.000000Z","otp_expired_at":null,"otp_verified_at":null}},{"id":10,"order_no":"319df656-6c30-44a8-bab6-2cc1a9388992","customer_id":22,"outlet_id":1,"amount":"250.00","payable_amount":"240.00","discount_amount":"10.00","delivery_charge":"0.00","status":2,"payment_type":2,"delivery_type":2,"created_user":1,"date":"2024-06-28 01:33:02","delivery_address":null,"created_by":null,"updated_by":null,"created_at":"2024-06-27T19:33:02.000000Z","updated_at":"2024-06-27T19:33:02.000000Z","items":[{"id":16,"order_id":10,"package_id":1,"count":0,"price":"250.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-06-27T19:33:02.000000Z","updated_at":"2024-06-27T19:33:02.000000Z","package":{"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/VuE0ivOvCMLJM93WdkgcDJzzCRQPd7sHwZFruiJJ.png","type":2,"price":"250.00","discount":"10.00","descriptions":"Please buy my product.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-05-25T17:53:06.000000Z"}}],"customer":{"id":22,"role_id":5,"name":"Guest","email":null,"phone":"01928393849","password":"","otp":null,"photo":null,"status":1,"created_by":null,"updated_by":null,"deleted_by":null,"deleted_at":null,"created_at":"2024-06-27T19:33:02.000000Z","updated_at":"2024-06-27T19:33:02.000000Z","otp_expired_at":null,"otp_verified_at":null}},{"id":11,"order_no":"4cbfa7af-0851-4b82-bde7-ca83cfb421e2","customer_id":23,"outlet_id":1,"amount":"740.00","payable_amount":"700.00","discount_amount":"40.00","delivery_charge":"0.00","status":2,"payment_type":2,"delivery_type":2,"created_user":1,"date":"2024-06-28 01:34:07","delivery_address":null,"created_by":null,"updated_by":null,"created_at":"2024-06-27T19:34:07.000000Z","updated_at":"2024-06-27T19:34:07.000000Z","items":[{"id":17,"order_id":11,"package_id":1,"count":0,"price":"250.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-06-27T19:34:07.000000Z","updated_at":"2024-06-27T19:34:07.000000Z","package":{"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/VuE0ivOvCMLJM93WdkgcDJzzCRQPd7sHwZFruiJJ.png","type":2,"price":"250.00","discount":"10.00","descriptions":"Please buy my product.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-05-25T17:53:06.000000Z"}},{"id":18,"order_id":11,"package_id":2,"count":0,"price":"120.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-06-27T19:34:07.000000Z","updated_at":"2024-06-27T19:34:07.000000Z","package":{"id":2,"name":"New Chicken","thumbnail":"upload/image/packages/thumbnails/8my6OSooC93P2KO3uRCS3wyFkaPs5PQMMeOp1c4H.png","type":2,"price":"120.00","discount":"10.00","descriptions":"New pics","status":1,"weight":"8","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-19T17:33:57.000000Z","updated_at":"2024-05-25T17:48:19.000000Z"}}],"customer":{"id":23,"role_id":5,"name":"Guest","email":null,"phone":"01929834832","password":"","otp":null,"photo":null,"status":1,"created_by":null,"updated_by":null,"deleted_by":null,"deleted_at":null,"created_at":"2024-06-27T19:34:07.000000Z","updated_at":"2024-06-27T19:34:07.000000Z","otp_expired_at":null,"otp_verified_at":null}}]

OrderCreateModel orderCreateModelFromJson(String str) => OrderCreateModel.fromJson(json.decode(str));
String orderCreateModelToJson(OrderCreateModel data) => json.encode(data.toJson());
class OrderCreateModel {
  OrderCreateModel({
      this.responseStatus, 
      this.data,});

  OrderCreateModel.fromJson(dynamic json) {
    responseStatus = json['_response_status'] != null ? ResponseStatus.fromJson(json['_response_status']) : null;
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(Data.fromJson(v));
      });
    }
  }
  ResponseStatus? responseStatus;
  List<Data>? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (responseStatus != null) {
      map['_response_status'] = responseStatus?.toJson();
    }
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 3
/// order_no : "02c9c980-b99a-4d85-9a51-cdc8f70bc2c6"
/// customer_id : 19
/// outlet_id : 1
/// amount : "870.00"
/// payable_amount : "830.00"
/// discount_amount : "40.00"
/// delivery_charge : "0.00"
/// status : 2
/// payment_type : 2
/// delivery_type : 2
/// created_user : 1
/// date : "2024-06-20 11:32:38"
/// delivery_address : null
/// created_by : null
/// updated_by : null
/// created_at : "2024-06-20T05:32:38.000000Z"
/// updated_at : "2024-06-20T05:32:38.000000Z"
/// items : [{"id":2,"order_id":3,"package_id":1,"count":0,"price":"250.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-06-20T05:32:38.000000Z","updated_at":"2024-06-20T05:32:38.000000Z","package":{"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/VuE0ivOvCMLJM93WdkgcDJzzCRQPd7sHwZFruiJJ.png","type":2,"price":"250.00","discount":"10.00","descriptions":"Please buy my product.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-05-25T17:53:06.000000Z"}},{"id":3,"order_id":3,"package_id":2,"count":0,"price":"120.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-06-20T05:32:38.000000Z","updated_at":"2024-06-20T05:32:38.000000Z","package":{"id":2,"name":"New Chicken","thumbnail":"upload/image/packages/thumbnails/8my6OSooC93P2KO3uRCS3wyFkaPs5PQMMeOp1c4H.png","type":2,"price":"120.00","discount":"10.00","descriptions":"New pics","status":1,"weight":"8","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-19T17:33:57.000000Z","updated_at":"2024-05-25T17:48:19.000000Z"}}]
/// customer : {"id":19,"role_id":5,"name":"Guest","email":null,"phone":"01928392891","password":"","otp":null,"photo":null,"status":1,"created_by":null,"updated_by":null,"deleted_by":null,"deleted_at":null,"created_at":"2024-06-20T05:32:38.000000Z","updated_at":"2024-06-20T05:32:38.000000Z","otp_expired_at":null,"otp_verified_at":null}

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());
class Data {
  Data({
      this.id, 
      this.orderNo, 
      this.customerId, 
      this.outletId, 
      this.amount, 
      this.payableAmount, 
      this.discountAmount, 
      this.deliveryCharge, 
      this.status, 
      this.paymentType, 
      this.deliveryType, 
      this.createdUser, 
      this.date, 
      this.deliveryAddress, 
      this.createdBy, 
      this.updatedBy, 
      this.createdAt, 
      this.updatedAt, 
      this.items, 
      this.customer,});

  Data.fromJson(dynamic json) {
    id = json['id'];
    orderNo = json['order_no'];
    customerId = json['customer_id'];
    outletId = json['outlet_id'];
    amount = json['amount'];
    payableAmount = json['payable_amount'];
    discountAmount = json['discount_amount'];
    deliveryCharge = json['delivery_charge'];
    status = json['status'];
    paymentType = json['payment_type'];
    deliveryType = json['delivery_type'];
    createdUser = json['created_user'];
    date = json['date'];
    deliveryAddress = json['delivery_address'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['items'] != null) {
      items = [];
      json['items'].forEach((v) {
        items?.add(Items.fromJson(v));
      });
    }
    customer = json['customer'] != null ? Customer.fromJson(json['customer']) : null;
  }
  num? id;
  String? orderNo;
  num? customerId;
  num? outletId;
  String? amount;
  String? payableAmount;
  String? discountAmount;
  String? deliveryCharge;
  num? status;
  num? paymentType;
  num? deliveryType;
  num? createdUser;
  String? date;
  dynamic deliveryAddress;
  dynamic createdBy;
  dynamic updatedBy;
  String? createdAt;
  String? updatedAt;
  List<Items>? items;
  Customer? customer;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['order_no'] = orderNo;
    map['customer_id'] = customerId;
    map['outlet_id'] = outletId;
    map['amount'] = amount;
    map['payable_amount'] = payableAmount;
    map['discount_amount'] = discountAmount;
    map['delivery_charge'] = deliveryCharge;
    map['status'] = status;
    map['payment_type'] = paymentType;
    map['delivery_type'] = deliveryType;
    map['created_user'] = createdUser;
    map['date'] = date;
    map['delivery_address'] = deliveryAddress;
    map['created_by'] = createdBy;
    map['updated_by'] = updatedBy;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    if (items != null) {
      map['items'] = items?.map((v) => v.toJson()).toList();
    }
    if (customer != null) {
      map['customer'] = customer?.toJson();
    }
    return map;
  }

}

/// id : 19
/// role_id : 5
/// name : "Guest"
/// email : null
/// phone : "01928392891"
/// password : ""
/// otp : null
/// photo : null
/// status : 1
/// created_by : null
/// updated_by : null
/// deleted_by : null
/// deleted_at : null
/// created_at : "2024-06-20T05:32:38.000000Z"
/// updated_at : "2024-06-20T05:32:38.000000Z"
/// otp_expired_at : null
/// otp_verified_at : null

Customer customerFromJson(String str) => Customer.fromJson(json.decode(str));
String customerToJson(Customer data) => json.encode(data.toJson());
class Customer {
  Customer({
      this.id, 
      this.roleId, 
      this.name, 
      this.email, 
      this.phone, 
      this.password, 
      this.otp, 
      this.photo, 
      this.status, 
      this.createdBy, 
      this.updatedBy, 
      this.deletedBy, 
      this.deletedAt, 
      this.createdAt, 
      this.updatedAt, 
      this.otpExpiredAt, 
      this.otpVerifiedAt,});

  Customer.fromJson(dynamic json) {
    id = json['id'];
    roleId = json['role_id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    password = json['password'];
    otp = json['otp'];
    photo = json['photo'];
    status = json['status'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    deletedBy = json['deleted_by'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    otpExpiredAt = json['otp_expired_at'];
    otpVerifiedAt = json['otp_verified_at'];
  }
  num? id;
  num? roleId;
  String? name;
  dynamic email;
  String? phone;
  String? password;
  dynamic otp;
  dynamic photo;
  num? status;
  dynamic createdBy;
  dynamic updatedBy;
  dynamic deletedBy;
  dynamic deletedAt;
  String? createdAt;
  String? updatedAt;
  dynamic otpExpiredAt;
  dynamic otpVerifiedAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['role_id'] = roleId;
    map['name'] = name;
    map['email'] = email;
    map['phone'] = phone;
    map['password'] = password;
    map['otp'] = otp;
    map['photo'] = photo;
    map['status'] = status;
    map['created_by'] = createdBy;
    map['updated_by'] = updatedBy;
    map['deleted_by'] = deletedBy;
    map['deleted_at'] = deletedAt;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    map['otp_expired_at'] = otpExpiredAt;
    map['otp_verified_at'] = otpVerifiedAt;
    return map;
  }

}

/// id : 2
/// order_id : 3
/// package_id : 1
/// count : 0
/// price : "250.00"
/// total_price : "0.00"
/// discount_amount : null
/// total_discount_amount : null
/// created_at : "2024-06-20T05:32:38.000000Z"
/// updated_at : "2024-06-20T05:32:38.000000Z"
/// package : {"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/VuE0ivOvCMLJM93WdkgcDJzzCRQPd7sHwZFruiJJ.png","type":2,"price":"250.00","discount":"10.00","descriptions":"Please buy my product.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-05-25T17:53:06.000000Z"}

Items itemsFromJson(String str) => Items.fromJson(json.decode(str));
String itemsToJson(Items data) => json.encode(data.toJson());
class Items {
  Items({
      this.id, 
      this.orderId, 
      this.packageId, 
      this.count, 
      this.price, 
      this.totalPrice, 
      this.discountAmount, 
      this.totalDiscountAmount, 
      this.createdAt, 
      this.updatedAt, 
      this.package,});

  Items.fromJson(dynamic json) {
    id = json['id'];
    orderId = json['order_id'];
    packageId = json['package_id'];
    count = json['count'];
    price = json['price'];
    totalPrice = json['total_price'];
    discountAmount = json['discount_amount'];
    totalDiscountAmount = json['total_discount_amount'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    package = json['package'] != null ? Package.fromJson(json['package']) : null;
  }
  num? id;
  num? orderId;
  num? packageId;
  num? count;
  String? price;
  String? totalPrice;
  dynamic discountAmount;
  dynamic totalDiscountAmount;
  String? createdAt;
  String? updatedAt;
  Package? package;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['order_id'] = orderId;
    map['package_id'] = packageId;
    map['count'] = count;
    map['price'] = price;
    map['total_price'] = totalPrice;
    map['discount_amount'] = discountAmount;
    map['total_discount_amount'] = totalDiscountAmount;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    if (package != null) {
      map['package'] = package?.toJson();
    }
    return map;
  }

}

/// id : 1
/// name : "6 Pics Chicken Leg"
/// thumbnail : "upload/image/packages/thumbnails/VuE0ivOvCMLJM93WdkgcDJzzCRQPd7sHwZFruiJJ.png"
/// type : 2
/// price : "250.00"
/// discount : "10.00"
/// descriptions : "Please buy my product."
/// status : 1
/// weight : "50"
/// weight_unit : "kg"
/// created_by : 1
/// updated_by : 1
/// deleted_by : null
/// deleted_at : null
/// created_at : "2024-05-13T18:51:49.000000Z"
/// updated_at : "2024-05-25T17:53:06.000000Z"

Package packageFromJson(String str) => Package.fromJson(json.decode(str));
String packageToJson(Package data) => json.encode(data.toJson());
class Package {
  Package({
      this.id, 
      this.name, 
      this.thumbnail, 
      this.type, 
      this.price, 
      this.discount, 
      this.descriptions, 
      this.status, 
      this.weight, 
      this.weightUnit, 
      this.createdBy, 
      this.updatedBy, 
      this.deletedBy, 
      this.deletedAt, 
      this.createdAt, 
      this.updatedAt,});

  Package.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    thumbnail = json['thumbnail'];
    type = json['type'];
    price = json['price'];
    discount = json['discount'];
    descriptions = json['descriptions'];
    status = json['status'];
    weight = json['weight'];
    weightUnit = json['weight_unit'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    deletedBy = json['deleted_by'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }
  num? id;
  String? name;
  String? thumbnail;
  num? type;
  String? price;
  String? discount;
  String? descriptions;
  num? status;
  String? weight;
  String? weightUnit;
  num? createdBy;
  num? updatedBy;
  dynamic deletedBy;
  dynamic deletedAt;
  String? createdAt;
  String? updatedAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['thumbnail'] = thumbnail;
    map['type'] = type;
    map['price'] = price;
    map['discount'] = discount;
    map['descriptions'] = descriptions;
    map['status'] = status;
    map['weight'] = weight;
    map['weight_unit'] = weightUnit;
    map['created_by'] = createdBy;
    map['updated_by'] = updatedBy;
    map['deleted_by'] = deletedBy;
    map['deleted_at'] = deletedAt;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    return map;
  }

}

/// success : true
/// code : 200
/// query_time : 0

ResponseStatus responseStatusFromJson(String str) => ResponseStatus.fromJson(json.decode(str));
String responseStatusToJson(ResponseStatus data) => json.encode(data.toJson());
class ResponseStatus {
  ResponseStatus({
      this.success, 
      this.code, 
      this.queryTime,});

  ResponseStatus.fromJson(dynamic json) {
    success = json['success'];
    code = json['code'];
    queryTime = json['query_time'];
  }
  bool? success;
  num? code;
  num? queryTime;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['success'] = success;
    map['code'] = code;
    map['query_time'] = queryTime;
    return map;
  }

}
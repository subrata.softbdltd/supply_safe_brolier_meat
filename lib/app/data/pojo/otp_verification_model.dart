import 'dart:convert';

/// _response_status : {"success":true,"code":200,"message":"Logged in successfully","query_time":0}
/// data : {"id":3,"role_id":2,"name":"Sujan Das","email":null,"phone":"01843599324","password":"","otp":null,"photo":"upload/image/farmers/mjWDk1nyyWClIKYyrgD6xHYwpOEn4l8xIkpr1ucV.jpg","status":1,"created_by":1,"updated_by":null,"deleted_by":null,"deleted_at":null,"created_at":"2024-03-28T20:56:56.000000Z","updated_at":"2024-03-28T22:09:50.000000Z","otp_expired_at":null,"otp_verified_at":"2024-03-29 04:09:49","token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiY2MwMzA3ZWI2YmUwZTg0YzBlY2U2YmRhNDBjNGQzZmIwZWU1MzVkNWZhMjQwMjVjOTA0OTI3MmQ4Zjk0NTMyMjRlNmRhY2VhMmEzMzk2YjUiLCJpYXQiOjE3MTE2NjM3OTAuNDEzNTksIm5iZiI6MTcxMTY2Mzc5MC40MTM1OTgsImV4cCI6MTc0MzE5OTc5MC4zOTU3MzEsInN1YiI6IjMiLCJzY29wZXMiOltdfQ.NFqXX3b1dD8lioHZXv1BHiCT1NLsp0zuXuWWxuSr2n-pe7Dy1MHE53BLjg-ZubkWQZnPZMWH5bDjgfI4vtgBEvrm_ASxsOBoqVcD4_rBRydDDkfRwtJPJkrd7oYQ7qzwO57m0hqIP_UAx-j2yUcTHeJEUMqZSH2Y9x7YjkOCbaFfaKcZRHi_z_BuI4dT4YjGaziGPJOqV7YqyGCmdA0wP7dKAEpe5Yw1iwcp29FbD-Td5eZPYOsDiB3H8B1N62b2QNeDmSzPMM31dlf7NjzfbRynohPALJq9lWFM-r1pidhWxkI5xfNikr-VY6HWeNQHRlma8Y6NcG_uap-7kerb92fGQ9a7q3PYvSDQP_jM4FBWosUOLsYq2aGmkTpz6fkI25PXyaUGbts2PAE2L8KRGyQmYNlNYwifb4gdToOD0TlKdxp5IRbFvz8oR-cfBrSi9LuiDPDg6Y_IUbHVfdGyixTELVkrZoL--NejLi8m29QGmwhCUQxwoIhu7PmccmWXazavC5frTwRSp8tPZ83rpuVzjB5_oXOm5YXhqmUNeRVmzRekLi52deUp1nY_Jk8pDwXEP5kqCI01UtrPFezV17NKhNWkqk5U6S3PNTVigtLEOWH6XL6wqjhwpxbma3j_Dbt9SU8wvEblisPu5JkURmbSie5rCg4QAxiFPSK60uo"}

OtpVerificationModel otpVerificationModelFromJson(String str) =>
    OtpVerificationModel.fromJson(json.decode(str));
String otpVerificationModelToJson(OtpVerificationModel data) =>
    json.encode(data.toJson());

class OtpVerificationModel {
  OtpVerificationModel({
    this.responseStatus,
    this.data,
  });

  OtpVerificationModel.fromJson(dynamic json) {
    responseStatus = json['_response_status'] != null
        ? ResponseStatus.fromJson(json['_response_status'])
        : null;
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  ResponseStatus? responseStatus;
  Data? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (responseStatus != null) {
      map['_response_status'] = responseStatus?.toJson();
    }
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }
}

/// id : 3
/// role_id : 2
/// name : "Sujan Das"
/// email : null
/// phone : "01843599324"
/// password : ""
/// otp : null
/// photo : "upload/image/farmers/mjWDk1nyyWClIKYyrgD6xHYwpOEn4l8xIkpr1ucV.jpg"
/// status : 1
/// created_by : 1
/// updated_by : null
/// deleted_by : null
/// deleted_at : null
/// created_at : "2024-03-28T20:56:56.000000Z"
/// updated_at : "2024-03-28T22:09:50.000000Z"
/// otp_expired_at : null
/// otp_verified_at : "2024-03-29 04:09:49"
/// token : "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiY2MwMzA3ZWI2YmUwZTg0YzBlY2U2YmRhNDBjNGQzZmIwZWU1MzVkNWZhMjQwMjVjOTA0OTI3MmQ4Zjk0NTMyMjRlNmRhY2VhMmEzMzk2YjUiLCJpYXQiOjE3MTE2NjM3OTAuNDEzNTksIm5iZiI6MTcxMTY2Mzc5MC40MTM1OTgsImV4cCI6MTc0MzE5OTc5MC4zOTU3MzEsInN1YiI6IjMiLCJzY29wZXMiOltdfQ.NFqXX3b1dD8lioHZXv1BHiCT1NLsp0zuXuWWxuSr2n-pe7Dy1MHE53BLjg-ZubkWQZnPZMWH5bDjgfI4vtgBEvrm_ASxsOBoqVcD4_rBRydDDkfRwtJPJkrd7oYQ7qzwO57m0hqIP_UAx-j2yUcTHeJEUMqZSH2Y9x7YjkOCbaFfaKcZRHi_z_BuI4dT4YjGaziGPJOqV7YqyGCmdA0wP7dKAEpe5Yw1iwcp29FbD-Td5eZPYOsDiB3H8B1N62b2QNeDmSzPMM31dlf7NjzfbRynohPALJq9lWFM-r1pidhWxkI5xfNikr-VY6HWeNQHRlma8Y6NcG_uap-7kerb92fGQ9a7q3PYvSDQP_jM4FBWosUOLsYq2aGmkTpz6fkI25PXyaUGbts2PAE2L8KRGyQmYNlNYwifb4gdToOD0TlKdxp5IRbFvz8oR-cfBrSi9LuiDPDg6Y_IUbHVfdGyixTELVkrZoL--NejLi8m29QGmwhCUQxwoIhu7PmccmWXazavC5frTwRSp8tPZ83rpuVzjB5_oXOm5YXhqmUNeRVmzRekLi52deUp1nY_Jk8pDwXEP5kqCI01UtrPFezV17NKhNWkqk5U6S3PNTVigtLEOWH6XL6wqjhwpxbma3j_Dbt9SU8wvEblisPu5JkURmbSie5rCg4QAxiFPSK60uo"

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    this.id,
    this.roleId,
    this.name,
    this.email,
    this.phone,
    this.password,
    this.otp,
    this.photo,
    this.status,
    this.createdBy,
    this.updatedBy,
    this.deletedBy,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
    this.otpExpiredAt,
    this.otpVerifiedAt,
    this.token,
  });

  Data.fromJson(dynamic json) {
    id = json['id'];
    roleId = json['role_id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    password = json['password'];
    otp = json['otp'];
    photo = json['photo'];
    status = json['status'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    deletedBy = json['deleted_by'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    otpExpiredAt = json['otp_expired_at'];
    otpVerifiedAt = json['otp_verified_at'];
    token = json['token'];
  }
  dynamic id;
  dynamic roleId;
  String? name;
  dynamic email;
  String? phone;
  String? password;
  dynamic otp;
  String? photo;
  dynamic status;
  dynamic createdBy;
  dynamic updatedBy;
  dynamic deletedBy;
  dynamic deletedAt;
  String? createdAt;
  String? updatedAt;
  dynamic otpExpiredAt;
  String? otpVerifiedAt;
  String? token;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['role_id'] = roleId;
    map['name'] = name;
    map['email'] = email;
    map['phone'] = phone;
    map['password'] = password;
    map['otp'] = otp;
    map['photo'] = photo;
    map['status'] = status;
    map['created_by'] = createdBy;
    map['updated_by'] = updatedBy;
    map['deleted_by'] = deletedBy;
    map['deleted_at'] = deletedAt;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    map['otp_expired_at'] = otpExpiredAt;
    map['otp_verified_at'] = otpVerifiedAt;
    map['token'] = token;
    return map;
  }
}

/// success : true
/// code : 200
/// message : "Logged in successfully"
/// query_time : 0

ResponseStatus responseStatusFromJson(String str) =>
    ResponseStatus.fromJson(json.decode(str));
String responseStatusToJson(ResponseStatus data) => json.encode(data.toJson());

class ResponseStatus {
  ResponseStatus({
    this.success,
    this.code,
    this.message,
    this.error,
    this.queryTime,
  });

  ResponseStatus.fromJson(dynamic json) {
    success = json['success'];
    code = json['code'];
    message = json['message'];
    error = json['error'];
    queryTime = json['query_time'];
  }
  bool? success;
  dynamic code;
  String? message;
  String? error;
  dynamic queryTime;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['success'] = success;
    map['code'] = code;
    map['message'] = message;
    map['error'] = error;
    map['query_time'] = queryTime;
    return map;
  }
}

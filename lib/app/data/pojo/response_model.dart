import 'dart:convert';

/// _response_status : {"success":true,"code":200,"message":"OTP Sent successfully.","query_time":0}
/// data : null

ResponseModel responseModelFromJson(String str) =>
    ResponseModel.fromJson(json.decode(str));
String responseModelToJson(ResponseModel data) => json.encode(data.toJson());

class ResponseModel {
  ResponseModel({
    this.responseStatus,
    this.data,
  });

  ResponseModel.fromJson(dynamic json) {
    responseStatus = json['_response_status'] != null
        ? ResponseStatus.fromJson(json['_response_status'])
        : null;
    data = json['data'];
  }
  ResponseStatus? responseStatus;
  dynamic data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (responseStatus != null) {
      map['_response_status'] = responseStatus?.toJson();
    }
    map['data'] = data;
    return map;
  }
}

/// success : true
/// code : 200
/// message : "OTP Sent successfully."
/// query_time : 0

ResponseStatus responseStatusFromJson(String str) =>
    ResponseStatus.fromJson(json.decode(str));
String responseStatusToJson(ResponseStatus data) => json.encode(data.toJson());

class ResponseStatus {
  ResponseStatus({
    this.success,
    this.code,
    this.message,
    this.error,
    this.queryTime,
  });

  ResponseStatus.fromJson(dynamic json) {
    success = json['success'];
    code = json['code'];
    message = json['message'];
    error = json['error'];
    queryTime = json['query_time'];
  }
  bool? success;
  num? code;
  String? message;
  String? error;
  num? queryTime;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['success'] = success;
    map['code'] = code;
    map['message'] = message;
    map['error'] = error;
    map['query_time'] = queryTime;
    return map;
  }
}

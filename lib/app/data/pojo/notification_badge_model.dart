import 'dart:convert';
/// _response_status : {"success":true,"code":200,"message":"Success","query_time":0}
/// data : {"count":3}

NotificationBadgeModel notificationBadgeModelFromJson(String str) => NotificationBadgeModel.fromJson(json.decode(str));
String notificationBadgeModelToJson(NotificationBadgeModel data) => json.encode(data.toJson());
class NotificationBadgeModel {
  NotificationBadgeModel({
      this.responseStatus, 
      this.data,});

  NotificationBadgeModel.fromJson(dynamic json) {
    responseStatus = json['_response_status'] != null ? ResponseStatus.fromJson(json['_response_status']) : null;
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  ResponseStatus? responseStatus;
  Data? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (responseStatus != null) {
      map['_response_status'] = responseStatus?.toJson();
    }
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }

}

/// count : 3

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());
class Data {
  Data({
      this.count,});

  Data.fromJson(dynamic json) {
    count = json['count'];
  }
  num? count;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['count'] = count;
    return map;
  }

}

/// success : true
/// code : 200
/// message : "Success"
/// query_time : 0

ResponseStatus responseStatusFromJson(String str) => ResponseStatus.fromJson(json.decode(str));
String responseStatusToJson(ResponseStatus data) => json.encode(data.toJson());
class ResponseStatus {
  ResponseStatus({
      this.success, 
      this.code, 
      this.message, 
      this.queryTime,});

  ResponseStatus.fromJson(dynamic json) {
    success = json['success'];
    code = json['code'];
    message = json['message'];
    queryTime = json['query_time'];
  }
  bool? success;
  num? code;
  String? message;
  num? queryTime;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['success'] = success;
    map['code'] = code;
    map['message'] = message;
    map['query_time'] = queryTime;
    return map;
  }

}
import 'dart:convert';

/// _response_status : {"success":true,"code":200,"message":"Success","error":"Unauthorized! Login First!","query_time":1}
/// data : {"id":3,"role_id":2,"name":"Sujan Das","email":null,"phone":"01843599324","password":"","otp":null,"photo":"upload/image/farmers/mjWDk1nyyWClIKYyrgD6xHYwpOEn4l8xIkpr1ucV.jpg","status":1,"created_by":1,"updated_by":null,"deleted_by":null,"deleted_at":null,"created_at":"2024-03-28T20:56:56.000000Z","updated_at":"2024-03-28T22:09:50.000000Z","otp_expired_at":null,"otp_verified_at":"2024-03-29 04:09:49"}

UserDetailsModel userDetailsModelFromJson(String str) =>
    UserDetailsModel.fromJson(json.decode(str));
String userDetailsModelToJson(UserDetailsModel data) =>
    json.encode(data.toJson());

class UserDetailsModel {
  UserDetailsModel({
    this.responseStatus,
    this.data,
  });

  UserDetailsModel.fromJson(dynamic json) {
    responseStatus = json['_response_status'] != null
        ? ResponseStatus.fromJson(json['_response_status'])
        : null;
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  ResponseStatus? responseStatus;
  Data? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (responseStatus != null) {
      map['_response_status'] = responseStatus?.toJson();
    }
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }
}

/// id : 3
/// role_id : 2
/// name : "Sujan Das"
/// email : null
/// phone : "01843599324"
/// password : ""
/// otp : null
/// photo : "upload/image/farmers/mjWDk1nyyWClIKYyrgD6xHYwpOEn4l8xIkpr1ucV.jpg"
/// status : 1
/// created_by : 1
/// updated_by : null
/// deleted_by : null
/// deleted_at : null
/// created_at : "2024-03-28T20:56:56.000000Z"
/// updated_at : "2024-03-28T22:09:50.000000Z"
/// otp_expired_at : null
/// otp_verified_at : "2024-03-29 04:09:49"

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    this.id,
    this.roleId,
    this.name,
    this.email,
    this.phone,
    this.password,
    this.otp,
    this.photo,
    this.status,
    this.createdBy,
    this.updatedBy,
    this.deletedBy,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
    this.otpExpiredAt,
    this.otpVerifiedAt,
  });

  Data.fromJson(dynamic json) {
    id = json['id'];
    roleId = json['role_id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    password = json['password'];
    otp = json['otp'];
    photo = json['photo'];
    status = json['status'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    deletedBy = json['deleted_by'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    otpExpiredAt = json['otp_expired_at'];
    otpVerifiedAt = json['otp_verified_at'];
  }
  dynamic id;
  dynamic roleId;
  String? name;
  dynamic email;
  String? phone;
  String? password;
  dynamic otp;
  String? photo;
  dynamic status;
  dynamic createdBy;
  dynamic updatedBy;
  dynamic deletedBy;
  dynamic deletedAt;
  String? createdAt;
  String? updatedAt;
  dynamic otpExpiredAt;
  String? otpVerifiedAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['role_id'] = roleId;
    map['name'] = name;
    map['email'] = email;
    map['phone'] = phone;
    map['password'] = password;
    map['otp'] = otp;
    map['photo'] = photo;
    map['status'] = status;
    map['created_by'] = createdBy;
    map['updated_by'] = updatedBy;
    map['deleted_by'] = deletedBy;
    map['deleted_at'] = deletedAt;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    map['otp_expired_at'] = otpExpiredAt;
    map['otp_verified_at'] = otpVerifiedAt;
    return map;
  }
}

/// success : true
/// code : 200
/// message : "Success"
/// error : "Unauthorized! Login First!"
/// query_time : 1

ResponseStatus responseStatusFromJson(String str) =>
    ResponseStatus.fromJson(json.decode(str));
String responseStatusToJson(ResponseStatus data) => json.encode(data.toJson());

class ResponseStatus {
  ResponseStatus({
    this.success,
    this.code,
    this.message,
    this.error,
    this.queryTime,
  });

  ResponseStatus.fromJson(dynamic json) {
    success = json['success'];
    code = json['code'];
    message = json['message'];
    error = json['error'];
    queryTime = json['query_time'];
  }
  bool? success;
  dynamic code;
  String? message;
  String? error;
  dynamic queryTime;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['success'] = success;
    map['code'] = code;
    map['message'] = message;
    map['error'] = error;
    map['query_time'] = queryTime;
    return map;
  }
}

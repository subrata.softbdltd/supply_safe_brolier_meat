import 'dart:convert';
/// current_page : 1
/// total_page : 1
/// page_size : 10
/// total : 7
/// order : "DESC"
/// _response_status : {"success":true,"code":200,"query_time":0}
/// data : [{"id":18,"order_no":"c181444c-9bcb-4038-884c-951d66bcc45c","customer_id":24,"outlet_id":1,"amount":"620.00","payable_amount":"590.00","discount_amount":"30.00","delivery_charge":"0.00","status":1,"payment_type":2,"delivery_type":1,"created_user":24,"date":"2024-07-26 23:05:20","delivery_address":null,"created_by":null,"updated_by":null,"created_at":"2024-07-26T17:05:20.000000Z","updated_at":"2024-07-26T17:05:20.000000Z","items":[{"id":26,"order_id":18,"package_id":1,"count":0,"price":"250.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-07-26T17:05:20.000000Z","updated_at":"2024-07-26T17:05:20.000000Z","package":{"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/16sYn00p5zQ9reD90QCe1pa0Sxm9rxoBjD2aVeWE.jpg","type":2,"price":"250.00","discount":"10.00","descriptions":"Lorem ipsum dolor sit amet, consectetur adipiscing elit,\r\nsed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\r\n\r\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut\r\naliquip ex ea commodo consequat.\r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum\r\ndolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,\r\nsunt in culpa qui officia deserunt mollit anim id est laborum.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-07-06T16:00:24.000000Z"}},{"id":27,"order_id":18,"package_id":2,"count":0,"price":"120.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-07-26T17:05:20.000000Z","updated_at":"2024-07-26T17:05:20.000000Z","package":{"id":2,"name":"New Chicken","thumbnail":"upload/image/packages/thumbnails/8my6OSooC93P2KO3uRCS3wyFkaPs5PQMMeOp1c4H.png","type":2,"price":"120.00","discount":"10.00","descriptions":"New pics","status":1,"weight":"8","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-19T17:33:57.000000Z","updated_at":"2024-05-25T17:48:19.000000Z"}}],"customer":{"id":24,"role_id":5,"name":"Guest","email":null,"phone":"01975962961","password":"","otp":null,"photo":null,"status":1,"created_by":null,"updated_by":null,"deleted_by":null,"deleted_at":null,"created_at":"2024-07-25T17:23:08.000000Z","updated_at":"2024-07-25T17:24:43.000000Z","otp_expired_at":null,"otp_verified_at":"2024-07-25 23:24:43"}},{"id":17,"order_no":"aa011ea1-7e2e-46d7-94cc-bf44d3edb44f","customer_id":24,"outlet_id":1,"amount":"620.00","payable_amount":"590.00","discount_amount":"30.00","delivery_charge":"0.00","status":1,"payment_type":2,"delivery_type":1,"created_user":24,"date":"2024-07-26 20:44:43","delivery_address":null,"created_by":null,"updated_by":null,"created_at":"2024-07-26T14:44:43.000000Z","updated_at":"2024-07-26T14:44:43.000000Z","items":[{"id":24,"order_id":17,"package_id":1,"count":0,"price":"250.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-07-26T14:44:43.000000Z","updated_at":"2024-07-26T14:44:43.000000Z","package":{"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/16sYn00p5zQ9reD90QCe1pa0Sxm9rxoBjD2aVeWE.jpg","type":2,"price":"250.00","discount":"10.00","descriptions":"Lorem ipsum dolor sit amet, consectetur adipiscing elit,\r\nsed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\r\n\r\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut\r\naliquip ex ea commodo consequat.\r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum\r\ndolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,\r\nsunt in culpa qui officia deserunt mollit anim id est laborum.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-07-06T16:00:24.000000Z"}},{"id":25,"order_id":17,"package_id":2,"count":0,"price":"120.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-07-26T14:44:43.000000Z","updated_at":"2024-07-26T14:44:43.000000Z","package":{"id":2,"name":"New Chicken","thumbnail":"upload/image/packages/thumbnails/8my6OSooC93P2KO3uRCS3wyFkaPs5PQMMeOp1c4H.png","type":2,"price":"120.00","discount":"10.00","descriptions":"New pics","status":1,"weight":"8","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-19T17:33:57.000000Z","updated_at":"2024-05-25T17:48:19.000000Z"}}],"customer":{"id":24,"role_id":5,"name":"Guest","email":null,"phone":"01975962961","password":"","otp":null,"photo":null,"status":1,"created_by":null,"updated_by":null,"deleted_by":null,"deleted_at":null,"created_at":"2024-07-25T17:23:08.000000Z","updated_at":"2024-07-25T17:24:43.000000Z","otp_expired_at":null,"otp_verified_at":"2024-07-25 23:24:43"}},{"id":16,"order_no":"ca454d70-7732-43f3-b27a-e069e379cca8","customer_id":24,"outlet_id":1,"amount":"500.00","payable_amount":"480.00","discount_amount":"20.00","delivery_charge":"0.00","status":1,"payment_type":2,"delivery_type":1,"created_user":24,"date":"2024-07-26 20:43:09","delivery_address":null,"created_by":null,"updated_by":null,"created_at":"2024-07-26T14:43:09.000000Z","updated_at":"2024-07-26T14:43:09.000000Z","items":[{"id":23,"order_id":16,"package_id":1,"count":0,"price":"250.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-07-26T14:43:09.000000Z","updated_at":"2024-07-26T14:43:09.000000Z","package":{"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/16sYn00p5zQ9reD90QCe1pa0Sxm9rxoBjD2aVeWE.jpg","type":2,"price":"250.00","discount":"10.00","descriptions":"Lorem ipsum dolor sit amet, consectetur adipiscing elit,\r\nsed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\r\n\r\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut\r\naliquip ex ea commodo consequat.\r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum\r\ndolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,\r\nsunt in culpa qui officia deserunt mollit anim id est laborum.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-07-06T16:00:24.000000Z"}}],"customer":{"id":24,"role_id":5,"name":"Guest","email":null,"phone":"01975962961","password":"","otp":null,"photo":null,"status":1,"created_by":null,"updated_by":null,"deleted_by":null,"deleted_at":null,"created_at":"2024-07-25T17:23:08.000000Z","updated_at":"2024-07-25T17:24:43.000000Z","otp_expired_at":null,"otp_verified_at":"2024-07-25 23:24:43"}},{"id":15,"order_no":"1b81d78e-a192-49fe-bacd-83568786d9d3","customer_id":24,"outlet_id":1,"amount":"500.00","payable_amount":"480.00","discount_amount":"20.00","delivery_charge":"0.00","status":1,"payment_type":2,"delivery_type":1,"created_user":24,"date":"2024-07-26 20:42:41","delivery_address":null,"created_by":null,"updated_by":null,"created_at":"2024-07-26T14:42:41.000000Z","updated_at":"2024-07-26T14:42:41.000000Z","items":[{"id":22,"order_id":15,"package_id":1,"count":0,"price":"250.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-07-26T14:42:41.000000Z","updated_at":"2024-07-26T14:42:41.000000Z","package":{"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/16sYn00p5zQ9reD90QCe1pa0Sxm9rxoBjD2aVeWE.jpg","type":2,"price":"250.00","discount":"10.00","descriptions":"Lorem ipsum dolor sit amet, consectetur adipiscing elit,\r\nsed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\r\n\r\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut\r\naliquip ex ea commodo consequat.\r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum\r\ndolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,\r\nsunt in culpa qui officia deserunt mollit anim id est laborum.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-07-06T16:00:24.000000Z"}}],"customer":{"id":24,"role_id":5,"name":"Guest","email":null,"phone":"01975962961","password":"","otp":null,"photo":null,"status":1,"created_by":null,"updated_by":null,"deleted_by":null,"deleted_at":null,"created_at":"2024-07-25T17:23:08.000000Z","updated_at":"2024-07-25T17:24:43.000000Z","otp_expired_at":null,"otp_verified_at":"2024-07-25 23:24:43"}},{"id":14,"order_no":"d4adf123-4715-4703-a87c-7da8b6fa0645","customer_id":24,"outlet_id":1,"amount":"500.00","payable_amount":"480.00","discount_amount":"20.00","delivery_charge":"0.00","status":1,"payment_type":2,"delivery_type":1,"created_user":24,"date":"2024-07-26 20:41:04","delivery_address":null,"created_by":null,"updated_by":null,"created_at":"2024-07-26T14:41:04.000000Z","updated_at":"2024-07-26T14:41:04.000000Z","items":[{"id":21,"order_id":14,"package_id":1,"count":0,"price":"250.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-07-26T14:41:04.000000Z","updated_at":"2024-07-26T14:41:04.000000Z","package":{"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/16sYn00p5zQ9reD90QCe1pa0Sxm9rxoBjD2aVeWE.jpg","type":2,"price":"250.00","discount":"10.00","descriptions":"Lorem ipsum dolor sit amet, consectetur adipiscing elit,\r\nsed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\r\n\r\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut\r\naliquip ex ea commodo consequat.\r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum\r\ndolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,\r\nsunt in culpa qui officia deserunt mollit anim id est laborum.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-07-06T16:00:24.000000Z"}}],"customer":{"id":24,"role_id":5,"name":"Guest","email":null,"phone":"01975962961","password":"","otp":null,"photo":null,"status":1,"created_by":null,"updated_by":null,"deleted_by":null,"deleted_at":null,"created_at":"2024-07-25T17:23:08.000000Z","updated_at":"2024-07-25T17:24:43.000000Z","otp_expired_at":null,"otp_verified_at":"2024-07-25 23:24:43"}},{"id":13,"order_no":"1dd33b6f-b037-4017-bf9d-0ac49888b716","customer_id":24,"outlet_id":1,"amount":"500.00","payable_amount":"480.00","discount_amount":"20.00","delivery_charge":"0.00","status":1,"payment_type":2,"delivery_type":1,"created_user":24,"date":"2024-07-26 20:40:41","delivery_address":null,"created_by":null,"updated_by":null,"created_at":"2024-07-26T14:40:41.000000Z","updated_at":"2024-07-26T14:40:41.000000Z","items":[{"id":20,"order_id":13,"package_id":1,"count":0,"price":"250.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-07-26T14:40:41.000000Z","updated_at":"2024-07-26T14:40:41.000000Z","package":{"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/16sYn00p5zQ9reD90QCe1pa0Sxm9rxoBjD2aVeWE.jpg","type":2,"price":"250.00","discount":"10.00","descriptions":"Lorem ipsum dolor sit amet, consectetur adipiscing elit,\r\nsed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\r\n\r\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut\r\naliquip ex ea commodo consequat.\r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum\r\ndolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,\r\nsunt in culpa qui officia deserunt mollit anim id est laborum.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-07-06T16:00:24.000000Z"}}],"customer":{"id":24,"role_id":5,"name":"Guest","email":null,"phone":"01975962961","password":"","otp":null,"photo":null,"status":1,"created_by":null,"updated_by":null,"deleted_by":null,"deleted_at":null,"created_at":"2024-07-25T17:23:08.000000Z","updated_at":"2024-07-25T17:24:43.000000Z","otp_expired_at":null,"otp_verified_at":"2024-07-25 23:24:43"}},{"id":12,"order_no":"7c066d55-c80d-4cb8-b507-deb97b82e347","customer_id":24,"outlet_id":1,"amount":"500.00","payable_amount":"480.00","discount_amount":"20.00","delivery_charge":"0.00","status":1,"payment_type":2,"delivery_type":1,"created_user":24,"date":"2024-07-26 17:17:08","delivery_address":null,"created_by":null,"updated_by":null,"created_at":"2024-07-26T11:17:08.000000Z","updated_at":"2024-07-26T11:17:08.000000Z","items":[{"id":19,"order_id":12,"package_id":1,"count":0,"price":"250.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-07-26T11:17:08.000000Z","updated_at":"2024-07-26T11:17:08.000000Z","package":{"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/16sYn00p5zQ9reD90QCe1pa0Sxm9rxoBjD2aVeWE.jpg","type":2,"price":"250.00","discount":"10.00","descriptions":"Lorem ipsum dolor sit amet, consectetur adipiscing elit,\r\nsed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\r\n\r\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut\r\naliquip ex ea commodo consequat.\r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum\r\ndolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,\r\nsunt in culpa qui officia deserunt mollit anim id est laborum.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-07-06T16:00:24.000000Z"}}],"customer":{"id":24,"role_id":5,"name":"Guest","email":null,"phone":"01975962961","password":"","otp":null,"photo":null,"status":1,"created_by":null,"updated_by":null,"deleted_by":null,"deleted_at":null,"created_at":"2024-07-25T17:23:08.000000Z","updated_at":"2024-07-25T17:24:43.000000Z","otp_expired_at":null,"otp_verified_at":"2024-07-25 23:24:43"}}]

OrderDataModel orderDataModelFromJson(String str) => OrderDataModel.fromJson(json.decode(str));
String orderDataModelToJson(OrderDataModel data) => json.encode(data.toJson());
class OrderDataModel {
  OrderDataModel({
      this.currentPage, 
      this.totalPage, 
      this.pageSize, 
      this.total, 
      this.order, 
      this.responseStatus, 
      this.data,});

  OrderDataModel.fromJson(dynamic json) {
    currentPage = json['current_page'];
    totalPage = json['total_page'];
    pageSize = json['page_size'];
    total = json['total'];
    order = json['order'];
    responseStatus = json['_response_status'] != null ? ResponseStatus.fromJson(json['_response_status']) : null;
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(Data.fromJson(v));
      });
    }
  }
  num? currentPage;
  num? totalPage;
  num? pageSize;
  num? total;
  String? order;
  ResponseStatus? responseStatus;
  List<Data>? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['current_page'] = currentPage;
    map['total_page'] = totalPage;
    map['page_size'] = pageSize;
    map['total'] = total;
    map['order'] = order;
    if (responseStatus != null) {
      map['_response_status'] = responseStatus?.toJson();
    }
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 18
/// order_no : "c181444c-9bcb-4038-884c-951d66bcc45c"
/// customer_id : 24
/// outlet_id : 1
/// amount : "620.00"
/// payable_amount : "590.00"
/// discount_amount : "30.00"
/// delivery_charge : "0.00"
/// status : 1
/// payment_type : 2
/// delivery_type : 1
/// created_user : 24
/// date : "2024-07-26 23:05:20"
/// delivery_address : null
/// created_by : null
/// updated_by : null
/// created_at : "2024-07-26T17:05:20.000000Z"
/// updated_at : "2024-07-26T17:05:20.000000Z"
/// items : [{"id":26,"order_id":18,"package_id":1,"count":0,"price":"250.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-07-26T17:05:20.000000Z","updated_at":"2024-07-26T17:05:20.000000Z","package":{"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/16sYn00p5zQ9reD90QCe1pa0Sxm9rxoBjD2aVeWE.jpg","type":2,"price":"250.00","discount":"10.00","descriptions":"Lorem ipsum dolor sit amet, consectetur adipiscing elit,\r\nsed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\r\n\r\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut\r\naliquip ex ea commodo consequat.\r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum\r\ndolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,\r\nsunt in culpa qui officia deserunt mollit anim id est laborum.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-07-06T16:00:24.000000Z"}},{"id":27,"order_id":18,"package_id":2,"count":0,"price":"120.00","total_price":"0.00","discount_amount":null,"total_discount_amount":null,"created_at":"2024-07-26T17:05:20.000000Z","updated_at":"2024-07-26T17:05:20.000000Z","package":{"id":2,"name":"New Chicken","thumbnail":"upload/image/packages/thumbnails/8my6OSooC93P2KO3uRCS3wyFkaPs5PQMMeOp1c4H.png","type":2,"price":"120.00","discount":"10.00","descriptions":"New pics","status":1,"weight":"8","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-19T17:33:57.000000Z","updated_at":"2024-05-25T17:48:19.000000Z"}}]
/// customer : {"id":24,"role_id":5,"name":"Guest","email":null,"phone":"01975962961","password":"","otp":null,"photo":null,"status":1,"created_by":null,"updated_by":null,"deleted_by":null,"deleted_at":null,"created_at":"2024-07-25T17:23:08.000000Z","updated_at":"2024-07-25T17:24:43.000000Z","otp_expired_at":null,"otp_verified_at":"2024-07-25 23:24:43"}

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());
class Data {
  Data({
      this.id, 
      this.orderNo, 
      this.customerId, 
      this.outletId, 
      this.amount, 
      this.payableAmount, 
      this.discountAmount, 
      this.deliveryCharge, 
      this.status, 
      this.paymentType, 
      this.deliveryType, 
      this.createdUser, 
      this.date, 
      this.deliveryAddress, 
      this.createdBy, 
      this.updatedBy, 
      this.createdAt, 
      this.updatedAt, 
      this.items, 
      this.customer,});

  Data.fromJson(dynamic json) {
    id = json['id'];
    orderNo = json['order_no'];
    customerId = json['customer_id'];
    outletId = json['outlet_id'];
    amount = json['amount'];
    payableAmount = json['payable_amount'];
    discountAmount = json['discount_amount'];
    deliveryCharge = json['delivery_charge'];
    status = json['status'];
    paymentType = json['payment_type'];
    deliveryType = json['delivery_type'];
    createdUser = json['created_user'];
    date = json['date'];
    deliveryAddress = json['delivery_address'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['items'] != null) {
      items = [];
      json['items'].forEach((v) {
        items?.add(Items.fromJson(v));
      });
    }
    customer = json['customer'] != null ? Customer.fromJson(json['customer']) : null;
  }
  num? id;
  String? orderNo;
  num? customerId;
  num? outletId;
  String? amount;
  String? payableAmount;
  String? discountAmount;
  String? deliveryCharge;
  num? status;
  num? paymentType;
  num? deliveryType;
  num? createdUser;
  String? date;
  dynamic deliveryAddress;
  dynamic createdBy;
  dynamic updatedBy;
  String? createdAt;
  String? updatedAt;
  List<Items>? items;
  Customer? customer;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['order_no'] = orderNo;
    map['customer_id'] = customerId;
    map['outlet_id'] = outletId;
    map['amount'] = amount;
    map['payable_amount'] = payableAmount;
    map['discount_amount'] = discountAmount;
    map['delivery_charge'] = deliveryCharge;
    map['status'] = status;
    map['payment_type'] = paymentType;
    map['delivery_type'] = deliveryType;
    map['created_user'] = createdUser;
    map['date'] = date;
    map['delivery_address'] = deliveryAddress;
    map['created_by'] = createdBy;
    map['updated_by'] = updatedBy;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    if (items != null) {
      map['items'] = items?.map((v) => v.toJson()).toList();
    }
    if (customer != null) {
      map['customer'] = customer?.toJson();
    }
    return map;
  }

}

/// id : 24
/// role_id : 5
/// name : "Guest"
/// email : null
/// phone : "01975962961"
/// password : ""
/// otp : null
/// photo : null
/// status : 1
/// created_by : null
/// updated_by : null
/// deleted_by : null
/// deleted_at : null
/// created_at : "2024-07-25T17:23:08.000000Z"
/// updated_at : "2024-07-25T17:24:43.000000Z"
/// otp_expired_at : null
/// otp_verified_at : "2024-07-25 23:24:43"

Customer customerFromJson(String str) => Customer.fromJson(json.decode(str));
String customerToJson(Customer data) => json.encode(data.toJson());
class Customer {
  Customer({
      this.id, 
      this.roleId, 
      this.name, 
      this.email, 
      this.phone, 
      this.password, 
      this.otp, 
      this.photo, 
      this.status, 
      this.createdBy, 
      this.updatedBy, 
      this.deletedBy, 
      this.deletedAt, 
      this.createdAt, 
      this.updatedAt, 
      this.otpExpiredAt, 
      this.otpVerifiedAt,});

  Customer.fromJson(dynamic json) {
    id = json['id'];
    roleId = json['role_id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    password = json['password'];
    otp = json['otp'];
    photo = json['photo'];
    status = json['status'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    deletedBy = json['deleted_by'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    otpExpiredAt = json['otp_expired_at'];
    otpVerifiedAt = json['otp_verified_at'];
  }
  num? id;
  num? roleId;
  String? name;
  dynamic email;
  String? phone;
  String? password;
  dynamic otp;
  dynamic photo;
  num? status;
  dynamic createdBy;
  dynamic updatedBy;
  dynamic deletedBy;
  dynamic deletedAt;
  String? createdAt;
  String? updatedAt;
  dynamic otpExpiredAt;
  String? otpVerifiedAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['role_id'] = roleId;
    map['name'] = name;
    map['email'] = email;
    map['phone'] = phone;
    map['password'] = password;
    map['otp'] = otp;
    map['photo'] = photo;
    map['status'] = status;
    map['created_by'] = createdBy;
    map['updated_by'] = updatedBy;
    map['deleted_by'] = deletedBy;
    map['deleted_at'] = deletedAt;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    map['otp_expired_at'] = otpExpiredAt;
    map['otp_verified_at'] = otpVerifiedAt;
    return map;
  }

}

/// id : 26
/// order_id : 18
/// package_id : 1
/// count : 0
/// price : "250.00"
/// total_price : "0.00"
/// discount_amount : null
/// total_discount_amount : null
/// created_at : "2024-07-26T17:05:20.000000Z"
/// updated_at : "2024-07-26T17:05:20.000000Z"
/// package : {"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/16sYn00p5zQ9reD90QCe1pa0Sxm9rxoBjD2aVeWE.jpg","type":2,"price":"250.00","discount":"10.00","descriptions":"Lorem ipsum dolor sit amet, consectetur adipiscing elit,\r\nsed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\r\n\r\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut\r\naliquip ex ea commodo consequat.\r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum\r\ndolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,\r\nsunt in culpa qui officia deserunt mollit anim id est laborum.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-07-06T16:00:24.000000Z"}

Items itemsFromJson(String str) => Items.fromJson(json.decode(str));
String itemsToJson(Items data) => json.encode(data.toJson());
class Items {
  Items({
      this.id, 
      this.orderId, 
      this.packageId, 
      this.count, 
      this.price, 
      this.totalPrice, 
      this.discountAmount, 
      this.totalDiscountAmount, 
      this.createdAt, 
      this.updatedAt, 
      this.package,});

  Items.fromJson(dynamic json) {
    id = json['id'];
    orderId = json['order_id'];
    packageId = json['package_id'];
    count = json['count'];
    price = json['price'];
    totalPrice = json['total_price'];
    discountAmount = json['discount_amount'];
    totalDiscountAmount = json['total_discount_amount'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    package = json['package'] != null ? Package.fromJson(json['package']) : null;
  }
  num? id;
  num? orderId;
  num? packageId;
  num? count;
  String? price;
  String? totalPrice;
  dynamic discountAmount;
  dynamic totalDiscountAmount;
  String? createdAt;
  String? updatedAt;
  Package? package;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['order_id'] = orderId;
    map['package_id'] = packageId;
    map['count'] = count;
    map['price'] = price;
    map['total_price'] = totalPrice;
    map['discount_amount'] = discountAmount;
    map['total_discount_amount'] = totalDiscountAmount;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    if (package != null) {
      map['package'] = package?.toJson();
    }
    return map;
  }

}

/// id : 1
/// name : "6 Pics Chicken Leg"
/// thumbnail : "upload/image/packages/thumbnails/16sYn00p5zQ9reD90QCe1pa0Sxm9rxoBjD2aVeWE.jpg"
/// type : 2
/// price : "250.00"
/// discount : "10.00"
/// descriptions : "Lorem ipsum dolor sit amet, consectetur adipiscing elit,\r\nsed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\r\n\r\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut\r\naliquip ex ea commodo consequat.\r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum\r\ndolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,\r\nsunt in culpa qui officia deserunt mollit anim id est laborum."
/// status : 1
/// weight : "50"
/// weight_unit : "kg"
/// created_by : 1
/// updated_by : 1
/// deleted_by : null
/// deleted_at : null
/// created_at : "2024-05-13T18:51:49.000000Z"
/// updated_at : "2024-07-06T16:00:24.000000Z"

Package packageFromJson(String str) => Package.fromJson(json.decode(str));
String packageToJson(Package data) => json.encode(data.toJson());
class Package {
  Package({
      this.id, 
      this.name, 
      this.thumbnail, 
      this.type, 
      this.price, 
      this.discount, 
      this.descriptions, 
      this.status, 
      this.weight, 
      this.weightUnit, 
      this.createdBy, 
      this.updatedBy, 
      this.deletedBy, 
      this.deletedAt, 
      this.createdAt, 
      this.updatedAt,});

  Package.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    thumbnail = json['thumbnail'];
    type = json['type'];
    price = json['price'];
    discount = json['discount'];
    descriptions = json['descriptions'];
    status = json['status'];
    weight = json['weight'];
    weightUnit = json['weight_unit'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    deletedBy = json['deleted_by'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }
  num? id;
  String? name;
  String? thumbnail;
  num? type;
  String? price;
  String? discount;
  String? descriptions;
  num? status;
  String? weight;
  String? weightUnit;
  num? createdBy;
  num? updatedBy;
  dynamic deletedBy;
  dynamic deletedAt;
  String? createdAt;
  String? updatedAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['thumbnail'] = thumbnail;
    map['type'] = type;
    map['price'] = price;
    map['discount'] = discount;
    map['descriptions'] = descriptions;
    map['status'] = status;
    map['weight'] = weight;
    map['weight_unit'] = weightUnit;
    map['created_by'] = createdBy;
    map['updated_by'] = updatedBy;
    map['deleted_by'] = deletedBy;
    map['deleted_at'] = deletedAt;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    return map;
  }

}

/// success : true
/// code : 200
/// query_time : 0

ResponseStatus responseStatusFromJson(String str) => ResponseStatus.fromJson(json.decode(str));
String responseStatusToJson(ResponseStatus data) => json.encode(data.toJson());
class ResponseStatus {
  ResponseStatus({
      this.success, 
      this.code, 
      this.queryTime,});

  ResponseStatus.fromJson(dynamic json) {
    success = json['success'];
    code = json['code'];
    queryTime = json['query_time'];
  }
  bool? success;
  num? code;
  num? queryTime;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['success'] = success;
    map['code'] = code;
    map['query_time'] = queryTime;
    return map;
  }

}
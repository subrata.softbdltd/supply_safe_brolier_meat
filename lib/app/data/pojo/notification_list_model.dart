import 'dart:convert';
/// current_page : 1
/// total_page : 1
/// page_size : 10
/// total : 6
/// order : "desc"
/// _response_status : {"success":true,"code":200,"query_time":0}
/// data : [{"id":24,"title":"title","body":"body test","content":null,"receiver_entity_type":"all","receiver_entity_id":null,"link_label":"","link_url":"","created_at":"2024-09-26T20:20:03.000000Z","updated_at":"2024-09-26T20:20:03.000000Z","notice_read_at":null},{"id":23,"title":"title","body":"body test","content":null,"receiver_entity_type":"all","receiver_entity_id":null,"link_label":"","link_url":"","created_at":"2024-09-26T20:18:58.000000Z","updated_at":"2024-09-26T20:18:58.000000Z","notice_read_at":null},{"id":7,"title":"test","body":"test","content":"123","receiver_entity_type":"all","receiver_entity_id":null,"link_label":"","link_url":"","created_at":"2024-09-26T19:31:42.000000Z","updated_at":"2024-09-26T19:31:42.000000Z","notice_read_at":null},{"id":4,"title":"Test","body":"test","content":"","receiver_entity_type":"all","receiver_entity_id":null,"link_label":"","link_url":"","created_at":"2024-09-26T19:25:00.000000Z","updated_at":"2024-09-26T19:25:00.000000Z","notice_read_at":null},{"id":3,"title":"New Notice","body":"Test body","content":"test","receiver_entity_type":"consumer","receiver_entity_id":null,"link_label":"","link_url":"","created_at":"2024-09-26T09:01:43.000000Z","updated_at":"2024-09-26T09:01:43.000000Z","notice_read_at":null},{"id":1,"title":"title","body":"body test","content":null,"receiver_entity_type":"all","receiver_entity_id":null,"link_label":"","link_url":"","created_at":"2024-09-25T09:23:05.000000Z","updated_at":"2024-09-25T09:23:05.000000Z","notice_read_at":null}]

NotificationListModel notificationListModelFromJson(String str) => NotificationListModel.fromJson(json.decode(str));
String notificationListModelToJson(NotificationListModel data) => json.encode(data.toJson());
class NotificationListModel {
  NotificationListModel({
      this.currentPage, 
      this.totalPage, 
      this.pageSize, 
      this.total, 
      this.order, 
      this.responseStatus, 
      this.data,});

  NotificationListModel.fromJson(dynamic json) {
    currentPage = json['current_page'];
    totalPage = json['total_page'];
    pageSize = json['page_size'];
    total = json['total'];
    order = json['order'];
    responseStatus = json['_response_status'] != null ? ResponseStatus.fromJson(json['_response_status']) : null;
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(Data.fromJson(v));
      });
    }
  }
  num? currentPage;
  num? totalPage;
  num? pageSize;
  num? total;
  String? order;
  ResponseStatus? responseStatus;
  List<Data>? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['current_page'] = currentPage;
    map['total_page'] = totalPage;
    map['page_size'] = pageSize;
    map['total'] = total;
    map['order'] = order;
    if (responseStatus != null) {
      map['_response_status'] = responseStatus?.toJson();
    }
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 24
/// title : "title"
/// body : "body test"
/// content : null
/// receiver_entity_type : "all"
/// receiver_entity_id : null
/// link_label : ""
/// link_url : ""
/// created_at : "2024-09-26T20:20:03.000000Z"
/// updated_at : "2024-09-26T20:20:03.000000Z"
/// notice_read_at : null

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());
class Data {
  Data({
      this.id, 
      this.title, 
      this.body, 
      this.content, 
      this.receiverEntityType, 
      this.receiverEntityId, 
      this.linkLabel, 
      this.linkUrl, 
      this.createdAt, 
      this.updatedAt, 
      this.noticeReadAt,});

  Data.fromJson(dynamic json) {
    id = json['id'];
    title = json['title'];
    body = json['body'];
    content = json['content'];
    receiverEntityType = json['receiver_entity_type'];
    receiverEntityId = json['receiver_entity_id'];
    linkLabel = json['link_label'];
    linkUrl = json['link_url'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    noticeReadAt = json['notice_read_at'];
  }
  num? id;
  String? title;
  String? body;
  dynamic content;
  String? receiverEntityType;
  dynamic receiverEntityId;
  String? linkLabel;
  String? linkUrl;
  String? createdAt;
  String? updatedAt;
  dynamic noticeReadAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['title'] = title;
    map['body'] = body;
    map['content'] = content;
    map['receiver_entity_type'] = receiverEntityType;
    map['receiver_entity_id'] = receiverEntityId;
    map['link_label'] = linkLabel;
    map['link_url'] = linkUrl;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    map['notice_read_at'] = noticeReadAt;
    return map;
  }

}

/// success : true
/// code : 200
/// query_time : 0

ResponseStatus responseStatusFromJson(String str) => ResponseStatus.fromJson(json.decode(str));
String responseStatusToJson(ResponseStatus data) => json.encode(data.toJson());
class ResponseStatus {
  ResponseStatus({
      this.success, 
      this.code, 
      this.queryTime,});

  ResponseStatus.fromJson(dynamic json) {
    success = json['success'];
    code = json['code'];
    queryTime = json['query_time'];
  }
  bool? success;
  num? code;
  num? queryTime;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['success'] = success;
    map['code'] = code;
    map['query_time'] = queryTime;
    return map;
  }

}
import 'dart:convert';

/// _response_status : {"success":true,"code":200,"error":"Error message","query_time":0}
/// data : [{"farmer_id":1,"total_broiler":"100","total_weight":"20","farmer":{"id":1,"name":"Sujan Das","phone":"01843599324"}}]

PlantTransationHistory plantTransationHistoryFromJson(String str) =>
    PlantTransationHistory.fromJson(json.decode(str));
String plantTransationHistoryToJson(PlantTransationHistory data) =>
    json.encode(data.toJson());

class PlantTransationHistory {
  PlantTransationHistory({
    this.responseStatus,
    this.data,
  });

  PlantTransationHistory.fromJson(dynamic json) {
    responseStatus = json['_response_status'] != null
        ? ResponseStatus.fromJson(json['_response_status'])
        : null;
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(Data.fromJson(v));
      });
    }
  }
  ResponseStatus? responseStatus;
  List<Data>? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (responseStatus != null) {
      map['_response_status'] = responseStatus?.toJson();
    }
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// farmer_id : 1
/// total_broiler : "100"
/// total_weight : "20"
/// farmer : {"id":1,"name":"Sujan Das","phone":"01843599324"}

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    this.farmerId,
    this.totalBroiler,
    this.totalWeight,
    this.farmer,
  });

  Data.fromJson(dynamic json) {
    farmerId = json['farmer_id'];
    totalBroiler = json['total_broiler'];
    totalWeight = json['total_weight'];
    farmer = json['farmer'] != null ? Farmer.fromJson(json['farmer']) : null;
  }
  num? farmerId;
  String? totalBroiler;
  String? totalWeight;
  Farmer? farmer;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['farmer_id'] = farmerId;
    map['total_broiler'] = totalBroiler;
    map['total_weight'] = totalWeight;
    if (farmer != null) {
      map['farmer'] = farmer?.toJson();
    }
    return map;
  }
}

/// id : 1
/// name : "Sujan Das"
/// phone : "01843599324"

Farmer farmerFromJson(String str) => Farmer.fromJson(json.decode(str));
String farmerToJson(Farmer data) => json.encode(data.toJson());

class Farmer {
  Farmer({
    this.id,
    this.name,
    this.phone,
  });

  Farmer.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    phone = json['phone'];
  }
  num? id;
  String? name;
  String? phone;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['phone'] = phone;
    return map;
  }
}

/// success : true
/// code : 200
/// error : "Error message"
/// query_time : 0

ResponseStatus responseStatusFromJson(String str) =>
    ResponseStatus.fromJson(json.decode(str));
String responseStatusToJson(ResponseStatus data) => json.encode(data.toJson());

class ResponseStatus {
  ResponseStatus({
    this.success,
    this.code,
    this.error,
    this.queryTime,
  });

  ResponseStatus.fromJson(dynamic json) {
    success = json['success'];
    code = json['code'];
    error = json['error'];
    queryTime = json['query_time'];
  }
  bool? success;
  num? code;
  String? error;
  num? queryTime;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['success'] = success;
    map['code'] = code;
    map['error'] = error;
    map['query_time'] = queryTime;
    return map;
  }
}

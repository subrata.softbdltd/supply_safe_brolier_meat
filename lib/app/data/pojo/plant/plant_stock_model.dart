import 'dart:convert';

/// _response_status : {"success":true,"code":200,"message":"Success","query_time":1}
/// data : [{"id":1,"name":"6pcs wings","thumbnail":"upload/image/packages/thumbnails/I83x34u43Ao13GjBjy0nvYIo45W73PPHsdQvxFZT.png","type":3,"price":"150.00","discount":"0.00","weight":"510.00","weight_unit":"gram","stocks":{"id":1,"count":20,"package_id":1}},{"id":2,"name":"4pics wings","thumbnail":"upload/image/packages/thumbnails/xZjTaPEbQq4Vdb9P8IFOFBAMiSvYvtj590cBXX0z.png","type":3,"price":"150.00","discount":"0.00","weight":"500.00","weight_unit":"gram","stocks":null}]

PlantStockModel plantStockModelFromJson(String str) =>
    PlantStockModel.fromJson(json.decode(str));
String plantStockModelToJson(PlantStockModel data) =>
    json.encode(data.toJson());

class PlantStockModel {
  PlantStockModel({
    this.responseStatus,
    this.data,
  });

  PlantStockModel.fromJson(dynamic json) {
    responseStatus = json['_response_status'] != null
        ? ResponseStatus.fromJson(json['_response_status'])
        : null;
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(Data.fromJson(v));
      });
    }
  }
  ResponseStatus? responseStatus;
  List<Data>? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (responseStatus != null) {
      map['_response_status'] = responseStatus?.toJson();
    }
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 1
/// name : "6pcs wings"
/// thumbnail : "upload/image/packages/thumbnails/I83x34u43Ao13GjBjy0nvYIo45W73PPHsdQvxFZT.png"
/// type : 3
/// price : "150.00"
/// discount : "0.00"
/// weight : "510.00"
/// weight_unit : "gram"
/// stocks : {"id":1,"count":20,"package_id":1}

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    this.id,
    this.name,
    this.thumbnail,
    this.type,
    this.price,
    this.discount,
    this.weight,
    this.weightUnit,
    this.stocks,
  });

  Data.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    thumbnail = json['thumbnail'];
    type = json['type'];
    price = json['price'];
    discount = json['discount'];
    weight = json['weight'];
    weightUnit = json['weight_unit'];
    stocks = json['stocks'] != null ? Stocks.fromJson(json['stocks']) : null;
  }
  num? id;
  String? name;
  String? thumbnail;
  num? type;
  String? price;
  String? discount;
  String? weight;
  String? weightUnit;
  Stocks? stocks;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['thumbnail'] = thumbnail;
    map['type'] = type;
    map['price'] = price;
    map['discount'] = discount;
    map['weight'] = weight;
    map['weight_unit'] = weightUnit;
    if (stocks != null) {
      map['stocks'] = stocks?.toJson();
    }
    return map;
  }
}

/// id : 1
/// count : 20
/// package_id : 1

Stocks stocksFromJson(String str) => Stocks.fromJson(json.decode(str));
String stocksToJson(Stocks data) => json.encode(data.toJson());

class Stocks {
  Stocks({
    this.id,
    this.count,
    this.packageId,
  });

  Stocks.fromJson(dynamic json) {
    id = json['id'];
    count = json['count'];
    packageId = json['package_id'];
  }
  num? id;
  num? count;
  num? packageId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['count'] = count;
    map['package_id'] = packageId;
    return map;
  }
}

/// success : true
/// code : 200
/// message : "Success"
/// query_time : 1

ResponseStatus responseStatusFromJson(String str) =>
    ResponseStatus.fromJson(json.decode(str));
String responseStatusToJson(ResponseStatus data) => json.encode(data.toJson());

class ResponseStatus {
  ResponseStatus({
    this.success,
    this.code,
    this.message,
    this.error,
    this.queryTime,
  });

  ResponseStatus.fromJson(dynamic json) {
    success = json['success'];
    code = json['code'];
    message = json['message'];
    error = json['error'];
    queryTime = json['query_time'];
  }
  bool? success;
  num? code;
  String? message;
  String? error;
  num? queryTime;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['success'] = success;
    map['code'] = code;
    map['message'] = message;
    map['error'] = error;
    map['query_time'] = queryTime;
    return map;
  }
}

import 'dart:convert';
/// _response_status : {"success":true,"code":200,"query_time":0}
/// data : [{"id":3,"upload_by":2,"value":"upload/image/gallery/q1HtIbCHX2HLElbdKtn2pZAfjYM9R3eVIBW5QRZ4.jpg","deleted_at":null,"created_at":null},{"id":4,"upload_by":2,"value":"upload/image/gallery/QowSoFa0jyxdYxB08ra9ao3pbYbXxQVjcAltRTjQ.jpg","deleted_at":null,"created_at":null}]

PhotoGalleryModel photoGalleryModelFromJson(String str) => PhotoGalleryModel.fromJson(json.decode(str));
String photoGalleryModelToJson(PhotoGalleryModel data) => json.encode(data.toJson());
class PhotoGalleryModel {
  PhotoGalleryModel({
      this.responseStatus, 
      this.data,});

  PhotoGalleryModel.fromJson(dynamic json) {
    responseStatus = json['_response_status'] != null ? ResponseStatus.fromJson(json['_response_status']) : null;
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(Data.fromJson(v));
      });
    }
  }
  ResponseStatus? responseStatus;
  List<Data>? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (responseStatus != null) {
      map['_response_status'] = responseStatus?.toJson();
    }
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 3
/// upload_by : 2
/// value : "upload/image/gallery/q1HtIbCHX2HLElbdKtn2pZAfjYM9R3eVIBW5QRZ4.jpg"
/// deleted_at : null
/// created_at : null

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());
class Data {
  Data({
      this.id, 
      this.uploadBy, 
      this.value, 
      this.deletedAt, 
      this.createdAt,});

  Data.fromJson(dynamic json) {
    id = json['id'];
    uploadBy = json['upload_by'];
    value = json['value'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
  }
  num? id;
  num? uploadBy;
  String? value;
  dynamic deletedAt;
  dynamic createdAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['upload_by'] = uploadBy;
    map['value'] = value;
    map['deleted_at'] = deletedAt;
    map['created_at'] = createdAt;
    return map;
  }

}

/// success : true
/// code : 200
/// query_time : 0

ResponseStatus responseStatusFromJson(String str) => ResponseStatus.fromJson(json.decode(str));
String responseStatusToJson(ResponseStatus data) => json.encode(data.toJson());
class ResponseStatus {
  ResponseStatus({
      this.success, 
      this.code, 
      this.queryTime,});

  ResponseStatus.fromJson(dynamic json) {
    success = json['success'];
    code = json['code'];
    queryTime = json['query_time'];
  }
  bool? success;
  num? code;
  num? queryTime;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['success'] = success;
    map['code'] = code;
    map['query_time'] = queryTime;
    return map;
  }

}
import 'dart:convert';

/// _response_status : {"success":true,"code":200,"message":"Success","query_time":0}
/// data : {"id":1,"farmer_id":1,"batch_no":"BCH00001","total_broiler":100,"total_damage_broiler":null,"start_date":"2024-03-31","end_date":"2024-04-29","is_data_submitted":0,"status":1,"created_by":1,"updated_by":1,"deleted_by":1,"deleted_at":null,"created_at":"2024-03-30T16:41:31.000000Z","updated_at":"2024-04-22T20:27:58.000000Z","instructions":[{"day":1,"data_submitted":false,"instruction_data":[{"id":1,"title":"Ins one","day":1,"checked":0},{"id":2,"title":"Ins 2","day":1,"checked":0}]},{"day":2,"data_submitted":false,"instruction_data":[{"id":3,"title":"Ins 21","day":2,"checked":0},{"id":4,"title":"Ins 22","day":2,"checked":0}]},{"day":3,"data_submitted":false,"instruction_data":[{"id":5,"title":"Ins 31","day":3,"checked":0}]}]}

BroilerDetailsModel broilerDetailsModelFromJson(String str) =>
    BroilerDetailsModel.fromJson(json.decode(str));
String broilerDetailsModelToJson(BroilerDetailsModel data) =>
    json.encode(data.toJson());

class BroilerDetailsModel {
  BroilerDetailsModel({
    this.responseStatus,
    this.data,
  });

  BroilerDetailsModel.fromJson(dynamic json) {
    responseStatus = json['_response_status'] != null
        ? ResponseStatus.fromJson(json['_response_status'])
        : null;
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  ResponseStatus? responseStatus;
  Data? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (responseStatus != null) {
      map['_response_status'] = responseStatus?.toJson();
    }
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }
}

/// id : 1
/// farmer_id : 1
/// batch_no : "BCH00001"
/// total_broiler : 100
/// total_damage_broiler : null
/// start_date : "2024-03-31"
/// end_date : "2024-04-29"
/// is_data_submitted : 0
/// status : 1
/// created_by : 1
/// updated_by : 1
/// deleted_by : 1
/// deleted_at : null
/// created_at : "2024-03-30T16:41:31.000000Z"
/// updated_at : "2024-04-22T20:27:58.000000Z"
/// instructions : [{"day":1,"data_submitted":false,"instruction_data":[{"id":1,"title":"Ins one","day":1,"checked":0},{"id":2,"title":"Ins 2","day":1,"checked":0}]},{"day":2,"data_submitted":false,"instruction_data":[{"id":3,"title":"Ins 21","day":2,"checked":0},{"id":4,"title":"Ins 22","day":2,"checked":0}]},{"day":3,"data_submitted":false,"instruction_data":[{"id":5,"title":"Ins 31","day":3,"checked":0}]}]

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    this.id,
    this.farmerId,
    this.batchNo,
    this.totalBroiler,
    this.totalDamageBroiler,
    this.startDate,
    this.endDate,
    this.isDataSubmitted,
    this.status,
    this.createdBy,
    this.updatedBy,
    this.deletedBy,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
    this.instructions,
  });

  Data.fromJson(dynamic json) {
    id = json['id'];
    farmerId = json['farmer_id'];
    batchNo = json['batch_no'];
    totalBroiler = json['total_broiler'];
    totalDamageBroiler = json['total_damage_broiler'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    isDataSubmitted = json['is_data_submitted'];
    status = json['status'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    deletedBy = json['deleted_by'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['instructions'] != null) {
      instructions = [];
      json['instructions'].forEach((v) {
        instructions?.add(Instructions.fromJson(v));
      });
    }
  }
  num? id;
  num? farmerId;
  String? batchNo;
  num? totalBroiler;
  dynamic totalDamageBroiler;
  String? startDate;
  String? endDate;
  num? isDataSubmitted;
  num? status;
  num? createdBy;
  num? updatedBy;
  num? deletedBy;
  dynamic deletedAt;
  String? createdAt;
  String? updatedAt;
  List<Instructions>? instructions;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['farmer_id'] = farmerId;
    map['batch_no'] = batchNo;
    map['total_broiler'] = totalBroiler;
    map['total_damage_broiler'] = totalDamageBroiler;
    map['start_date'] = startDate;
    map['end_date'] = endDate;
    map['is_data_submitted'] = isDataSubmitted;
    map['status'] = status;
    map['created_by'] = createdBy;
    map['updated_by'] = updatedBy;
    map['deleted_by'] = deletedBy;
    map['deleted_at'] = deletedAt;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    if (instructions != null) {
      map['instructions'] = instructions?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// day : 1
/// data_submitted : false
/// instruction_data : [{"id":1,"title":"Ins one","day":1,"checked":0},{"id":2,"title":"Ins 2","day":1,"checked":0}]

Instructions instructionsFromJson(String str) =>
    Instructions.fromJson(json.decode(str));
String instructionsToJson(Instructions data) => json.encode(data.toJson());

class Instructions {
  Instructions({
    this.day,
    this.dataSubmitted,
    this.comment,
    this.instructionData,
  });

  Instructions.fromJson(dynamic json) {
    day = json['day'];
    dataSubmitted = json['data_submitted'];
    if (json['instruction_data'] != null) {
      instructionData = [];
      json['instruction_data'].forEach((v) {
        instructionData?.add(InstructionData.fromJson(v));
      });
    }
    comment = json['comment'];
  }
  dynamic day;
  bool? dataSubmitted;
  List<InstructionData>? instructionData;
  dynamic comment;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['day'] = day;
    map['data_submitted'] = dataSubmitted;
    map['comment'] = comment;
    if (instructionData != null) {
      map['instruction_data'] =
          instructionData?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 1
/// title : "Ins one"
/// day : 1
/// checked : 0

InstructionData instructionDataFromJson(String str) =>
    InstructionData.fromJson(json.decode(str));
String instructionDataToJson(InstructionData data) =>
    json.encode(data.toJson());

class InstructionData {
  InstructionData({
    this.id,
    this.title,
    this.day,
    this.checked,
  });

  InstructionData.fromJson(dynamic json) {
    id = json['id'];
    title = json['title'];
    day = json['day'];
    checked = json['checked'];
  }
  dynamic id;
  String? title;
  dynamic day;
  num? checked;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['title'] = title;
    map['day'] = day;
    map['checked'] = checked;
    return map;
  }
}

/// success : true
/// code : 200
/// message : "Success"
/// query_time : 0

ResponseStatus responseStatusFromJson(String str) =>
    ResponseStatus.fromJson(json.decode(str));
String responseStatusToJson(ResponseStatus data) => json.encode(data.toJson());

class ResponseStatus {
  ResponseStatus({
    this.success,
    this.code,
    this.message,
    this.queryTime,
  });

  ResponseStatus.fromJson(dynamic json) {
    success = json['success'];
    code = json['code'];
    message = json['message'];
    queryTime = json['query_time'];
  }
  bool? success;
  num? code;
  String? message;
  num? queryTime;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['success'] = success;
    map['code'] = code;
    map['message'] = message;
    map['query_time'] = queryTime;
    return map;
  }
}

import 'dart:convert';

/// current_page : 1
/// total_page : 1
/// page_size : 10
/// total : 4
/// order : "DESC"
/// _response_status : {"success":true,"code":200,"query_time":0}
/// data : [{"id":12,"request_code":"REQ00012","outlet_id":1,"plant_id":1,"status":1,"created_at":"2024-05-31T21:27:10.000000Z","updated_at":"2024-05-31T21:27:10.000000Z","plant":{"id":1,"name":"Plant One","phone":"01975962962"},"outlet":{"id":1,"name":"Subrata Store","owner":"Subrata","phone":"01841962961"},"outlet_package_request_details":[{"id":8,"outlet_package_request_id":12,"quantity":33,"package_id":1,"package":{"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/VuE0ivOvCMLJM93WdkgcDJzzCRQPd7sHwZFruiJJ.png","type":2,"price":"250.00","discount":"10.00","descriptions":"Please buy my product.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-05-25T17:53:06.000000Z"}}]},{"id":11,"request_code":"REQ00011","outlet_id":1,"plant_id":1,"status":1,"created_at":"2024-05-31T21:17:45.000000Z","updated_at":"2024-05-31T21:17:45.000000Z","plant":{"id":1,"name":"Plant One","phone":"01975962962"},"outlet":{"id":1,"name":"Subrata Store","owner":"Subrata","phone":"01841962961"},"outlet_package_request_details":[{"id":7,"outlet_package_request_id":11,"quantity":40,"package_id":1,"package":{"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/VuE0ivOvCMLJM93WdkgcDJzzCRQPd7sHwZFruiJJ.png","type":2,"price":"250.00","discount":"10.00","descriptions":"Please buy my product.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-05-25T17:53:06.000000Z"}}]},{"id":10,"request_code":"REQ00010","outlet_id":1,"plant_id":1,"status":1,"created_at":"2024-05-31T21:15:45.000000Z","updated_at":"2024-05-31T21:15:45.000000Z","plant":{"id":1,"name":"Plant One","phone":"01975962962"},"outlet":{"id":1,"name":"Subrata Store","owner":"Subrata","phone":"01841962961"},"outlet_package_request_details":[{"id":6,"outlet_package_request_id":10,"quantity":10,"package_id":1,"package":{"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/VuE0ivOvCMLJM93WdkgcDJzzCRQPd7sHwZFruiJJ.png","type":2,"price":"250.00","discount":"10.00","descriptions":"Please buy my product.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-05-25T17:53:06.000000Z"}}]},{"id":9,"request_code":"REQ00009","outlet_id":1,"plant_id":1,"status":1,"created_at":"2024-05-31T21:05:35.000000Z","updated_at":"2024-05-31T21:05:35.000000Z","plant":{"id":1,"name":"Plant One","phone":"01975962962"},"outlet":{"id":1,"name":"Subrata Store","owner":"Subrata","phone":"01841962961"},"outlet_package_request_details":[{"id":4,"outlet_package_request_id":9,"quantity":50,"package_id":1,"package":{"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/VuE0ivOvCMLJM93WdkgcDJzzCRQPd7sHwZFruiJJ.png","type":2,"price":"250.00","discount":"10.00","descriptions":"Please buy my product.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-05-25T17:53:06.000000Z"}},{"id":5,"outlet_package_request_id":9,"quantity":60,"package_id":2,"package":{"id":2,"name":"New Chicken","thumbnail":"upload/image/packages/thumbnails/8my6OSooC93P2KO3uRCS3wyFkaPs5PQMMeOp1c4H.png","type":2,"price":"120.00","discount":"10.00","descriptions":"New pics","status":1,"weight":"8","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-19T17:33:57.000000Z","updated_at":"2024-05-25T17:48:19.000000Z"}}]}]

PackageRequestModel packageRequestModelFromJson(String str) =>
    PackageRequestModel.fromJson(json.decode(str));
String packageRequestModelToJson(PackageRequestModel data) =>
    json.encode(data.toJson());

class PackageRequestModel {
  PackageRequestModel({
    this.currentPage,
    this.totalPage,
    this.pageSize,
    this.total,
    this.order,
    this.responseStatus,
    this.data,
  });

  PackageRequestModel.fromJson(dynamic json) {
    currentPage = json['current_page'];
    totalPage = json['total_page'];
    pageSize = json['page_size'];
    total = json['total'];
    order = json['order'];
    responseStatus = json['_response_status'] != null
        ? ResponseStatus.fromJson(json['_response_status'])
        : null;
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(Data.fromJson(v));
      });
    }
  }
  num? currentPage;
  num? totalPage;
  num? pageSize;
  num? total;
  String? order;
  ResponseStatus? responseStatus;
  List<Data>? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['current_page'] = currentPage;
    map['total_page'] = totalPage;
    map['page_size'] = pageSize;
    map['total'] = total;
    map['order'] = order;
    if (responseStatus != null) {
      map['_response_status'] = responseStatus?.toJson();
    }
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 12
/// request_code : "REQ00012"
/// outlet_id : 1
/// plant_id : 1
/// status : 1
/// created_at : "2024-05-31T21:27:10.000000Z"
/// updated_at : "2024-05-31T21:27:10.000000Z"
/// plant : {"id":1,"name":"Plant One","phone":"01975962962"}
/// outlet : {"id":1,"name":"Subrata Store","owner":"Subrata","phone":"01841962961"}
/// outlet_package_request_details : [{"id":8,"outlet_package_request_id":12,"quantity":33,"package_id":1,"package":{"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/VuE0ivOvCMLJM93WdkgcDJzzCRQPd7sHwZFruiJJ.png","type":2,"price":"250.00","discount":"10.00","descriptions":"Please buy my product.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-05-25T17:53:06.000000Z"}}]

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    this.id,
    this.requestCode,
    this.outletId,
    this.plantId,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.plant,
    this.outlet,
    this.outletPackageRequestDetails,
  });

  Data.fromJson(dynamic json) {
    id = json['id'];
    requestCode = json['request_code'];
    outletId = json['outlet_id'];
    plantId = json['plant_id'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    plant = json['plant'] != null ? Plant.fromJson(json['plant']) : null;
    outlet = json['outlet'] != null ? Outlet.fromJson(json['outlet']) : null;
    if (json['outlet_package_request_details'] != null) {
      outletPackageRequestDetails = [];
      json['outlet_package_request_details'].forEach((v) {
        outletPackageRequestDetails
            ?.add(OutletPackageRequestDetails.fromJson(v));
      });
    }
  }
  num? id;
  String? requestCode;
  num? outletId;
  num? plantId;
  num? status;
  String? createdAt;
  String? updatedAt;
  Plant? plant;
  Outlet? outlet;
  List<OutletPackageRequestDetails>? outletPackageRequestDetails;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['request_code'] = requestCode;
    map['outlet_id'] = outletId;
    map['plant_id'] = plantId;
    map['status'] = status;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    if (plant != null) {
      map['plant'] = plant?.toJson();
    }
    if (outlet != null) {
      map['outlet'] = outlet?.toJson();
    }
    if (outletPackageRequestDetails != null) {
      map['outlet_package_request_details'] =
          outletPackageRequestDetails?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 8
/// outlet_package_request_id : 12
/// quantity : 33
/// package_id : 1
/// package : {"id":1,"name":"6 Pics Chicken Leg","thumbnail":"upload/image/packages/thumbnails/VuE0ivOvCMLJM93WdkgcDJzzCRQPd7sHwZFruiJJ.png","type":2,"price":"250.00","discount":"10.00","descriptions":"Please buy my product.","status":1,"weight":"50","weight_unit":"kg","created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-13T18:51:49.000000Z","updated_at":"2024-05-25T17:53:06.000000Z"}

OutletPackageRequestDetails outletPackageRequestDetailsFromJson(String str) =>
    OutletPackageRequestDetails.fromJson(json.decode(str));
String outletPackageRequestDetailsToJson(OutletPackageRequestDetails data) =>
    json.encode(data.toJson());

class OutletPackageRequestDetails {
  OutletPackageRequestDetails({
    this.id,
    this.outletPackageRequestId,
    this.quantity,
    this.packageId,
    this.package,
  });

  OutletPackageRequestDetails.fromJson(dynamic json) {
    id = json['id'];
    outletPackageRequestId = json['outlet_package_request_id'];
    quantity = json['quantity'];
    packageId = json['package_id'];
    package =
        json['package'] != null ? Package.fromJson(json['package']) : null;
  }
  num? id;
  num? outletPackageRequestId;
  num? quantity;
  num? packageId;
  Package? package;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['outlet_package_request_id'] = outletPackageRequestId;
    map['quantity'] = quantity;
    map['package_id'] = packageId;
    if (package != null) {
      map['package'] = package?.toJson();
    }
    return map;
  }
}

/// id : 1
/// name : "6 Pics Chicken Leg"
/// thumbnail : "upload/image/packages/thumbnails/VuE0ivOvCMLJM93WdkgcDJzzCRQPd7sHwZFruiJJ.png"
/// type : 2
/// price : "250.00"
/// discount : "10.00"
/// descriptions : "Please buy my product."
/// status : 1
/// weight : "50"
/// weight_unit : "kg"
/// created_by : 1
/// updated_by : 1
/// deleted_by : null
/// deleted_at : null
/// created_at : "2024-05-13T18:51:49.000000Z"
/// updated_at : "2024-05-25T17:53:06.000000Z"

Package packageFromJson(String str) => Package.fromJson(json.decode(str));
String packageToJson(Package data) => json.encode(data.toJson());

class Package {
  Package({
    this.id,
    this.name,
    this.thumbnail,
    this.type,
    this.price,
    this.discount,
    this.descriptions,
    this.status,
    this.weight,
    this.weightUnit,
    this.createdBy,
    this.updatedBy,
    this.deletedBy,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
  });

  Package.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    thumbnail = json['thumbnail'];
    type = json['type'];
    price = json['price'];
    discount = json['discount'];
    descriptions = json['descriptions'];
    status = json['status'];
    weight = json['weight'];
    weightUnit = json['weight_unit'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    deletedBy = json['deleted_by'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }
  num? id;
  String? name;
  String? thumbnail;
  num? type;
  String? price;
  String? discount;
  String? descriptions;
  num? status;
  String? weight;
  String? weightUnit;
  num? createdBy;
  num? updatedBy;
  dynamic deletedBy;
  dynamic deletedAt;
  String? createdAt;
  String? updatedAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['thumbnail'] = thumbnail;
    map['type'] = type;
    map['price'] = price;
    map['discount'] = discount;
    map['descriptions'] = descriptions;
    map['status'] = status;
    map['weight'] = weight;
    map['weight_unit'] = weightUnit;
    map['created_by'] = createdBy;
    map['updated_by'] = updatedBy;
    map['deleted_by'] = deletedBy;
    map['deleted_at'] = deletedAt;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    return map;
  }
}

/// id : 1
/// name : "Subrata Store"
/// owner : "Subrata"
/// phone : "01841962961"

Outlet outletFromJson(String str) => Outlet.fromJson(json.decode(str));
String outletToJson(Outlet data) => json.encode(data.toJson());

class Outlet {
  Outlet({
    this.id,
    this.name,
    this.owner,
    this.phone,
  });

  Outlet.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    owner = json['owner'];
    phone = json['phone'];
  }
  num? id;
  String? name;
  String? owner;
  String? phone;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['owner'] = owner;
    map['phone'] = phone;
    return map;
  }
}

/// id : 1
/// name : "Plant One"
/// phone : "01975962962"

Plant plantFromJson(String str) => Plant.fromJson(json.decode(str));
String plantToJson(Plant data) => json.encode(data.toJson());

class Plant {
  Plant({
    this.id,
    this.name,
    this.phone,
  });

  Plant.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    phone = json['phone'];
  }
  num? id;
  String? name;
  String? phone;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['phone'] = phone;
    return map;
  }
}

/// success : true
/// code : 200
/// query_time : 0

ResponseStatus responseStatusFromJson(String str) =>
    ResponseStatus.fromJson(json.decode(str));
String responseStatusToJson(ResponseStatus data) => json.encode(data.toJson());

class ResponseStatus {
  ResponseStatus({
    this.success,
    this.code,
    this.error,
    this.queryTime,
  });

  ResponseStatus.fromJson(dynamic json) {
    success = json['success'];
    code = json['code'];
    code = json['error'];
    queryTime = json['query_time'];
  }
  bool? success;
  num? code;
  String? error;
  num? queryTime;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['success'] = success;
    map['code'] = code;
    map['error'] = error;
    map['query_time'] = queryTime;
    return map;
  }
}

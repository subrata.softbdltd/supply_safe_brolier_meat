import 'dart:convert';

/// current_page : 1
/// total_page : 1
/// page_size : 10
/// total : 4
/// order : "DESC"
/// _response_status : {"success":true,"code":200,"query_time":0}
/// data : [{"id":7,"request_code":"REQ00007","outlet_id":1,"plant_id":1,"status":1,"created_at":"2024-05-23T16:29:00.000000Z","updated_at":"2024-05-23T16:29:00.000000Z","plant":{"id":1,"name":"Plant One","phone":"01537457618"},"outlet":{"id":1,"name":"Subrata Store","owner":"Subrata","phone":"01712054784"},"outlet_package_request_details":[{"id":2,"outlet_package_request_id":7,"quantity":5,"package_id":1,"package":{"id":1,"name":"6pcs wings","thumbnail":"upload/image/packages/thumbnails/I83x34u43Ao13GjBjy0nvYIo45W73PPHsdQvxFZT.png","type":3,"price":"150.00","discount":"0.00","descriptions":"this package","weight":"510.00","weight_unit":"gram","status":1,"created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-14T17:18:40.000000Z","updated_at":"2024-05-17T19:37:26.000000Z"}}]}]

OutletPackageListModel outletPackageListModelFromJson(String str) =>
    OutletPackageListModel.fromJson(json.decode(str));
String outletPackageListModelToJson(OutletPackageListModel data) =>
    json.encode(data.toJson());

class OutletPackageListModel {
  OutletPackageListModel({
    this.currentPage,
    this.totalPage,
    this.pageSize,
    this.total,
    this.order,
    this.responseStatus,
    this.data,
  });

  OutletPackageListModel.fromJson(dynamic json) {
    currentPage = json['current_page'];
    totalPage = json['total_page'];
    pageSize = json['page_size'];
    total = json['total'];
    order = json['order'];
    responseStatus = json['_response_status'] != null
        ? ResponseStatus.fromJson(json['_response_status'])
        : null;
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(Data.fromJson(v));
      });
    }
  }
  num? currentPage;
  num? totalPage;
  num? pageSize;
  num? total;
  String? order;
  ResponseStatus? responseStatus;
  List<Data>? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['current_page'] = currentPage;
    map['total_page'] = totalPage;
    map['page_size'] = pageSize;
    map['total'] = total;
    map['order'] = order;
    if (responseStatus != null) {
      map['_response_status'] = responseStatus?.toJson();
    }
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 7
/// request_code : "REQ00007"
/// outlet_id : 1
/// plant_id : 1
/// status : 1
/// created_at : "2024-05-23T16:29:00.000000Z"
/// updated_at : "2024-05-23T16:29:00.000000Z"
/// plant : {"id":1,"name":"Plant One","phone":"01537457618"}
/// outlet : {"id":1,"name":"Subrata Store","owner":"Subrata","phone":"01712054784"}
/// outlet_package_request_details : [{"id":2,"outlet_package_request_id":7,"quantity":5,"package_id":1,"package":{"id":1,"name":"6pcs wings","thumbnail":"upload/image/packages/thumbnails/I83x34u43Ao13GjBjy0nvYIo45W73PPHsdQvxFZT.png","type":3,"price":"150.00","discount":"0.00","descriptions":"this package","weight":"510.00","weight_unit":"gram","status":1,"created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-14T17:18:40.000000Z","updated_at":"2024-05-17T19:37:26.000000Z"}}]

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    this.id,
    this.requestCode,
    this.outletId,
    this.plantId,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.plant,
    this.outlet,
    this.outletPackageRequestDetails,
  });

  Data.fromJson(dynamic json) {
    id = json['id'];
    requestCode = json['request_code'];
    outletId = json['outlet_id'];
    plantId = json['plant_id'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    plant = json['plant'] != null ? Plant.fromJson(json['plant']) : null;
    outlet = json['outlet'] != null ? Outlet.fromJson(json['outlet']) : null;
    if (json['outlet_package_request_details'] != null) {
      outletPackageRequestDetails = [];
      json['outlet_package_request_details'].forEach((v) {
        outletPackageRequestDetails
            ?.add(OutletPackageRequestDetails.fromJson(v));
      });
    }
  }
  num? id;
  String? requestCode;
  num? outletId;
  num? plantId;
  num? status;
  String? createdAt;
  String? updatedAt;
  Plant? plant;
  Outlet? outlet;
  List<OutletPackageRequestDetails>? outletPackageRequestDetails;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['request_code'] = requestCode;
    map['outlet_id'] = outletId;
    map['plant_id'] = plantId;
    map['status'] = status;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    if (plant != null) {
      map['plant'] = plant?.toJson();
    }
    if (outlet != null) {
      map['outlet'] = outlet?.toJson();
    }
    if (outletPackageRequestDetails != null) {
      map['outlet_package_request_details'] =
          outletPackageRequestDetails?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 2
/// outlet_package_request_id : 7
/// quantity : 5
/// package_id : 1
/// package : {"id":1,"name":"6pcs wings","thumbnail":"upload/image/packages/thumbnails/I83x34u43Ao13GjBjy0nvYIo45W73PPHsdQvxFZT.png","type":3,"price":"150.00","discount":"0.00","descriptions":"this package","weight":"510.00","weight_unit":"gram","status":1,"created_by":1,"updated_by":1,"deleted_by":null,"deleted_at":null,"created_at":"2024-05-14T17:18:40.000000Z","updated_at":"2024-05-17T19:37:26.000000Z"}

OutletPackageRequestDetails outletPackageRequestDetailsFromJson(String str) =>
    OutletPackageRequestDetails.fromJson(json.decode(str));
String outletPackageRequestDetailsToJson(OutletPackageRequestDetails data) =>
    json.encode(data.toJson());

class OutletPackageRequestDetails {
  OutletPackageRequestDetails({
    this.id,
    this.outletPackageRequestId,
    this.quantity,
    this.packageId,
    this.package,
  });

  OutletPackageRequestDetails.fromJson(dynamic json) {
    id = json['id'];
    outletPackageRequestId = json['outlet_package_request_id'];
    quantity = json['quantity'];
    packageId = json['package_id'];
    package =
        json['package'] != null ? Package.fromJson(json['package']) : null;
  }
  num? id;
  num? outletPackageRequestId;
  num? quantity;
  num? packageId;
  Package? package;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['outlet_package_request_id'] = outletPackageRequestId;
    map['quantity'] = quantity;
    map['package_id'] = packageId;
    if (package != null) {
      map['package'] = package?.toJson();
    }
    return map;
  }
}

/// id : 1
/// name : "6pcs wings"
/// thumbnail : "upload/image/packages/thumbnails/I83x34u43Ao13GjBjy0nvYIo45W73PPHsdQvxFZT.png"
/// type : 3
/// price : "150.00"
/// discount : "0.00"
/// descriptions : "this package"
/// weight : "510.00"
/// weight_unit : "gram"
/// status : 1
/// created_by : 1
/// updated_by : 1
/// deleted_by : null
/// deleted_at : null
/// created_at : "2024-05-14T17:18:40.000000Z"
/// updated_at : "2024-05-17T19:37:26.000000Z"

Package packageFromJson(String str) => Package.fromJson(json.decode(str));
String packageToJson(Package data) => json.encode(data.toJson());

class Package {
  Package({
    this.id,
    this.name,
    this.thumbnail,
    this.type,
    this.price,
    this.discount,
    this.descriptions,
    this.weight,
    this.weightUnit,
    this.status,
    this.createdBy,
    this.updatedBy,
    this.deletedBy,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
  });

  Package.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    thumbnail = json['thumbnail'];
    type = json['type'];
    price = json['price'];
    discount = json['discount'];
    descriptions = json['descriptions'];
    weight = json['weight'];
    weightUnit = json['weight_unit'];
    status = json['status'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    deletedBy = json['deleted_by'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }
  num? id;
  String? name;
  String? thumbnail;
  num? type;
  String? price;
  String? discount;
  String? descriptions;
  String? weight;
  String? weightUnit;
  num? status;
  num? createdBy;
  num? updatedBy;
  dynamic deletedBy;
  dynamic deletedAt;
  String? createdAt;
  String? updatedAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['thumbnail'] = thumbnail;
    map['type'] = type;
    map['price'] = price;
    map['discount'] = discount;
    map['descriptions'] = descriptions;
    map['weight'] = weight;
    map['weight_unit'] = weightUnit;
    map['status'] = status;
    map['created_by'] = createdBy;
    map['updated_by'] = updatedBy;
    map['deleted_by'] = deletedBy;
    map['deleted_at'] = deletedAt;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    return map;
  }
}

/// id : 1
/// name : "Subrata Store"
/// owner : "Subrata"
/// phone : "01712054784"

Outlet outletFromJson(String str) => Outlet.fromJson(json.decode(str));
String outletToJson(Outlet data) => json.encode(data.toJson());

class Outlet {
  Outlet({
    this.id,
    this.name,
    this.owner,
    this.phone,
  });

  Outlet.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    owner = json['owner'];
    phone = json['phone'];
  }
  num? id;
  String? name;
  String? owner;
  String? phone;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['owner'] = owner;
    map['phone'] = phone;
    return map;
  }
}

/// id : 1
/// name : "Plant One"
/// phone : "01537457618"

Plant plantFromJson(String str) => Plant.fromJson(json.decode(str));
String plantToJson(Plant data) => json.encode(data.toJson());

class Plant {
  Plant({
    this.id,
    this.name,
    this.phone,
  });

  Plant.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    phone = json['phone'];
  }
  num? id;
  String? name;
  String? phone;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['phone'] = phone;
    return map;
  }
}

/// success : true
/// code : 200
/// query_time : 0

ResponseStatus responseStatusFromJson(String str) =>
    ResponseStatus.fromJson(json.decode(str));
String responseStatusToJson(ResponseStatus data) => json.encode(data.toJson());

class ResponseStatus {
  ResponseStatus({
    this.success,
    this.code,
    this.queryTime,
  });

  ResponseStatus.fromJson(dynamic json) {
    success = json['success'];
    code = json['code'];
    queryTime = json['query_time'];
  }
  bool? success;
  num? code;
  num? queryTime;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['success'] = success;
    map['code'] = code;
    map['query_time'] = queryTime;
    return map;
  }
}

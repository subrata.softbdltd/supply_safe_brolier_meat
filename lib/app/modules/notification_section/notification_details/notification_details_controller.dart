import 'package:get/get.dart';

import '../../../data/pojo/Response_model.dart';
import '../../../network/api_client.dart';
import '../../../network/api_url.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/notification_list_model.dart';

class NotificationDetailsController extends GetxController {
  final selectedID = 0.obs;
  final selectedNotificationData = Data().obs;

  final count = 0.obs;
  @override
  void onInit() {
    if (Get.arguments != null && Get.arguments["id"] != null && Get.arguments["data"] != null) {
      selectedID.value = Get.arguments["id"];
      selectedNotificationData.value = Get.arguments["data"];
      readNotification(selectedID.value);
    }
    super.onInit();
  }

  readNotification(id) async {
    var data = <String, dynamic>{};

    var response =
        await ApiClient().putAPI(notificationRead(id), data, readNotification);
    if (response != null) {
      ResponseModel responseModel = responseModelFromJson(response.toString());
      if (responseModel.responseStatus?.success ?? false) {}
    }
  }


}

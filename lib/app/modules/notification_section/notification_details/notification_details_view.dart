import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:soft_builder/constraints/my_colors.dart';
import '../../../core/constants/app_colors.dart';
import '../../../core/constants/app_constants.dart';
import '../../../core/constants/my_text_style.dart';
import '../../../core/widgets/app_defaults/app_app_bar.dart';
import 'notification_details_controller.dart';

class NotificationDetailsView extends GetView<NotificationDetailsController> {
  const NotificationDetailsView({super.key});
  @override
  Widget build(BuildContext context) {
    logger.t(controller.selectedID.value);
    return Scaffold(
      backgroundColor: AppColor.backgroundColor,
      appBar: AppAppBar(
        title: "Notification Details",
        titleColor: AppColor.white,
        titleFontSize: 20.sp,
        needWhiteBackButtonColor: true,
        backgroundColor: AppColor.plantPrimaryColor,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: mainPaddingAll,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  controller.selectedNotificationData.value.title?.toUpperCase() ?? "",
                  style: text20Style(isWeight600: true),
                ),
                gapH8,
                Text(
                  appHelper.dateTimeFormatter(controller.selectedNotificationData.value.createdAt),
                  style: text12Style(color: MyColors.grey),
                ),
                // gapH3,
                appWidgets.divider(),
                gapH8,
                Text(
                  controller.selectedNotificationData.value.body ?? "",
                  style: text16Style(isWeight400: true),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

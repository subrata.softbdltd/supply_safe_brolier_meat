import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import '../../../core/constants/app_colors.dart';
import '../../../core/constants/app_constants.dart';
import '../../../core/widgets/app_defaults/app_app_bar.dart';
import '../../../routes/app_pages.dart';
import 'notification_controller.dart';

class NotificationView extends GetView<NotificationController> {
  const NotificationView({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundColor,
      appBar: AppAppBar(
        title: "Notification",
        titleColor: AppColor.white,
        titleFontSize: 20.sp,
        needWhiteBackButtonColor: true,
        backgroundColor: const Color(0xff227B94),
      ),
      body: Padding(
        padding: mainPaddingAll,
        child: Obx(() {
          return Column(
            children: [
              Expanded(
                child: ListView.separated(
                  controller: controller.scrollController,
                  itemCount: controller.notificationData.length,
                  shrinkWrap: true,
                  physics: const BouncingScrollPhysics(),
                  itemBuilder: (context, index) {
                    var data = controller.notificationData[index];
                    return Material(
                      borderRadius: BorderRadius.circular(15),
                      color: data.noticeReadAt != null
                          ? AppColor.white
                          : AppColor.gradientLeftColor.withOpacity(0.3),
                      child: InkWell(
                        onTap: () => controller.onGoNext(index),
                        borderRadius: BorderRadius.circular(15),
                        child: Ink(
                          width: double.infinity,
                          padding: const EdgeInsets.symmetric(
                            horizontal: 12,
                            vertical: 16,
                          ),
                          decoration: BoxDecoration(
                            color: data.noticeReadAt != null
                                ? AppColor.white
                                : AppColor.gradientLeftColor.withOpacity(0.3),
                            borderRadius: BorderRadius.circular(16),
                            border: Border.all(
                              color: AppColor.grey.withOpacity(.3),
                              width: 1,
                            ),
                            // boxShadow: [
                            //   BoxShadow(
                            //       color: AppColor.grey.withOpacity(.3),
                            //       blurRadius: 5,
                            //       spreadRadius: -5)
                            // ]
                          ),
                          child: Row(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: AppColor.primaryColor.withOpacity(.1),
                                  shape: BoxShape.circle,
                                ),
                                padding: EdgeInsets.all(8.r),
                                child: Icon(
                                  CupertinoIcons.bell,
                                  size: 20.w,
                                  color: AppColor.plantPrimaryColor,
                                ),
                              ),
                              gapW12,
                              Expanded(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      data.title ?? "",
                                      style: Theme.of(context)
                                          .textTheme
                                          .titleMedium,
                                    ),
                                    gapH3,
                                    Text(
                                      appHelper.timeAgo(data.createdAt ?? ""),
                                      style: Theme.of(context)
                                          .textTheme
                                          .labelSmall,
                                    ),
                                    gapH8,
                                    Text(
                                      data.body ?? "",
                                      style:
                                          Theme.of(context).textTheme.bodySmall,
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return gapH8;
                  },
                ),
              ),
            ],
          );
        }),
      ),
    );
  }
}

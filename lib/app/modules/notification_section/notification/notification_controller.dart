import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/notification_list_model.dart';

import '../../../core/constants/app_constants.dart';
import '../../../network/api_url.dart';
import '../../../routes/app_pages.dart';

class NotificationController extends GetxController {
  ScrollController scrollController = ScrollController();

  final selectedNotificationData = Data().obs;
  final notificationData = <Data>[].obs;
  final isDataLoaded = true.obs;
  int pageNo = 1, maxPageSize = 1;

  @override
  void onInit() {
    super.onInit();
    getNotificationList();
    scrollController.addListener(() {
      if (scrollController.hasClients) {
        if (scrollController.position.maxScrollExtent ==
            scrollController.offset) {
          if (pageNo < maxPageSize) {
            pageNo += 1;
            getNotificationList();
          }
        }
      }
    });
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  getNotificationList() async {
    if (pageNo == 1) {
      notificationData.clear();
    }
    var mQueryParams = <String, dynamic>{};

    mQueryParams["order"] = "desc";
    mQueryParams["page"] = pageNo;
    mQueryParams["page_size"] = 10;

    var response = await apiClient.getAPI(
      notificationListUrl,
      getNotificationList,
      mQueryParameters: mQueryParams,
    );
    NotificationListModel notificationListModel =
        notificationListModelFromJson(response.toString());

    if (notificationListModel.responseStatus?.success ?? false) {
      List<Data> temp = [];
      notificationListModel.data?.forEach((element) {
        if (element.body != "") {
          temp.add(element);
        }
      });

      notificationData.addAll(temp);
      notificationData.refresh();
      if (pageNo == 1) {
        maxPageSize = (notificationListModel.totalPage ?? 1).toInt();
      }
    }
  }


  onGoNext(index) {
    selectedNotificationData.value = notificationData[index];
    logger.i("selectedNotificationData ${selectedNotificationData.value.title}");
    Get.toNamed(
        Routes.NOTIFICATION_DETAILS,
        arguments: {
          "id" :  selectedNotificationData.value.id,
          "data" : selectedNotificationData.value,
        }
    )?.then((value) {
      // if(value ?? false){
      getNotificationList();
      // }
    });
  }
}

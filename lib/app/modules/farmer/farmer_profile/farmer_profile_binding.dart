import 'package:get/get.dart';

import 'farmer_profile_controller.dart';

class FarmerProfileBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FarmerProfileController>(
      () => FarmerProfileController(),
    );
  }
}

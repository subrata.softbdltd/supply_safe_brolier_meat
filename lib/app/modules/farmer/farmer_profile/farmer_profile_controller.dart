import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/user_details_model.dart'
    as user_details;
import 'package:supply_safe_brolier_meat/app/core/state/global_state.dart';

import '../../../core/constants/app_constants.dart';
import '../../../network/api_client.dart';
import '../../../network/api_url.dart';

class FarmerProfileController extends GetxController {
  final globalState = Get.find<GlobalState>();
  TextEditingController nameController = TextEditingController(),
      phoneNumber = TextEditingController();
  final needEdit = false.obs;
  final userDetailsData = user_details.Data().obs;
  final isLoggedIn = false.obs;
  void onInit() {
    super.onInit();
    getUserData();
  }

  getUserData() async {
    await appHelper.getToken().then((token) async {
      if (token != null && token != "") {
        isLoggedIn.value = true;
        var response = await ApiClient().getAPI(getUserDetailsUrl, getUserData);
        if (response != null) {
          user_details.UserDetailsModel userDetailsModel =
              user_details.userDetailsModelFromJson(response.toString());
          if (userDetailsModel.responseStatus?.success ?? false) {
            userDetailsData.value =
                userDetailsModel.data ?? user_details.Data();
            nameController.text = userDetailsData.value.name ?? "";
            phoneNumber.text = userDetailsData.value.phone ?? "";
          } else {
            appWidgets.showSimpleToast(userDetailsModel.responseStatus?.message,
                isError: true);
          }
        }
      } else {
        isLoggedIn.value = false;
      }
    });
  }
}

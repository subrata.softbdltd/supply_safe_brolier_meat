import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/core/state/global_state.dart';

import 'home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
    Get.lazyPut<GlobalState>(
      () => GlobalState(),
    );
  }
}

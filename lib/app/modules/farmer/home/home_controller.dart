import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_constants.dart';
import 'package:supply_safe_brolier_meat/app/core/state/global_state.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/broiler_list_model.dart'
    as broiler_list;
import 'package:supply_safe_brolier_meat/app/data/pojo/user_details_model.dart'
    as user_details;
import 'package:supply_safe_brolier_meat/app/network/api_url.dart';
import 'package:supply_safe_brolier_meat/app/routes/app_pages.dart';

import '../../../network/api_client.dart';

class HomeController extends GetxController {
  final globalState = Get.find<GlobalState>();
  final broilerListData = <broiler_list.Data>[].obs;
  final isLoading = false.obs;
  final userDetailsData = user_details.Data().obs;
  final isLoggedIn = false.obs;

  @override
  void onInit() {
    super.onInit();
    getBroilerListData();
    getUserData();
    requestNotificationPermission();
  }

  getBroilerListData() async {
    isLoading.value = true;
    var response = await ApiClient().getAPI(
      broilerListUrl,
      getBroilerListData,
      needLoader: false,
    );
    if (response != null) {
      print("Hello");
      broiler_list.BroilerListModel broilerListModel =
          broiler_list.broilerListModelFromJson(response.toString());

      if (broilerListModel.responseStatus?.success ?? false) {
        broilerListData.value = broilerListModel.data ?? [];
        isLoading.value = false;
      } else {
        print("Hello");
        appWidgets.showSimpleToast(broilerListModel.responseStatus?.error,
            isError: true);
        isLoading.value = false;
        Get.offAllNamed(Routes.LOGIN_SCREEN);
      }
    }
  }

  requestNotificationPermission() async {
    FirebaseMessaging messaging = FirebaseMessaging.instance;
    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );
  }

  onGoNext(id, batchName) {
    Get.toNamed(Routes.BROILER_DETAILS, arguments: {
      "id": id,
      "batchName": batchName,
    })?.then((value) {
      getBroilerListData();
    });
  }

  getUserData() async {
    await appHelper.getToken().then((token) async {
      if (token != null && token != "") {
        isLoggedIn.value = true;
        var response = await ApiClient().getAPI(getUserDetailsUrl, getUserData);
        if (response != null) {
          user_details.UserDetailsModel userDetailsModel =
              user_details.userDetailsModelFromJson(response.toString());
          if (userDetailsModel.responseStatus?.success ?? false) {
            userDetailsData.value =
                userDetailsModel.data ?? user_details.Data();
          } else {
            appWidgets.showSimpleToast(userDetailsModel.responseStatus?.message,
                isError: true);
          }
        }
      } else {
        isLoggedIn.value = false;
      }
    });
  }
}

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_colors.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_constants.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/asset_constants.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/my_text_style.dart';
import 'package:supply_safe_brolier_meat/app/core/widgets/app_defaults/app_app_bar.dart';

import '../../../core/widgets/app_defaults/my_drawer.dart';
import '../../../core/widgets/app_widgets.dart';
import '../../../routes/app_pages.dart';
import 'home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({super.key});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return AppWidget().appExitConfirmation();
      },
      child: Scaffold(
          backgroundColor: AppColor.backgroundColor,
          appBar: AppAppBar(
            title: "Home",
            titleColor: AppColor.white,
            titleFontSize: 20.sp,
            needWhiteBackButtonColor: true,
            backgroundColor: const Color(0xff227B94),
            needActionButton:
                controller.globalState.isLoggedIn.value ? true : false,
            // needActionButtonList: true,

            actionsList: [
              IconButton(
                onPressed: () {
                  Get.toNamed(Routes.NOTIFICATION);
                },
                icon: Icon(
                  Icons.notifications,
                  size: 20.w,
                ),
              )
            ],
          ),
          drawer: Obx(() {
            return AppDrawer(
              userImage: controller.userDetailsData.value.photo ?? "",
              userName: controller.userDetailsData.value.name ?? '',
            );
          }),
          body: Padding(
            padding: mainPaddingAll,
            child: Column(
              children: [
                Expanded(
                  child: Obx(() {
                    if (!controller.isLoading.value) {
                      return ListView.separated(
                        itemCount: controller.broilerListData.length,
                        shrinkWrap: true,
                        physics: const BouncingScrollPhysics(),
                        itemBuilder: (context, index) {
                          final data = controller.broilerListData[index];
                          final currentDay =
                              appHelper.totalDayCount(data.startDate ?? "");
                          return boilerCardWidget(
                            onTap: () {
                              controller.onGoNext(
                                data.id,
                                data.batchNo,
                              );
                            },
                            batchNo: data.batchNo ?? "",
                            startDate: data.startDate ?? "",
                            dayCount: currentDay <= 30
                                ? "${appHelper.replaceNumber("${currentDay + 1}")} তম দিন"
                                : "ব্যাচ সম্পন্ন হয়েছ",
                            totalChicken: appHelper
                                .replaceNumber("${data.totalBroiler ?? 0}"),
                            totalDamageBroiler: appHelper.replaceNumber(
                                "${data.totalDamageBroiler ?? 0}"),
                            totalGoodBroiler: appHelper.replaceNumber(
                                "${data.totalBroiler ?? 0 - data.totalDamageBroiler ?? 0}"),
                          );
                        },
                        separatorBuilder: (context, index) {
                          return gapH16;
                        },
                      );
                    } else {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                  }),
                ),
              ],
            ),
          )),
    );
  }

  Widget boilerCardWidget({
    required String batchNo,
    required String startDate,
    required String dayCount,
    required String totalChicken,
    required String totalDamageBroiler,
    required String totalGoodBroiler,
    required Function() onTap,
  }) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: defaultRadius,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.4),
            spreadRadius: 0.5,
            blurRadius: 1,
            offset: const Offset(0, 0.5), // changes position of shadow
          ),
        ],
      ),
      child: Material(
        borderRadius: defaultRadius,
        child: InkWell(
          borderRadius: defaultRadius,
          onTap: onTap,
          child: Ink(
            decoration: BoxDecoration(
              color: const Color(0xffAFD198).withOpacity(0.5),
              borderRadius: defaultRadius,
            ),
            padding: EdgeInsets.all(20.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "ব্যাচ নাম্বার: $batchNo" ?? "",
                  style: text20Style(fontWeight: FontWeight.w600),
                ),
                Text(
                  "শুরুর তারিখ: $startDate",
                  style: text14Style(),
                ),
                Text(
                  dayCount,
                  style: text14Style(),
                ),
                appWidgets.divider(
                  color: AppColor.gray.withOpacity(0.3),
                ),
                gapH12,
                Row(
                  children: [
                    chickenCountWidget(
                      count: totalChicken,
                      imageName: chickenOneImage,
                      borderColor: AppColor.gray,
                      imageColor: AppColor.gray,
                    ),
                    gapW8,
                    chickenCountWidget(
                      count: totalDamageBroiler,
                      imageName: chickenOneImage,
                      borderColor: AppColor.red,
                      imageColor: AppColor.red,
                    ),
                    gapW8,
                    chickenCountWidget(
                      count: totalGoodBroiler,
                      imageName: chickenOneImage,
                      borderColor: Colors.blue,
                      imageColor: Colors.blue,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget chickenCountWidget({
    required String count,
    required String imageName,
    required Color borderColor,
    required Color imageColor,
  }) {
    return Expanded(
      child: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(8.w),
        decoration: BoxDecoration(
          borderRadius: defaultBorder,
          border: Border.all(color: borderColor),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              imageName,
              height: 20,
              color: imageColor,
            ),
            gapW3,
            Text(
              count,
              style: text14Style(fontWeight: FontWeight.w500),
            )
          ],
        ),
      ),
    );
  }
}

import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_constants.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/broiler_details_model.dart'
    as broiler_details;
import 'package:dio/dio.dart' as dio;
import '../../../data/pojo/Response_model.dart';
import '../../../network/api_client.dart';
import '../../../network/api_url.dart';
import '../../Farmer/broiler_details/broiler_details_controller.dart';


class FormFilupSectionController extends GetxController {
  final formKey = GlobalKey<FormState>();
  TextEditingController commentController = TextEditingController(),
      damageController = TextEditingController();
  final ScrollController scrollController = ScrollController();
  final broilerDetailsController = Get.find<BroilerDetailsController>();
  final selectedInstructions = broiler_details.Instructions().obs;
  final multiImage = <String>[].obs;
  final isSelected = [].obs, selectedID = [].obs;

  final ImagePicker picker = ImagePicker(); // Add an instance of ImagePicker

  @override
  void onInit() {
    super.onInit();
    logger.d("BatchID ${broilerDetailsController.batchID.value}");
    if (Get.arguments != null) {
      selectedInstructions.value = Get.arguments;

      if (selectedInstructions.value.dataSubmitted ?? false) {
        for (var element in selectedInstructions.value.instructionData ?? []) {
          selectedID.add(element?.id);
          isSelected.add(true);
          logger.d("selectedID length: ${selectedID.length}");
        }
        commentController.text = selectedInstructions.value.comment;
      } else {
        for (var element in selectedInstructions.value.instructionData ?? []) {
          isSelected.add(false);
        }
      }
    }
  }

  // Modified function to open camera or gallery dialog
  void getImageSourceOption() {
    Get.defaultDialog(
      titlePadding: mainPaddingAll,
      title: "Select Image Source",
      content: Column(
        children: [
          ListTile(
            leading: const Icon(Icons.camera_alt),
            title: const Text("Camera"),
            onTap: () {
              Get.back();
              getMultiImages(ImageSource.camera);
            },
          ),
          ListTile(
            leading: const Icon(Icons.photo_library),
            title: const Text("Gallery"),
            onTap: () {
              Get.back();
              getMultiImages(ImageSource.gallery);
            },
          ),
        ],
      ),
    );
  }

  // Updated function to support both camera and gallery
  Future<void> getMultiImages(ImageSource source) async {
    try {
      final List<XFile?> images = source == ImageSource.gallery
          ? await picker.pickMultiImage()
          : [await picker.pickImage(source: source)];

      if (images.isNotEmpty) {
        if (multiImage.length + images.length > 5) {
          appWidgets.showSimpleToast(
            "আপনি সর্বোচ্চ 5টি ছবি নির্বাচন করতে পারেন",
            isError: true,
            title: "সীমা ছাড়িয়ে গেছে",
          );
          return;
        }

        images.forEach((element) {
          if (element != null) {
            multiImage.add(element.path);
            logger.i("multiImagePath ${element.path}");
          }
        });
      } else {
        appWidgets.showSimpleToast(
          "No Image is Clicked or Selected",
          isError: true,
          title: "Image Not Found",
        );
      }
    } catch (e) {
      appWidgets.showSimpleToast(
        "Error picking image: $e",
        isError: true,
        title: "Image Error",
      );
      logger.e("Error picking image: $e");
    }
  }

  onSubmit() {
    if (formKey.currentState?.validate() ?? false) {
      submitBatchInstructions();
    }
  }

  submitBatchInstructions() async {
    var data = <String, dynamic>{};
    data["batch_id"] = broilerDetailsController.batchID.value;
    data["day"] = selectedInstructions.value.day;
    data["comment"] = commentController.text;
    data["number_of_death_broiler"] = damageController.text;
    if (selectedID.isNotEmpty) {
      for (var i = 0; i < selectedID.length; i++) {
        data['instruction_ids[$i]'] = selectedID[i];
      }
    }
    if (multiImage.isNotEmpty) {
      for (var i = 0; i < multiImage.length; i++) {
        data['images[$i]'] = dio.MultipartFile.fromFileSync(multiImage.value[i]);;
      }
    }

    var response = await ApiClient().postAPI(
      batchInstructionsUrl,
      data,
      submitBatchInstructions,
      isApplicationJson: false,

    );
    if (response != null) {
      ResponseModel responseModel = responseModelFromJson(response.toString());
      if (responseModel.responseStatus?.success ?? false) {
        Get.back(
          result: true,
        );

        appWidgets.showSimpleToast(
          responseModel.responseStatus?.message,
          isSuccess: true,
        );
      } else {
        appWidgets.showSimpleToast(
          responseModel.responseStatus?.error,
        );
      }
    }
  }
}



// class FormFilupSectionController extends GetxController {
//   final formKey = GlobalKey<FormState>();
//   TextEditingController commentController = TextEditingController(),
//       damageController = TextEditingController();
//   final ScrollController scrollController = ScrollController();
//   final broilerDetailsController = Get.find<BroilerDetailsController>();
//   final selectedInstructions = broiler_details.Instructions().obs;
//   final multiImage = <String>[].obs;
//   final isSelected = [].obs, selectedID = [].obs;
//
//   @override
//   void onInit() {
//     super.onInit();
//     logger.d("BatchID ${broilerDetailsController.batchID.value}");
//     if (Get.arguments != null) {
//       selectedInstructions.value = Get.arguments;
//
//       if (selectedInstructions.value.dataSubmitted ?? false) {
//         for (var element in selectedInstructions.value.instructionData ?? []) {
//           selectedID.add(element?.id);
//           isSelected.add(true);
//           logger.d("selectedID length: ${selectedID.length}");
//         }
//         commentController.text = selectedInstructions.value.comment;
//       } else {
//         for (var element in selectedInstructions.value.instructionData ?? []) {
//           isSelected.add(false);
//         }
//       }
//     }
//   }
//
//   getMultiImages(ImageSource source, List<String> multiImage) async {
//     final ImagePicker picker = ImagePicker();
//
//     final List<XFile> images = await picker.pickMultiImage();
//
//     if (images.isNotEmpty) {
//       if (multiImage.length + images.length > 5) {
//         appWidgets.showSimpleToast(
//           "আপনি সর্বোচ্চ 5টি ছবি নির্বাচন করতে পারেন",
//           isError: true,
//           title: "সীমা ছাড়িয়ে গেছে",
//         );
//         return;
//       }
//
//       images.forEach((element) {
//         multiImage.add(element.path.toString());
//         logger.i("multiImagePath ${element.path.toString()}");
//       });
//     } else {
//       appWidgets.showSimpleToast(
//         "No Image is Clicked or Selected",
//         isError: true,
//         title: "Image Not Found",
//       );
//     }
//   }
//
//   onSubmit() {
//     if (formKey.currentState?.validate() ?? false) {
//       submitBatchInstructions();
//     }
//   }
//
//   submitBatchInstructions() async {
//     var data = <String, dynamic>{};
//     data["batch_id"] = broilerDetailsController.batchID.value;
//     data["day"] = selectedInstructions.value.day;
//     data["comment"] = commentController.text;
//     data["number_of_death_broiler"] = damageController.text;
//     if (selectedID.isNotEmpty) {
//       for (var i = 0; i < selectedID.length; i++) {
//         data['instruction_ids[$i]'] = selectedID[i];
//       }
//     }
//     // if (multiImage.isNotEmpty) {
//     //   for (var i = 0; i < multiImage.length; i++) {
//     //     data['images[$i]'] = "Hello";
//     //   }
//     // }
//
//     var response = await ApiClient().postAPI(
//       batchInstructionsUrl,
//       data,
//       submitBatchInstructions,
//       isFormData: true,
//     );
//     if (response != null) {
//       ResponseModel responseModel = responseModelFromJson(response.toString());
//       if (responseModel.responseStatus?.success ?? false) {
//         Get.back(
//           result: true,
//         );
//
//         appWidgets.showSimpleToast(
//           responseModel.responseStatus?.message,
//           isSuccess: true,
//         );
//       } else {
//         appWidgets.showSimpleToast(
//           responseModel.responseStatus?.error,
//         );
//       }
//     }
//   }
// }

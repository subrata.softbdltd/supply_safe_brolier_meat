import 'dart:io';

import 'package:dynamic_height_grid_view/dynamic_height_grid_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:soft_builder/widget/my_edit_text.dart';
import 'package:supply_safe_brolier_meat/app/core/widgets/app_button.dart';

import '../../../core/constants/app_colors.dart';
import '../../../core/constants/app_constants.dart';
import '../../../core/constants/my_text_style.dart';
import '../../../core/widgets/app_defaults/app_app_bar.dart';
import 'form_filup_section_controller.dart';

class FormFilupSectionView extends GetView<FormFilupSectionController> {
  const FormFilupSectionView({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppAppBar(
        title:
            "${appHelper.replaceNumber("${controller.selectedInstructions.value.day}")} তম দিন",
        titleColor: AppColor.white,
        titleFontSize: 20.sp,
        needWhiteBackButtonColor: true,
        backgroundColor: const Color(0xff227B94),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(
          vertical: 20.w,
          horizontal: 15.w,
        ),
        height: 100.w,
        decoration: const BoxDecoration(
          color: Colors.transparent,
          // borderRadius: BorderRadius.only(
          //   topLeft: Radius.circular(12.w),
          //   topRight: Radius.circular(12.w),
          // )
        ),
        child: AppButton(
          backgroundColor: const Color(0xff227B94),
          text: "জমা দিন",
          onPressed: controller.onSubmit,
          fontSize: 20,
        ),
      ),
      body: SafeArea(
          child: SingleChildScrollView(
        child: Padding(
          padding: mainPaddingAll,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ListView.separated(
                itemCount: controller
                        .selectedInstructions.value.instructionData?.length ??
                    0,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  final data = controller
                      .selectedInstructions.value.instructionData?[index];

                  return Obx(() {
                    return Material(
                      borderRadius: defaultBorder,
                      child: InkWell(
                        onTap: () {
                          controller.isSelected[index] =
                              !controller.isSelected[index];

                          if (controller.isSelected[index]) {
                            controller.selectedID.add(data?.id);
                          } else {
                            controller.selectedID.removeAt(index);
                          }
                          logger.d("Here is ID: ${data?.id}");
                          logger.d(
                              "selectedID length: ${controller.selectedID.length}");
                          logger.d("selectedID: ${controller.selectedID}");
                        },
                        borderRadius: defaultBorder,
                        child: Ink(
                          padding: EdgeInsets.all(12.w),
                          decoration: BoxDecoration(
                            border: Border.all(
                                color: controller.isSelected[index]
                                    ? AppColor.plantPrimaryColor
                                    : Colors.grey),
                            borderRadius: defaultBorder,
                          ),
                          child: Row(
                            children: [
                              Container(
                                padding: EdgeInsets.all(2.w),
                                decoration: BoxDecoration(
                                    color: Colors.grey,
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                        color: controller.isSelected[index]
                                            ? AppColor.textColorBlue
                                            : Colors.white)),
                                child: Container(
                                  padding: EdgeInsets.all(6.w),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: controller.isSelected[index]
                                        ? AppColor.plantPrimaryColor
                                        : Colors.white,
                                  ),
                                ),
                              ),
                              gapW12,
                              Expanded(
                                  child: Text(
                                data?.title ?? "",
                                style: text16Style(),
                              ))
                            ],
                          ),
                        ),
                      ),
                    );
                  });
                },
                separatorBuilder: (context, index) {
                  return gapH12;
                },
              ),
              gapH12,
              Form(
                  key: controller.formKey,
                  child: Column(
                    children: [
                      MyEditText(
                        title: "ক্ষতি চিকেন গণনা",
                        controller: controller.damageController,
                        isNumberKeyboard: true,
                        isSignedNumberKeyboard: true,
                        isRequired: false,
                      ),
                      MyEditText(
                        title: "মন্তব্য করুন",
                        controller: controller.commentController,
                        minLine: 5,
                        maxLine: 6,
                        errorMsg: "আপনার মন্তব্য লিখুন",
                      ),
                      gapH20,
                      Obx(() {
                        return Visibility(
                            visible: (controller.multiImage.length < 5),
                            child: Material(
                              child: InkWell(
                                  onTap: () {
                                    controller.getImageSourceOption();
                                  },
                                  child: Ink(
                                    padding: mainPaddingSymmetric(12),
                                    child: Row(
                                      children: [
                                        const Icon(
                                          Icons.image,
                                        ),
                                        gapW8,
                                        Text(
                                          "ছবি আপলোড করুন",
                                          style: text16Style(),
                                        ),
                                      ],
                                    ),
                                  )),
                            ));
                      }),
                      gapH12,
                      Obx(() {
                        return Visibility(
                          visible: controller.multiImage.isNotEmpty,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "ছবি",
                                style: text18Style(),
                              ),
                              appWidgets.divider(
                                  color: AppColor.appBarTextColor),
                            ],
                          ),
                        );
                      }),
                      gapH12,
                      Obx(() {
                        if (controller.multiImage.isEmpty) {
                          return Container();
                        }

                        return DynamicHeightGridView(
                          controller: controller.scrollController,
                          shrinkWrap: true,
                          physics: const BouncingScrollPhysics(),
                          crossAxisCount: 3,
                          itemCount: controller.multiImage.length,
                          builder: (context, index) {
                            final data = controller.multiImage[index];
                            return AnimationConfiguration.staggeredGrid(
                              position: index,
                              columnCount: 2,
                              duration: const Duration(milliseconds: 375),
                              child: ScaleAnimation(
                                delay: const Duration(milliseconds: 250),
                                child: FadeInAnimation(
                                  child: Container(
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(12)),
                                    child: Stack(
                                      children: [
                                        Image.file(
                                          File(data),
                                          fit: BoxFit.cover,
                                        ),
                                        Positioned(
                                          top: 5,
                                          right: 5,
                                          child: Material(
                                            child: InkWell(
                                              onTap: () {
                                                controller.multiImage
                                                    .removeAt(index);
                                              },
                                              child: Ink(
                                                decoration: const BoxDecoration(
                                                  color: AppColor.white,
                                                ),
                                                child: const Icon(
                                                  size: 18,
                                                  Icons.close_rounded,
                                                  color: AppColor.primaryColor,
                                                ),
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                        );
                      }),
                    ],
                  ))
            ],
          ),
        ),
      )),
    );
  }
}

import 'package:get/get.dart';

import 'form_filup_section_controller.dart';

class FormFilupSectionBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FormFilupSectionController>(
      () => FormFilupSectionController(),
    );
  }
}

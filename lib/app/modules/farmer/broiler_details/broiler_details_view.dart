import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:rotated_corner_decoration/rotated_corner_decoration.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_constants.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/my_text_style.dart';

import '../../../core/constants/app_colors.dart';
import '../../../core/widgets/app_defaults/app_app_bar.dart';
import 'broiler_details_controller.dart';

class BroilerDetailsView extends GetView<BroilerDetailsController> {
  const BroilerDetailsView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.background,
      appBar: AppAppBar(
        title: "ব্যাচ বিস্তারিত",
        titleColor: AppColor.white,
        titleFontSize: 20.sp,
        needWhiteBackButtonColor: true,
        backgroundColor: const Color(0xff227B94),
      ),
      body: Obx(() {
        return Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
          child: controller.isLoading.value
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : Column(
                  children: [
                    Container(
                      // height: 150.h,
                      width: double.infinity,
                      padding: const EdgeInsets.all(18),
                      decoration: BoxDecoration(
                          color: const Color(0xff135D66),
                          borderRadius: BorderRadius.circular(12)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "ব্যাচ নাম্বার: ${controller.broilerDetailsData.value.data?.batchNo ?? ""}" ??
                                "",
                            style: text18Style(
                                fontWeight: FontWeight.w600,
                                color: AppColor.white),
                          ),
                          gapH3,
                          Text(
                            "শুরুর তারিখ: ${controller.broilerDetailsData.value.data?.startDate ?? ""}",
                            style: text16Style(
                              color: AppColor.white,
                            ),
                          ),
                          gapH3,
                          if (controller.currentDay.value <= 30)
                            Text(
                              "${appHelper.replaceNumber("${controller.currentDay.value + 1}")} তম দিন",
                              style: text16Style(
                                color: AppColor.white,
                              ),
                            )
                          else
                            Text(
                              "ব্যাচ সম্পন্ন  হয়েছ",
                              style: text16Style(
                                color: AppColor.white,
                              ),
                            ),
                          gapH12,
                        ],
                      ),
                    ),
                    gapH12,
                    Expanded(
                      child: ListView.separated(
                        shrinkWrap: true,
                        controller: controller.scrollController,
                        physics: const BouncingScrollPhysics(),
                        itemCount: controller.instructionData.length,
                        itemBuilder: (context, index) {
                          final data = controller.instructionData[index];

                          return Material(
                            child: InkWell(
                              borderRadius: BorderRadius.circular(12.w),
                              onTap: () {
                                controller.onGoNext(data);
                              },
                              child: Container(
                                padding: mainPaddingAll,
                                decoration: BoxDecoration(
                                  border:
                                      Border.all(width: 1, color: Colors.green),
                                  color:
                                      const Color(0xffAFD198).withOpacity(0.5),
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                foregroundDecoration: (data.dataSubmitted ??
                                        false)
                                    ? RotatedCornerDecoration.withColor(
                                        badgeCornerRadius:
                                            const Radius.circular(12),
                                        color: Colors.red,
                                        badgeSize: Size(40.w, 40.w),
                                        textSpan: TextSpan(
                                          text: 'সম্পন্ন',
                                          style:
                                              text12Style(color: Colors.white),
                                        ),
                                      )
                                    : null,
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        "দিন ${appHelper.replaceNumber("${data.day ?? 0}")}",
                                        style: text18Style(),
                                      ),
                                    ),
                                    (data.dataSubmitted ?? false)
                                        ? const SizedBox()
                                        : Icon(
                                            Icons.arrow_forward_ios,
                                            size: 15.w,
                                          )
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                        separatorBuilder: (context, index) {
                          return gapH16;
                        },
                      ),
                    ),
                    gapH12,
                  ],
                ),
        );
      }),
    );
  }
}

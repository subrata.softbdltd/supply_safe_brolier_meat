import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:soft_builder/constraints/my_constraints.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_constants.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/Response_model.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/broiler_details_model.dart'
    as broiler_details;
import 'package:supply_safe_brolier_meat/app/network/api_client.dart';
import 'package:supply_safe_brolier_meat/app/routes/app_pages.dart';

import '../../../network/api_url.dart';

class BroilerDetailsController extends GetxController {
  final ScrollController scrollController = ScrollController();
  List<TextEditingController> commentControllers = [];
  final broilerDetailsData = broiler_details.BroilerDetailsModel().obs;
  final instructionData = <broiler_details.Instructions>[].obs;
  final instructionIdList = [].obs;

  final isLoading = false.obs;
  final selectedDays = 0.obs, currentDay = 0.obs, batchID = 0.obs;

  int currentlyExpandedIndex = -1.obs;

  final batchName = "".obs;

  @override
  void onInit() {
    super.onInit();
    if (Get.arguments != null &&
        Get.arguments["id"] != "" &&
        Get.arguments["batchName"] != "") {
      batchName.value = Get.arguments["batchName"];
      batchID.value = Get.arguments["id"];
      getBroilerDetails(Get.arguments["id"]);
    }
  }

  toggleCheckbox(id) {
    if (instructionIdList.isEmpty) {
      if (instructionIdList.contains(id)) {
        instructionIdList.remove(id);
      } else {
        instructionIdList.add(id);
      }
    } else {
      instructionIdList.remove(id);
    }
  }

  Future<void> getBroilerDetails(id) async {
    var response =
        await ApiClient().getAPI(broilerDetails(id), getBroilerDetails);
    if (response != null) {
      broilerDetailsData.value =
          broiler_details.broilerDetailsModelFromJson(response.toString());
      if (broilerDetailsData.value.responseStatus?.success ?? false) {
        currentDay.value = appHelper
            .totalDayCount(broilerDetailsData.value.data!.startDate ?? "");
        instructionData.value =
            broilerDetailsData.value.data?.instructions ?? [];
        instructionData.forEach((data) {
          data.instructionData?.forEach((element) {
            if (element.day == currentDay.value) {
              if (element.checked == 1) {
                instructionIdList.add(element.id);
              }
            }
          });
        });

        print("Hello");
      } else {
        Get.back();
        myWidget.showSimpleToast(
          broilerDetailsData.value.responseStatus?.message,
          isError: true,
        );
      }
    }
  }

  onGoNext(data) {
    // selectedInstructions.value = data;
    Get.toNamed(Routes.FORM_FILUP_SECTION, arguments: data)?.then((value) {
      if(value == true){
        getBroilerDetails(Get.arguments["id"]);
      }
    });
  }
}

// WidgetsBinding.instance.addPostFrameCallback((_) {

import 'package:get/get.dart';

import 'broiler_details_controller.dart';

class BroilerDetailsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<BroilerDetailsController>(
      () => BroilerDetailsController(),
    );
  }
}

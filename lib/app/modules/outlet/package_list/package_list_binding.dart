import 'package:get/get.dart';

import 'package_list_controller.dart';

class PackageListBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PackageListController>(
      () => PackageListController(),
    );
  }
}

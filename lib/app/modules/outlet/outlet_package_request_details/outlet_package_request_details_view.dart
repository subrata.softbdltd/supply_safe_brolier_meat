import 'package:dynamic_height_grid_view/dynamic_height_grid_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/package_request_model.dart';

import '../../../core/constants/app_colors.dart';
import '../../../core/constants/app_constants.dart';
import '../../../core/constants/asset_constants.dart';
import '../../../core/constants/my_text_style.dart';
import '../../../core/widgets/app_defaults/app_app_bar.dart';
import '../../../core/widgets/app_network_image.dart';
import 'outlet_package_request_details_controller.dart';

class OutletPackageRequestDetailsView
    extends GetView<OutletPackageRequestDetailsController> {
  const OutletPackageRequestDetailsView({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundColor,
      appBar: AppAppBar(
        title: "বিস্তারিত",
        titleColor: AppColor.white,
        titleFontSize: 20.sp,
        needWhiteBackButtonColor: true,
        backgroundColor: AppColor.plantPrimaryColor,
      ),
      floatingActionButton:
          controller.packageRequestController.status.value == 1
              ? FloatingActionButton.extended(
                  backgroundColor: AppColor.red,
                  onPressed: controller.deleteStatus,
                  label: Text(
                    "বাতিল করুন",
                    style: text14Style(color: AppColor.white),
                  ))
              : const SizedBox(),
      body: Padding(
        padding: mainPaddingSymmetric(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(
              child: Text(
                "অনুরোধ কোড: ${controller.packageRequestController.selectedData.value.requestCode}",
                style:
                    const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
            ),
            gapH20,
            Text(
              "বিবরণ",
              style: text20Style(),
            ),
            tableRowDesign(
              "নাম",
              value: controller.packageRequestController.selectedData.value
                      .plant?.name ??
                  "",
              isBorderAll: true,
            ),
            tableRowDesign("ফোন",
                value: controller.packageRequestController.selectedData.value
                        .plant?.phone ??
                    ""),
            gapH12,
            Text(
              "অর্ডারের বিবরণ",
              style: text20Style(),
            ),
            gapH12,
            tableRowDesign("অর্ডারের তারিখ",
                value: DateFormat('dd MMM yyyy').format(DateTime.parse(
                    controller.packageRequestController.selectedData.value
                            .createdAt ??
                        "")),
                isBorderAll: true),
            tableRowDesign(
              "স্ট্যাটাস",
              value: controller
                          .packageRequestController.selectedData.value.status ==
                      0
                  ? "তৈরি হয়েছে"
                  : controller.packageRequestController.selectedData.value
                              .status ==
                          1
                      ? "স্থাপন করা হয়েছে"
                      : controller.packageRequestController.selectedData.value
                                  .status ==
                              2
                          ? "বিতরণ করা হয়েছে"
                          : "বাতিল",
            ),
            gapH20,
            Text(
              "প্যাকেজ বিবরণ",
              style: text20Style(),
            ),
            appWidgets.divider(color: AppColor.appBarTextColor),
            gapH8,
            Obx(() {
              return Expanded(
                  child: AnimationLimiter(
                child: DynamicHeightGridView(
                  controller: controller.scrollController,
                  shrinkWrap: true,
                  physics: const BouncingScrollPhysics(),
                  crossAxisCount: 2,
                  itemCount: controller.packageRequestController.selectedData
                          .value.outletPackageRequestDetails?.length ??
                      0,
                  builder: (context, index) {
                    final data = controller.packageRequestController
                        .selectedData.value.outletPackageRequestDetails?[index];
                    return AnimationConfiguration.staggeredGrid(
                      position: index,
                      columnCount: 2,
                      duration: const Duration(milliseconds: 375),
                      child: ScaleAnimation(
                        delay: const Duration(milliseconds: 250),
                        child: FadeInAnimation(
                          child: cardItems(
                            context,
                            data ?? OutletPackageRequestDetails(),
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ));
            }),
          ],
        ),
      ),
    );
  }

  Widget cardItems(
    BuildContext context,
    OutletPackageRequestDetails data,
  ) {
    return Material(
      borderRadius: BorderRadius.circular(12),
      child: Ink(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            gapH12,
            AppNetworkImage(
              imageUrl: data.package?.thumbnail ?? "",
              noImage: noDataFound,
              width: double.infinity,
              assetImageColor: Colors.grey,
              assetWidth: 80.0,
            ),
            gapH3,
            Padding(
              padding: EdgeInsets.only(left: 10.w, bottom: 10.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Text(
                      data.package?.name ?? "",
                      textAlign: TextAlign.center,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: text16Style(
                        isWeight600: true,
                      ),
                    ),
                  ),
                  gapH12,
                  Text(
                    "পরিমাণ: ${data.quantity ?? 0} প্যাক",
                    style: text12Style(),
                  ),
                  Text(
                    "প্রতি প্যাকেজ ওজন: ${data.package?.weight ?? 0} ${data.package?.weightUnit}",
                    style: text12Style(),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  tableRowDesign(
    String title, {
    String value = "",
    bool isBorderAll = false,
    bool needButton = false,
    bool needStyle = false,
    isNeedStatusButton = false,
    valueFlex = 1,
  }) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 8.w),
      decoration: BoxDecoration(
        border: isBorderAll
            ? Border.all(color: AppColor.grey.withOpacity(.4), width: 1)
            : Border(
                right:
                    BorderSide(color: AppColor.grey.withOpacity(.4), width: 1),
                bottom:
                    BorderSide(color: AppColor.grey.withOpacity(.4), width: 1),
                left:
                    BorderSide(color: AppColor.grey.withOpacity(.4), width: 1),
              ),
      ),
      child: IntrinsicHeight(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              flex: 2,
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 12.h),
                child: Text(
                  "$title:",
                  style: text14Style(isWeight600: true, fontSize: 16),
                ),
              ),
            ),
            const VerticalDivider(color: AppColor.grey),
            Expanded(
              flex: 3,
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 12.h),
                child: Text(
                  value,
                  style: text14Style(fontSize: 15),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

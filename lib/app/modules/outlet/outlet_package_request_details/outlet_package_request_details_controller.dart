import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/constants/app_constants.dart';
import '../../../data/pojo/Response_model.dart';
import '../../../network/api_url.dart';
import '../package_request/package_request_controller.dart';

class OutletPackageRequestDetailsController extends GetxController {
  final packageRequestController = Get.find<PackageRequestController>();
  final ScrollController scrollController = ScrollController();

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }

  deleteStatus() async {
    var data = <String, dynamic>{};
    // data["package_request_id"] = packageRequestController.selectedData.value.id;
    var response =
        await apiClient.postAPI(plantCancelStatusUpdateUrl(packageRequestController.selectedData.value.id), data, deleteStatus);
    if (response != null) {
      ResponseModel responseStatus = responseModelFromJson(response.toString());
      if (responseStatus.responseStatus?.success ?? false) {
        Get.back();

        appWidgets.showSimpleToast(
          responseStatus.responseStatus?.message,
          isSuccess: true,
        );
      } else {
        appWidgets.showSimpleToast(
          responseStatus.responseStatus?.error,
        );
      }
    }
  }
}

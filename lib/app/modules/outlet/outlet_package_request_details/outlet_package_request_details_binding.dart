import 'package:get/get.dart';

import 'outlet_package_request_details_controller.dart';

class OutletPackageRequestDetailsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OutletPackageRequestDetailsController>(
      () => OutletPackageRequestDetailsController(),
    );
  }
}

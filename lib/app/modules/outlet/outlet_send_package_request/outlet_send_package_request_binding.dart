import 'package:get/get.dart';

import 'outlet_send_package_request_controller.dart';

class OutletSendPackageRequestBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OutletSendPackageRequestController>(
      () => OutletSendPackageRequestController(),
    );
  }
}

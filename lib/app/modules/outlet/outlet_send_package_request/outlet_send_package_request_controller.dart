import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_constants.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/plant_list_model.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/status_model.dart';
import 'package:supply_safe_brolier_meat/app/network/api_url.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/plant/plant_stock_model.dart'
    as stock_data;
import '../../../data/model/drop_down_model.dart';
import '../package_list/package_list_controller.dart';

class OutletSendPackageRequestController extends GetxController {
  List<TextEditingController> textController = [];
  final Map<dynamic, dynamic> quantityMap = {};
  final packageListController = Get.find<PackageListController>();
  final List<stock_data.Data> items = [];
  final plantListData = <Data>[].obs;
  final plantList = <MyDropDownModel>[].obs;
  final selectedPlant = MyDropDownModel().obs;

  @override
  void onInit() {
    super.onInit();
    getPlantList();
  }

  getPlantList() async {
    var response = await apiClient.getAPI(getPlantUrl, getPlantList);
    if (response != null) {
      PlantListModel plantListModel =
          plantListModelFromJson(response.toString());
      if (plantListModel.responseStatus?.success ?? false) {
        plantListData.value = plantListModel.data ?? [];
        plantList.clear();
        for (var element in plantListData) {
          plantList.add(MyDropDownModel(
            id: element.id.toString(),
            title: element.name.toString(),
          ));
        }
      } else {
        Get.back();
        appWidgets.showSimpleToast(plantListModel.responseStatus?.message);
      }
    }
  }

  onTapNext() {
    if (quantityMap.isEmpty) {
      appWidgets.showSimpleToast("তথ্য প্রদান করুন", isInfo: true);
    } else {
      sendRequest();
    }
  }

  sendRequest() async {
    var data = <String, dynamic>{};
    data["plant_id"] = selectedPlant.value.id ?? "";
    data["outlet_package_request_details"] = quantityMap.entries
        .map((entry) => {"package_id": entry.key, "quantity": entry.value})
        .toList();

    var response = await apiClient.postAPI(
      sendRequestByOutletUrl,
      data,
      sendRequest,
      isApplicationJson: true,
    );
    if (response != null) {
      StatusModel statusModel = statusModelFromJson(response.toString());
      if (statusModel.responseStatus?.success ?? false) {
        Get.back();
        appWidgets.showSimpleToast(
          statusModel.responseStatus?.message,
          isSuccess: true,
        );
      } else {
        appWidgets.showSimpleToast(
          statusModel.responseStatus?.error,
        );
      }
    }
  }

  handlePlantClick(value) {
    selectedPlant.value = value;
    textController = List.generate(packageListController.stockData.length,
        (index) => TextEditingController());
  }
}

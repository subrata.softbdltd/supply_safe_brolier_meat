import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:soft_builder/soft_builder.dart'; // Assuming you have this package for some specific functionality
import 'package:supply_safe_brolier_meat/app/core/constants/app_constants.dart';

import '../../../core/constants/app_colors.dart';
import '../../../core/widgets/app_button.dart';
import '../../../core/widgets/app_defaults/app_app_bar.dart';
import '../../../core/widgets/app_drop_down.dart';
import 'outlet_send_package_request_controller.dart';

class OutletSendPackageRequestView
    extends GetView<OutletSendPackageRequestController> {
  const OutletSendPackageRequestView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppAppBar(
        title: "প্যাকেজ অনুরোধ পাঠান",
        titleColor: AppColor.white,
        titleFontSize: 20.sp,
        needWhiteBackButtonColor: true,
        backgroundColor: AppColor.plantPrimaryColor,
      ),
      bottomNavigationBar: Container(
        height: 100,
        padding: mainPaddingAll,
        child: AppButton(
          onPressed: controller.onTapNext,
          text: "চেকআউট করুন",
          fontSize: 20,
          backgroundColor: AppColor.plantPrimaryColor,
          fontWeight: FontWeight.w500,
        ),
      ),
      body: Padding(
        padding: mainPaddingAll,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Obx(() {
                return AppDropDown(
                  title: "Pant List",
                  needSearch: true,
                  hint: "Select your plant",
                  isRequired: true,
                  dropDownItems: controller.plantList.toList(),
                  selectedItem: controller.selectedPlant.value,
                  handleClick: controller.handlePlantClick,
                );
              }),
              gapH12,
              Obx(() {
                return Visibility(
                  visible: controller.selectedPlant.value?.id != null,
                  child: ListView.separated(
                    itemCount:
                        controller.packageListController.stockData.length,
                    shrinkWrap: true,
                    physics: const BouncingScrollPhysics(),
                    itemBuilder: (context, index) {
                      final data =
                          controller.packageListController.stockData[index];
                      return MyEditText(
                        isRequired: false,
                        isNumberKeyboard: true,
                        isSignedNumberKeyboard: true,
                        title: data.name ?? "",
                        controller: controller.textController[index],
                        onChanged: (value) {
                          if (value.isNotEmpty) {
                            controller.quantityMap[data.id] = int.parse(value);
                          } else {
                            controller.quantityMap.remove(data.id);
                          }
                        },
                      );
                    },
                    separatorBuilder: (context, index) {
                      return gapH12;
                    },
                  ),
                );
              }),
            ],
          ),
        ),
      ),
    );
  }
}

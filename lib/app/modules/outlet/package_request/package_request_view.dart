import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_constants.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/asset_constants.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/my_text_style.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/package_request_model.dart';
import '../../../core/constants/app_colors.dart';
import '../../../core/widgets/app_defaults/app_app_bar.dart';
import '../../../core/widgets/tab_bar_header.dart';
import 'package_request_controller.dart';

class PackageRequestView extends GetView<PackageRequestController> {
  const PackageRequestView({super.key});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        backgroundColor: AppColor.mainBG,
        appBar: AppAppBar(
          title: "প্যাকেজ অনুরোধ",
          titleColor: AppColor.white,
          titleFontSize: 20.sp,
          needWhiteBackButtonColor: true,
          backgroundColor: AppColor.plantPrimaryColor,
        ),
        body: Padding(
          padding: mainPaddingAll,
          child: Column(
            children: [
              headerSection(),
              gapH12,
              Expanded(
                child: TabBarView(
                  physics: const NeverScrollableScrollPhysics(),
                  children: List.generate(3, (_) => movementTabBar(controller)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget headerSection() {
    return Obx(() {
      return TabBarHeader(
        onTap: (index) {
          if(index == 0){
            controller.status.value = 1;
          }else if(index == 1){
            controller.status.value = 2;
          }else{
            controller.status.value = 3;
          }
          controller.selectedIndex.value = index;
          controller.pageNo = 1;
          controller.packagesData.clear();
          controller.getPackagesStatusList();
        },
        tabTitles: const [
          "অপেক্ষমান",
          "বিতরণকৃত",
          "বাতিলকৃত",
        ],
        selectedIndex: controller.selectedIndex.value,
      );
    });
  }

  Widget movementTabBar(PackageRequestController controller) {
    return Obx(() {
      if (controller.isLoading.value && controller.pageNo == 1) {
        return appWidgets.circularProgressBar();
      } else if (controller.packagesData.isEmpty) {
        return appWidgets.noData();
      } else {
        return NotificationListener<ScrollNotification>(
          onNotification: (scrollNotification) {
            if (scrollNotification is ScrollEndNotification &&
                controller.scrollController.position.extentAfter == 0) {
              if (controller.isMoreDataAvailable.value) {
                controller.getPackagesStatusList();
              }
            }
            return false;
          },
          child: ListView.separated(
            itemCount: controller.packagesData.length +
                (controller.isMoreDataAvailable.value ? 1 : 0),
            controller: controller.scrollController,
            shrinkWrap: true,
            physics: const BouncingScrollPhysics(),
            itemBuilder: (context, index) {
              if (index == controller.packagesData.length &&
                  controller.isMoreDataAvailable.value) {
                return appWidgets.circularProgressBar();
              }
              final data = controller.packagesData[index];
              return Padding(
                padding: EdgeInsets.only(
                  bottom: index == controller.packagesData.length - 1 ? 50 : 0,
                ),
                child: packageRequestCard(
                    data: data,
                    onTap: () {
                      controller.onGoDetails(data);
                    }),
              );
            },
            separatorBuilder: (context, index) {
              return gapH12;
            },
          ),
        );
      }
    });
  }

  Widget packageRequestCard({
    required Data data,
    required Function() onTap,
  }) {
    return Material(
      borderRadius: BorderRadius.circular(defaultBorderRadius),
      child: InkWell(
        onTap: onTap,
        borderRadius: BorderRadius.circular(defaultBorderRadius),
        child: Ink(
            padding: mainPaddingAll,
            decoration: BoxDecoration(
              color: const Color(0xffF7F9F2),
              borderRadius: BorderRadius.circular(defaultBorderRadius),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  outletImage,
                  height: 22.w,
                  width: 22.w,
                ),
                gapW16,
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Name: ${data.plant?.name ?? ""}',
                      style: text18Style(),
                    ),
                    gapH3,
                    Text(
                      'Phone: ${data.plant?.phone ?? ""}',
                      style: text16Style(
                        isWeight400: true,
                      ),
                    ),
                    gapH3,
                    Text(
                        "Order Date: ${DateFormat('dd MMM yyyy').format(DateTime.parse(data.createdAt ?? ""))}"),
                    gapH3,
                    // Row(
                    //   crossAxisAlignment: CrossAxisAlignment.center,
                    //   children: [
                    //     Text(
                    //       "Status:",
                    //       style: text18Style(),
                    //     ),
                    //     gapW8,
                    //     Text(
                    //       data.status == 0
                    //           ? "তৈরি হয়েছে"
                    //           : data.status == 1
                    //               ? "স্থাপন করা হয়েছে"
                    //               : data.status == 2
                    //                   ? "বিতরণ করা হয়েছে"
                    //                   : "বাতিল",
                    //       style: text16Style(
                    //         color: data.status == 0
                    //             ? Colors.blue
                    //             : data.status == 1
                    //                 ? Colors.green
                    //                 : data.status == 2
                    //                     ? AppColor.plantPrimaryColor
                    //                     : Colors.red,
                    //       ),
                    //     ),
                    //   ],
                    // )
                  ],
                )),
                Icon(
                  Icons.arrow_forward_ios,
                  size: 22.w,
                ),
              ],
            )),
      ),
    );
  }
}

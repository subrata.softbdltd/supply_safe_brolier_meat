import 'package:get/get.dart';

import 'package_request_controller.dart';

class PackageRequestBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PackageRequestController>(
      () => PackageRequestController(),
    );
  }
}

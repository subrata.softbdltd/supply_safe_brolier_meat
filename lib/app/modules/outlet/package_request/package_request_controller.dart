import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_constants.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/package_request_model.dart';

import '../../../data/pojo/Response_model.dart';
import '../../../network/api_url.dart';
import '../../../routes/app_pages.dart';

class PackageRequestController extends GetxController {
  ScrollController scrollController = ScrollController();
  final packagesData = <Data>[].obs;
  final selectedData = Data().obs;
  final isLoading = false.obs, isMoreDataAvailable = true.obs;
  final selectedProjectName = "".obs;
  final status = 1.obs, selectedIndex = 0.obs;
  int pageNo = 1, pageSize = 10;

  @override
  void onInit() {
    super.onInit();
    getPackagesStatusList();
  }

  Future<void> getPackagesStatusList() async {
    isLoading.value = true;
    var mQueryParameter = <String, dynamic>{};
    mQueryParameter["status"] = status;
    mQueryParameter["page"] = pageNo;
    mQueryParameter["page_size"] = pageSize;
    mQueryParameter["order"] = "DESC";

    var response = await apiClient.getAPI(
      packageRequestUrl,
      getPackagesStatusList,
      mQueryParameters: mQueryParameter,
    );

    isLoading.value = false;

    if (response != null) {
      PackageRequestModel packageRequestModel =
          packageRequestModelFromJson(response.toString());

      if (packageRequestModel.responseStatus?.success ?? false) {
        if (pageNo == 1) {
          packagesData.clear();
        }

        if (packageRequestModel.data?.isNotEmpty ?? false) {
          packagesData.addAll(packageRequestModel.data!);
          isMoreDataAvailable.value =
              packageRequestModel.data!.length == pageSize;
        } else {
          isMoreDataAvailable.value = false;
        }
      } else {
        appWidgets.showSimpleToast("Something went wrong");
      }
    }
  }

  onGoDetails(data) {
    selectedData.value = data ?? Data();
    Get.toNamed(Routes.OUTLET_PACKAGE_REQUEST_DETAILS)?.then((value){
      getPackagesStatusList();
    });
  }


}

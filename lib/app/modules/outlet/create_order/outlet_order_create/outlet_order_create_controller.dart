import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/Response_model.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/plant/plant_stock_model.dart';

import '../../../../core/constants/app_constants.dart';
import '../../../../network/api_url.dart';

class OutletOrderCreateController extends GetxController {
  TextEditingController phoneNumberController = TextEditingController(),
      userNameController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  final ScrollController scrollController = ScrollController();
  final stockData = <Data>[].obs;

  final counts = <RxInt>[].obs;

  @override
  void onInit() {
    super.onInit();
    getPlantStock();
  }

  getPlantStock() async {
    var response = await apiClient.getAPI(packageStockUrl, getPlantStock);
    if (response != null) {
      PlantStockModel plantStockModel =
          plantStockModelFromJson(response.toString());
      if (plantStockModel.responseStatus?.success ?? false) {
        stockData.value = plantStockModel.data ?? [];
        counts.value = List.generate(stockData.length, (index) => 0.obs);
      } else {

        appWidgets.showSimpleToast(plantStockModel.responseStatus?.error);
      }
    }
  }

  onOrderSubmit() {
    if (formKey.currentState?.validate() ?? false) {
      createOrder();
    }
  }

  createOrder() async {
    var data = <String, dynamic>{};
    data["phone"] = phoneNumberController.text;
    data["name"] = userNameController.text;
    data["items"] = [];

    for (int i = 0; i < stockData.length; i++) {
      if (counts[i].value > 0) {
        data["items"].add({
          "package_id": stockData[i].id,
          "quantity": counts[i].value,
        });
      }
    }

    var response = await apiClient.postAPI(createOrderUrl, data, createOrder,
       isApplicationJson: false);
    if (response != null) {
      ResponseModel responseModel = responseModelFromJson(response.toString());
      if (responseModel.responseStatus?.success ?? false) {
        Get.back();
        appWidgets.showSimpleToast("সফলভাবে জমা দেওয়া হয়েছে",
            isSuccess: true);
        phoneNumberController.clear();
        userNameController.clear();
      }else{
        appWidgets.showSimpleToast(responseModel.responseStatus?.error);

      }
    }
  }

  void incrementCount(int index) {
    counts[index]++;
  }

  void decrementCount(int index) {
    if (counts[index] > 0) {
      counts[index]--;
    }
  }
}

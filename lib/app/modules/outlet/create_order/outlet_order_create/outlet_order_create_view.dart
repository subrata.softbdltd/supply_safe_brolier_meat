import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:soft_builder/widget/my_edit_text.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_colors.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/my_text_style.dart';
import 'package:supply_safe_brolier_meat/app/core/widgets/app_button.dart';

import '../../../../core/constants/app_constants.dart';
import '../../../../core/widgets/app_defaults/app_app_bar.dart';
import '../../../../routes/app_pages.dart';
import 'outlet_order_create_controller.dart';

class OutletOrderCreateView extends GetView<OutletOrderCreateController> {
  const OutletOrderCreateView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundColor,
      appBar: AppAppBar(
        titleFontSize: 20.sp,
        needWhiteBackButtonColor: true,
        backgroundColor: AppColor.plantPrimaryColor,
      ),
      body: SafeArea(
        child: Padding(
          padding: mainPaddingAll,
          child: Form(
            key: controller.formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                gapH20,
                Text(
                  "অর্ডার তৈরি করুন",
                  style: text12Style(isWeight600: true, fontSize: 22),
                ),
                appWidgets.divider(color: Colors.grey),
                gapH12,
                MyEditText(
                  title: "ব্যবহারকারীর নাম লিখুন",
                  isRequired: true,
                  controller: controller.userNameController,
                ),
                MyEditText(
                  title: "ব্যবহারকারীর ফোন নম্বর",
                  isMobileNumber: true,
                  isRequired: true,
                  controller: controller.phoneNumberController,
                ),
                gapH20,
                Expanded(
                  child: Obx(
                    () => ListView.separated(
                      physics: const BouncingScrollPhysics(),
                      controller: controller.scrollController,
                      itemCount: controller.stockData.length,
                      itemBuilder: (context, index) {
                        final item = controller.stockData[index];
                        return Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 10.w, vertical: 15.h),
                          decoration: BoxDecoration(
                              borderRadius: defaultBorder,
                              color: AppColor.white),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        item.name ?? "",
                                        style: text18Style(),
                                      ),
                                      Text(
                                        'Price: ${item.price} Discount: ${item.discount} Weight: ${item.weight} ${item.weightUnit}',
                                        style: text12Style(),
                                      ),
                                    ],
                                  ),
                                ),
                                IconButton(
                                  icon: const Icon(Icons.remove),
                                  onPressed: () {
                                    controller.decrementCount(index);
                                  },
                                ),
                                gapW8,
                                Obx(() {
                                  return Text(
                                    "${controller.counts[index]}",
                                    style: text18Style(),
                                  );
                                }),
                                gapW8,
                                IconButton(
                                  icon: const Icon(Icons.add),
                                  onPressed: () {
                                    controller.incrementCount(index);
                                  },
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) {
                        return gapH12;
                      },
                    ),
                  ),
                ),
                gapH12,
                AppButton(
                  text: "জমা দিন",
                  onPressed: controller.onOrderSubmit,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

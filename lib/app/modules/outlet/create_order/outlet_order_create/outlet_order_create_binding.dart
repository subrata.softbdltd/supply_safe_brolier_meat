import 'package:get/get.dart';

import 'outlet_order_create_controller.dart';

class OutletOrderCreateBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OutletOrderCreateController>(
      () => OutletOrderCreateController(),
    );
  }
}

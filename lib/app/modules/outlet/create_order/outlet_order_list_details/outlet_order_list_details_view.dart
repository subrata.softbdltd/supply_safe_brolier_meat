import 'package:dynamic_height_grid_view/dynamic_height_grid_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:intl/intl.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/create_order_details_model.dart';
import 'package:get/get.dart';

import '../../../../core/constants/app_colors.dart';
import '../../../../core/constants/app_constants.dart';
import '../../../../core/constants/asset_constants.dart';
import '../../../../core/constants/my_text_style.dart';
import '../../../../core/widgets/app_network_image.dart';
import 'outlet_order_list_details_controller.dart';

class OutletOrderListDetailsView
    extends GetView<OutletOrderListDetailsController> {
  const OutletOrderListDetailsView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundColor,
      body: SafeArea(
        child: Padding(
          padding: mainPaddingAll,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: Icon(
                    Icons.arrow_back_ios,
                    size: 25.w,
                  )),
              gapH12,
              Obx(() {
                return Expanded(
                    child: AnimationLimiter(
                  child: DynamicHeightGridView(
                    controller: controller.scrollController,
                    shrinkWrap: true,
                    physics: const BouncingScrollPhysics(),
                    crossAxisCount: 2,
                    itemCount: controller.items.length ?? 0,
                    builder: (context, index) {
                      final data = controller.items[index];
                      return AnimationConfiguration.staggeredGrid(
                        position: index,
                        columnCount: 2,
                        duration: const Duration(milliseconds: 375),
                        child: ScaleAnimation(
                          delay: const Duration(milliseconds: 250),
                          child: FadeInAnimation(
                            child: cardItems(
                              context,
                              data,
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ));
              }),
            ],
          ),
        ),
      ),
    );
  }

  Widget profileImage({required String userName}) {
    return CircleAvatar(
      radius: 60,
      backgroundColor: const Color(0xff153448),
      child: CircleAvatar(
        backgroundColor: AppColor.primaryColor,
        radius: 55,
        child: Text(
          userName,
          style:
              text20Style(fontSize: 40, isWeight500: true, color: Colors.white),
        ),
      ),
    );
  }

  Widget cardItems(
    BuildContext context,
    Items data,
  ) {
    return Material(
      borderRadius: BorderRadius.circular(12),
      child: Ink(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            gapH12,
            AppNetworkImage(
              imageUrl: data.package?.thumbnail ?? "",
              noImage: noDataFound,
              width: double.infinity,
              assetImageColor: Colors.grey,
              assetWidth: 80.0,
            ),
            gapH3,
            Padding(
              padding: EdgeInsets.only(left: 10.w, bottom: 10.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Text(
                      data.package?.name ?? "",
                      textAlign: TextAlign.center,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: text16Style(
                        isWeight600: true,
                      ),
                    ),
                  ),
                  gapH12,
                  Text(
                    "দাম: ${data.package?.price ?? 0}",
                    style: text12Style(),
                  ),
                  Text(
                    "প্রতি প্যাকেজ ওজন: ${data.package?.weight ?? 0} ${data.package?.weightUnit}",
                    style: text12Style(),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }


}

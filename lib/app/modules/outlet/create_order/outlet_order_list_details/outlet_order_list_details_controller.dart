import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_constants.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/create_order_details_model.dart';
import 'package:supply_safe_brolier_meat/app/network/api_url.dart';

class OutletOrderListDetailsController extends GetxController {
  final ScrollController scrollController = ScrollController();
  final items = <Items>[].obs;
  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
    if(Get.arguments != null){
      getOrderDetails(Get.arguments);
    }
  }

  getOrderDetails(id) async {
    var response =
        await apiClient.getAPI("$createOrderUrl/$id", getOrderDetails);
    if (response != null) {
      CreateOrderDetailsModel createOrderDetailsModel = createOrderDetailsModelFromJson(response.toString());
      if(createOrderDetailsModel.responseStatus?.success ?? false){
        items.value = createOrderDetailsModel.data?.items ?? [];

      }else{
        Get.back();
        appWidgets.showSimpleToast(createOrderDetailsModel.responseStatus?.message);
      }
    }
  }
}

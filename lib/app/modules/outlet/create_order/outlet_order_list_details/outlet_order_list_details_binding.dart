import 'package:get/get.dart';

import 'outlet_order_list_details_controller.dart';

class OutletOrderListDetailsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OutletOrderListDetailsController>(
      () => OutletOrderListDetailsController(),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/order_details_model.dart';

import '../../../../core/constants/app_constants.dart';
import '../../../../data/pojo/Response_model.dart';
import '../../../../network/api_client.dart';
import '../../../../network/api_url.dart';
import '../outlet_order_create_list/outlet_order_create_list_controller.dart';

class OrderDetailsController extends GetxController {
  final ScrollController scrollController = ScrollController();
  final orderSectionController = Get.find<OutletOrderCreateListController>();
  final orderDetails = Data().obs;
  final isLoading = true.obs;
  final orderId = 0.obs;

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
    if (Get.arguments != null) {
      orderId.value = Get.arguments;
      getOrderDetails(Get.arguments);
    }
  }
  getOrderDetails(id) async {
    isLoading.value = true;
    var response = await ApiClient().getAPI("$createOrderUrl/$id", getOrderDetails);
    if (response != null) {
      OrderDetailsModel orderDetailsModel =
      orderDetailsModelFromJson(response.toString());
      isLoading.value = false;
      if (orderDetailsModel.responseStatus?.success ?? false) {
        orderDetails.value = orderDetailsModel.data ?? Data();
      } else {
        isLoading.value = false;
        Get.back();
        appWidgets.showSimpleToast(orderDetailsModel.responseStatus?.message);
      }
    }
  }


  clickOnDelete() {
    appWidgets.simpleStatusAlertDialog(
      headTitle: "আপনি কি নিশ্চিত এই অর্ডার বাতিল করতে চান?",
      yesTap: () {
        Get.back();
        Get.back();
        orderDelete(orderId.value);
      },
      noTap: () {
        Get.back();
      },
    );
  }

  orderDelete(id) async {
    var data = <String, dynamic>{};
    data["status"] = 3;
    var response =
    await ApiClient().putAPI("$createOrderUrl/$id", data, orderDelete);
    if(response != null){
      ResponseModel responseModel = responseModelFromJson(response.toString());
      if(responseModel.responseStatus?.success ?? false){
        orderSectionController.getOrderStatusList();
        appWidgets.showSimpleToast(responseModel.responseStatus?.message, isSuccess: true);
      }else{
        appWidgets.showSimpleToast(responseModel.responseStatus?.message);
      }
    }
  }

  // onGoPackageDetails(id) {
  //   Get.toNamed(
  //     Routes.PRODUCT_DETAILS,
  //     arguments: {
  //       "id": id,
  //     },
  //   );
  // }

}

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:supply_safe_brolier_meat/app/routes/app_pages.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/order_data_model.dart';
import '../../../../core/constants/app_colors.dart';
import '../../../../core/constants/app_constants.dart';
import '../../../../core/constants/asset_constants.dart';
import '../../../../core/constants/my_text_style.dart';
import '../../../../core/widgets/app_defaults/app_app_bar.dart';
import '../../../../core/widgets/tab_bar_header.dart';
import 'outlet_order_create_list_controller.dart';

class OutletOrderCreateListView
    extends GetView<OutletOrderCreateListController> {
  const OutletOrderCreateListView({super.key});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppAppBar(
          title: "অর্ডার তালিকা",
          titleColor: AppColor.white,
          titleFontSize: 20.sp,
          needWhiteBackButtonColor: true,
          backgroundColor: AppColor.plantPrimaryColor,
          needActionButton: true,
          needIconButton: true,
          needActionButtonList: true,
          actionOnPressed: () {
            Get.toNamed(Routes.OUTLET_ORDER_CREATE)?.then((value) {
              controller.getOrderStatusList();
            });
          },
        ),
        backgroundColor: AppColor.backgroundColor,
        body: SafeArea(
          child: Padding(
            padding: mainPaddingAll,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // iconButton(),
                // Text(
                //   "অর্ডার তালিকা",
                //   style: text12Style(isWeight600: true, fontSize: 22),
                // ),

                Expanded(
                  child: Padding(
                    padding: mainPaddingAll,
                    child: Column(
                      children: [
                        headerSection(),
                        gapH12,
                        Expanded(
                          child: TabBarView(
                            physics: const NeverScrollableScrollPhysics(),
                            children: List.generate(
                                3, (_) => movementTabBar(controller)),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget iconButton() {
    return Align(
      alignment: Alignment.topRight,
      child: IconButton(
          onPressed: () {
            Get.toNamed(Routes.OUTLET_ORDER_CREATE)?.then((value) {
              controller.getOrderStatusList();
            });
          },
          icon: Icon(
            Icons.add,
            size: 30.w,
            color: Colors.black,
          )),
    );
  }

  Widget headerSection() {
    return Obx(() {
      return TabBarHeader(
        onTap: (index) {
          if (index == 0) {
            controller.status.value = 1;
          } else if (index == 1) {
            controller.status.value = 2;
          }
          if (index == 2) {
            controller.status.value = 3;
          }

          controller.selectedIndex.value = index;
          controller.pageNo = 1;
          controller.orderData.clear();
          controller.getOrderStatusList();
        },
        tabTitles: const [
          "অপেক্ষমান",
          "বিতরণকৃত",
          "বাতিলকৃত",
        ],
        selectedIndex: controller.selectedIndex.value,
      );
    });
  }

  Widget movementTabBar(OutletOrderCreateListController controller) {
    return Obx(() {
      if (controller.isLoading.value && controller.pageNo == 1) {
        return appWidgets.circularProgressBar();
      } else if (controller.orderData.isEmpty) {
        return appWidgets.noDataFoundMsg();
      } else {
        return NotificationListener<ScrollNotification>(
          onNotification: (scrollNotification) {
            if (scrollNotification is ScrollEndNotification &&
                controller.scrollController.position.extentAfter == 0) {
              if (controller.isMoreDataAvailable.value) {
                controller.getOrderStatusList();
              }
            }
            return false;
          },
          child: ListView.separated(
            itemCount: controller.orderData.length +
                (controller.isMoreDataAvailable.value ? 1 : 0),
            controller: controller.scrollController,
            shrinkWrap: true,
            physics: const BouncingScrollPhysics(),
            itemBuilder: (context, index) {
              if (index == controller.orderData.length &&
                  controller.isMoreDataAvailable.value) {
                return appWidgets.circularProgressBar();
              }
              final data = controller.orderData[index];
              return Padding(
                padding: EdgeInsets.only(
                  bottom: index == controller.orderData.length - 1 ? 50 : 0,
                ),
                child: packageRequestCard(
                    data: data,
                    onTap: () {
                      controller.onGoDetails(data.id);
                    }),
              );
            },
            separatorBuilder: (context, index) {
              return gapH12;
            },
          ),
        );
      }
    });
  }

  Widget packageRequestCard({
    required Data data,
    required Function() onTap,
  }) {
    return Material(
      borderRadius: BorderRadius.circular(defaultBorderRadius),
      child: InkWell(
        onTap: onTap,
        borderRadius: BorderRadius.circular(defaultBorderRadius),
        child: Ink(
            padding: mainPaddingAll,
            decoration: BoxDecoration(
              color: const Color(0xffF7F9F2),
              borderRadius: BorderRadius.circular(defaultBorderRadius),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  productImage,
                  height: 40.w,
                  width: 40.w,
                ),
                gapW16,
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                        "অর্ডার নং: ${data.orderNo}"),
                    gapH3,
                    Text(
                        "অর্ডারের তারিখ: ${DateFormat('dd MMM yyyy').format(DateTime.parse(data.date ?? ""))}"),
                    gapH3,
                    Text(
                      'দাম: ${data.payableAmount ?? ""}',
                      style: text16Style(
                        isWeight400: true,
                      ),
                    ),
                  ],
                )),
                Icon(
                  Icons.arrow_forward_ios,
                  size: 15.w,
                ),
              ],
            )),
      ),
    );
  }
}

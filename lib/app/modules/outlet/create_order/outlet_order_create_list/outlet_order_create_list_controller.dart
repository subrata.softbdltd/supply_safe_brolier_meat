import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_constants.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/order_data_model.dart';

import '../../../../core/state/global_state.dart';
import '../../../../network/api_client.dart';
import '../../../../network/api_url.dart';
import '../../../../routes/app_pages.dart';

class OutletOrderCreateListController extends GetxController {

  ScrollController scrollController = ScrollController();
  final globalState = Get.find<GlobalState>();
  var orderData = <Data>[].obs;
  final isLoading = false.obs, isMoreDataAvailable = true.obs;
  final selectedProjectName = "".obs;
  final status = 1.obs, selectedIndex = 0.obs;
  int pageNo = 1, pageSize = 10;

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
    if(globalState.isLoggedIn.value){
      getOrderStatusList();
      scrollController.addListener(scrollListener);
    }
  }
  void scrollListener() {
    if (scrollController.position.pixels ==
        scrollController.position.maxScrollExtent) {
      if (isMoreDataAvailable.value && !isLoading.value) {
        pageNo++;
        getOrderStatusList();
      }
    }
  }

  Future<void> getOrderStatusList() async {
    if (isLoading.value) return;
    if (pageNo == 1) {
      orderData.clear();
    }
    isLoading.value = true;
    var mQueryParameter = <String, dynamic>{};
    mQueryParameter["status"] = status;
    mQueryParameter["page"] = pageNo;
    mQueryParameter["page_size"] = pageSize;
    mQueryParameter["order"] = "DESC";

    var response = await ApiClient().getAPI(
      createOrderUrl,
      getOrderStatusList,
      mQueryParameters: mQueryParameter,
    );

    isLoading.value = false;

    if (response != null) {
      OrderDataModel orderModel = orderDataModelFromJson(response.toString());

      if (orderModel.responseStatus?.success ?? false) {
        if (pageNo == 1) {
          orderData.clear();
        }

        if (orderModel.data?.isNotEmpty ?? false) {
          orderData.addAll(orderModel.data!);
          isMoreDataAvailable.value = orderModel.data!.length == pageSize;
        } else {
          isMoreDataAvailable.value = false;
        }
      } else {
        appWidgets.showSimpleToast("Something went wrong");
      }
    }
  }

  onGoDetails(id) {
    Get.toNamed(
      Routes.ORDER_DETAILS,
      arguments: id,
    );
  }


}

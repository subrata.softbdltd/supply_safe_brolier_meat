import 'package:get/get.dart';

import 'outlet_order_create_list_controller.dart';

class OutletOrderCreateListBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OutletOrderCreateListController>(
      () => OutletOrderCreateListController(),
    );
  }
}

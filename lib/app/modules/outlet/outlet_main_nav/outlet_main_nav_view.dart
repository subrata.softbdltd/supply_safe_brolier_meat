import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:supply_safe_brolier_meat/app/modules/outlet/create_order/outlet_order_create_list/outlet_order_create_list_controller.dart';
import 'package:supply_safe_brolier_meat/app/modules/outlet/create_order/outlet_order_create_list/outlet_order_create_list_view.dart';
import 'package:supply_safe_brolier_meat/app/modules/outlet/outlet_profile/outlet_profile_controller.dart';
import 'package:supply_safe_brolier_meat/app/modules/outlet/outlet_profile/outlet_profile_view.dart';
import 'package:supply_safe_brolier_meat/app/modules/outlet/package_list/package_list_controller.dart';
import 'package:supply_safe_brolier_meat/app/modules/outlet/package_list/package_list_view.dart';
import 'package:supply_safe_brolier_meat/app/modules/outlet/package_request/package_request_controller.dart';
import 'package:supply_safe_brolier_meat/app/modules/outlet/package_request/package_request_view.dart';

import '../../../core/constants/app_colors.dart';
import '../../../core/constants/asset_constants.dart';
import '../../../core/widgets/app_widgets.dart';
import 'outlet_main_nav_controller.dart';

class OutletMainNavView extends GetView<OutletMainNavController> {
  const OutletMainNavView({super.key});
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return AppWidget().appExitConfirmation();
      },
      child: Scaffold(
          bottomNavigationBar: myBottomNavigationBar(),
          body: Obx(() {
            return IndexedStack(
              index: controller.currentIndex.value,
              children: const [
                PackageListView(),
                PackageRequestView(),
                OutletOrderCreateListView(),
                OutletProfileView(),
              ],
            );
          })),
    );
  }

  Widget myBottomNavigationBar() {
    double paddingVertical = 12.h;
    double paddingHorizontal = 12.w;

    return Obx(() {
      return Container(
        decoration: BoxDecoration(
          color: AppColor.white,
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(15),
            topRight: Radius.circular(15),
          ),
          boxShadow: [
            BoxShadow(
              color: AppColor.primaryColor.withOpacity(0.1),
              blurRadius: 10,
              spreadRadius: 8,
            )
          ],
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 12.h, horizontal: 10.w),
          child: GNav(
              selectedIndex: controller.currentIndex.value,
              onTabChange: (index) {
                controller.currentIndex.value = index;
                switch (index) {
                  case 0:
                    Get.find<PackageListController>().onInit();
                    break;
                  case 1:
                    Get.find<PackageRequestController>().onInit();
                    break;
                  case 2:
                    Get.find<OutletOrderCreateListController>().onInit();
                    break;
                  case 3:
                    Get.find<OutletProfileController>().onInit();
                    break;
                  default:
                }
              },
              gap: 5.w,
              backgroundColor: AppColor.white,
              color: AppColor.black.withOpacity(.7),
              activeColor: AppColor.white,
              tabBackgroundColor: AppColor.plantPrimaryColor,
              tabBorderRadius: 50.r,
              style: GnavStyle.google,
              duration: const Duration(milliseconds: 500),
              padding: EdgeInsets.symmetric(vertical: 12.h, horizontal: 20.w),
              tabs: [
                GButton(
                  leading: Image.asset(
                    homeIcon,
                    width: 25,
                    color: controller.currentIndex.value == 0
                        ? AppColor.white
                        : AppColor.primaryColor,
                  ),
                  icon: CupertinoIcons.home,
                  text: "Home",
                  textColor: AppColor.white,
                  backgroundColor: AppColor.plantPrimaryColor,
                  padding: EdgeInsets.symmetric(
                    vertical: paddingVertical,
                    horizontal: paddingHorizontal,
                  ),
                ),
                GButton(
                  leading: Image.asset(
                    packageIcon,
                    width: 25,
                    color: controller.currentIndex.value == 1
                        ? AppColor.white
                        : AppColor.primaryColor,
                  ),
                  icon: Icons.crop_landscape,
                  text: "Package",
                  textColor: AppColor.white,
                  backgroundColor: AppColor.plantPrimaryColor,
                  padding: EdgeInsets.symmetric(
                    vertical: paddingVertical,
                    horizontal: paddingHorizontal,
                  ),
                ),
                GButton(
                  leading: Image.asset(
                    orderImage,
                    width: 25,
                    color: controller.currentIndex.value == 2
                        ? AppColor.white
                        : AppColor.primaryColor,
                  ),
                  icon: Icons.crop_landscape,
                  text: "Order",
                  textColor: AppColor.white,
                  backgroundColor: AppColor.plantPrimaryColor,
                  padding: EdgeInsets.symmetric(
                    vertical: paddingVertical,
                    horizontal: paddingHorizontal,
                  ),
                ),
                GButton(
                  leading: Image.asset(
                    settingIcon,
                    width: 25,
                    color: controller.currentIndex.value == 3
                        ? AppColor.white
                        : AppColor.primaryColor,
                  ),
                  icon: CupertinoIcons.settings,
                  iconColor: AppColor.white,
                  text: "Profile",
                  textColor: AppColor.white,
                  backgroundColor: AppColor.plantPrimaryColor,
                  padding: EdgeInsets.symmetric(
                    vertical: paddingVertical,
                    horizontal: paddingHorizontal,
                  ),
                ),
              ]),
        ),
      );
    });
  }
}

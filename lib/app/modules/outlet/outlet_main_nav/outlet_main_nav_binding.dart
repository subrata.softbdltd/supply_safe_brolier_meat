import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/modules/outlet/create_order/outlet_order_create_list/outlet_order_create_list_controller.dart';
import 'package:supply_safe_brolier_meat/app/modules/outlet/outlet_profile/outlet_profile_controller.dart';
import 'package:supply_safe_brolier_meat/app/modules/outlet/package_list/package_list_controller.dart';
import 'package:supply_safe_brolier_meat/app/modules/outlet/package_request/package_request_controller.dart';

import 'outlet_main_nav_controller.dart';

class OutletMainNavBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OutletMainNavController>(
      () => OutletMainNavController(),
    );
    Get.lazyPut<PackageListController>(
      () => PackageListController(),
    );
    Get.lazyPut<PackageRequestController>(
      () => PackageRequestController(),
    );
    Get.lazyPut<OutletOrderCreateListController>(
      () => OutletOrderCreateListController(),
    );
    Get.lazyPut<OutletProfileController>(
      () => OutletProfileController(),
    );
  }
}

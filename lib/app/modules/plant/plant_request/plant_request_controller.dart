import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_constants.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/package_request_model.dart';
import 'package:supply_safe_brolier_meat/app/modules/Farmer/home/home_controller.dart';
import 'package:supply_safe_brolier_meat/app/modules/plant/plant_home/plant_home_controller.dart';

import '../../../data/pojo/Response_model.dart';
import '../../../network/api_url.dart';
import '../../../routes/app_pages.dart';
import '../stock/plant_stocks/plant_stocks_controller.dart';

class PlantRequestController extends GetxController {
  ScrollController scrollController = ScrollController();
  final plantStocksController = Get.find<PlantStocksController>();
  final packagesData = <Data>[].obs;
  final selectedData = Data().obs;
  final isLoading = false.obs, isMoreDataAvailable = true.obs;
  final selectedProjectName = "".obs;
  final status = 1.obs, selectedIndex = 0.obs;
  int pageNo = 1, pageSize = 10;


  // int waitingPage = 1;
  // int deliveredPage = 1;
  // int canceledPage = 1;
  // final int pageSize = 10;

  @override
  void onInit() {
    super.onInit();
    getPackagesStatusList();
    // getPackagesStatusList(status: 0, page: waitingPage);
    // getPackagesStatusList(status: 1, page: deliveredPage);
    // getPackagesStatusList(status: 2, page: canceledPage);
  }
  Future<void> getPackagesStatusList() async {
    isLoading.value = true;
    var mQueryParameter = <String, dynamic>{};
    mQueryParameter["status"] = status;
    mQueryParameter["page"] = pageNo;
    mQueryParameter["page_size"] = pageSize;
    mQueryParameter["order"] = "DESC";

    var response = await apiClient.getAPI(
      packageRequestUrl,
      getPackagesStatusList,
      mQueryParameters: mQueryParameter,
    );

    isLoading.value = false;

    if (response != null) {
      PackageRequestModel packageRequestModel =
      packageRequestModelFromJson(response.toString());

      if (packageRequestModel.responseStatus?.success ?? false) {
        if (pageNo == 1) {
          packagesData.clear();
        }

        if (packageRequestModel.data?.isNotEmpty ?? false) {
          packagesData.addAll(packageRequestModel.data!);
          isMoreDataAvailable.value =
              packageRequestModel.data!.length == pageSize;
        } else {
          isMoreDataAvailable.value = false;
        }
      } else {
        appWidgets.showSimpleToast("Something went wrong");
      }
    }
  }

  onGoDetails(data){
    selectedData.value = data ?? Data();
    Get.toNamed(Routes.PLANT_REQUEST_DETAILS)?.then((value){
      getPackagesStatusList();
      plantStocksController.getPlantStock();
    });
  }

  statusUpdate(id) async {
    var data = <String, dynamic>{};
    data["status"] = 2;
    var response =
        await apiClient.postAPI(plantStatusUpdateUrl(id), data, statusUpdate);
    if (response != null) {
      ResponseModel responseStatus = responseModelFromJson(response.toString());
      if (responseStatus.responseStatus?.success ?? false) {
        getPackagesStatusList();

        appWidgets.showSimpleToast(
          responseStatus.responseStatus?.message,
          isSuccess: true,
        );
      } else {}
    }
  }


}

import 'package:get/get.dart';

import 'plant_request_controller.dart';

class PlantRequestBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PlantRequestController>(
      () => PlantRequestController(),
    );
  }
}

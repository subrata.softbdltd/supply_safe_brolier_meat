import 'package:get/get.dart';

import 'plant_request_details_controller.dart';

class PlantRequestDetailsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PlantRequestDetailsController>(
      () => PlantRequestDetailsController(),
    );
  }
}

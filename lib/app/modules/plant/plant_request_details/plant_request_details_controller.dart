import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/constants/app_constants.dart';
import '../../../data/pojo/Response_model.dart';
import '../../../network/api_url.dart';
import '../plant_request/plant_request_controller.dart';

class PlantRequestDetailsController extends GetxController {
  final plantRequestController = Get.find<PlantRequestController>();
  final ScrollController scrollController = ScrollController();

  @override
  void onInit() {
    super.onInit();
  }

  deleteStatus() async {
    var data = <String, dynamic>{};
    data["status"] = 3;
    var response = await apiClient.postAPI(
        plantStatusUpdateUrl(plantRequestController.selectedData.value.id),
        data,
        deleteStatus);
    if (response != null) {
      ResponseModel responseStatus = responseModelFromJson(response.toString());
      if (responseStatus.responseStatus?.success ?? false) {
        Get.back();
        appWidgets.showSimpleToast(responseStatus.responseStatus?.message,
            isSuccess: true);

        // getPackagesStatusList();
      } else {
        logger.i(responseStatus.responseStatus?.error);
        appWidgets.showSimpleToast(
          responseStatus.responseStatus?.error,

        );
      }
    }
  }

  statusUpdate() async {
    var data = <String, dynamic>{};
    data["status"] = 2;
    var response = await apiClient.postAPI(
        plantStatusUpdateUrl(plantRequestController.selectedData.value.id),
        data,
        statusUpdate);
    if (response != null) {
      ResponseModel responseStatus = responseModelFromJson(response.toString());
      if (responseStatus.responseStatus?.success ?? false) {
        Get.back();
        appWidgets.showSimpleToast(responseStatus.responseStatus?.message,
            isSuccess: true);
        // getPackagesStatusList();

      } else {
        logger.i("responseStatus ${responseStatus.responseStatus?.error}");
        appWidgets.showSimpleToast(
          responseStatus.responseStatus?.error,
        );
      }
    }
  }
}

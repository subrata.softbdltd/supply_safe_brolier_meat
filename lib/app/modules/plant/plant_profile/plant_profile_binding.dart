import 'package:get/get.dart';

import 'plant_profile_controller.dart';

class PlantProfileBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PlantProfileController>(
      () => PlantProfileController(),
    );
  }
}

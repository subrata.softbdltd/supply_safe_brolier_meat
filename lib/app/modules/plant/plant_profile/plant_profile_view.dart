import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:soft_builder/widget/my_edit_text.dart';

import '../../../core/constants/app_colors.dart';
import '../../../core/constants/app_constants.dart';
import '../../../core/constants/asset_constants.dart';
import '../../../core/state/global_state.dart';
import '../../../core/widgets/app_button.dart';
import '../../../core/widgets/app_circle_network_image_viewer.dart';
import '../../../core/widgets/app_defaults/app_app_bar.dart';
import 'plant_profile_controller.dart';

class PlantProfileView extends GetView<PlantProfileController> {
  const PlantProfileView({super.key});
  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Scaffold(
        backgroundColor: AppColor.background,
        appBar: AppAppBar(
          title: "প্রোফাইল",
          titleColor: AppColor.white,
          needActionButtonList: true,
          titleFontSize: 20.sp,
          // iconButton: iconButton(
          //     icons: controller.needEdit.value ? Icons.close : Icons.edit,
          //     iconColor: !controller.needEdit.value ? Colors.black : Colors.red,
          //     onTap: () {
          //       logger.i(controller.needEdit.value);
          //       controller.needEdit.value = !controller.needEdit.value;
          //     }),
          backgroundColor: AppColor.plantPrimaryColor,
        ),
        body: SafeArea(
            child: SingleChildScrollView(
          padding: mainPaddingAll,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(
                child: Stack(
                  clipBehavior: Clip.none,
                  children: [
                    AppCircleNetworkImageViewer(
                      controller.globalState.userDetailsData.value.photo,
                      140,
                      assetImage: dummyImage,
                    ),
                    controller.needEdit.value
                        ? Positioned(
                            bottom: -20,
                            right: 2,
                            child: iconButton(
                                icons: Icons.add_a_photo,
                                onTap: () {},
                                iconColor: Colors.blue,
                                size: 30))
                        : const SizedBox()
                  ],
                ),
              ),
              gapH16,
              MyEditText(
                title: "",
                isReadonly: true,
                controller: controller.nameController,
              ),
              gapH16,
              MyEditText(
                title: "",
                isMobileNumber: true,
                isReadonly: true,
                controller: controller.phoneNumber,
              ),
              gapH20,
              Visibility(
                visible: controller.needEdit.value,
                child: AppButton(

                  text: "Update",
                  onPressed: () {},
                ),
              ),
              gapH20,
              AppButton(
                backgroundColor: AppColor.plantPrimaryColor,
                  text: "লগ আউট",
                  onPressed: () {
                    Get.find<GlobalState>().logoutSection();
                  })
            ],
          ),
        )),
      );
    });
  }

  Widget iconButton({
    required IconData icons,
    required Function() onTap,
    required Color iconColor,
    double? size = 20,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: IconButton(
          onPressed: onTap,
          icon: Icon(
            icons,
            size: size,
            color: iconColor,
          )),
    );
  }
}

import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/modules/plant/plant_home/plant_home_controller.dart';
import 'package:supply_safe_brolier_meat/app/modules/plant/plant_profile/plant_profile_controller.dart';
import 'package:supply_safe_brolier_meat/app/modules/plant/plant_request/plant_request_controller.dart';

import '../gallery/photo_gallery/photo_gallery_controller.dart';
import '../stock/plant_stocks/plant_stocks_controller.dart';
import 'plant_main_nav_controller.dart';

class PlantMainNavBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PlantMainNavController>(
      () => PlantMainNavController(),
    );
    Get.lazyPut<PlantHomeController>(
      () => PlantHomeController(),
    );
    Get.lazyPut<PlantStocksController>(
      () => PlantStocksController(),
    );
    Get.lazyPut<PhotoGalleryController>(
      () => PhotoGalleryController(),
    );
    Get.lazyPut<PlantRequestController>(
      () => PlantRequestController(),
    );
    Get.lazyPut<PlantProfileController>(
      () => PlantProfileController(),
    );
  }
}

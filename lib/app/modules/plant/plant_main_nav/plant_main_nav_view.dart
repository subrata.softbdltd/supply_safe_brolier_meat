import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_colors.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/asset_constants.dart';
import 'package:supply_safe_brolier_meat/app/modules/plant/gallery/photo_gallery/photo_gallery_controller.dart';
import 'package:supply_safe_brolier_meat/app/modules/plant/gallery/photo_gallery/photo_gallery_view.dart';
import 'package:supply_safe_brolier_meat/app/modules/plant/plant_home/plant_home_controller.dart';
import 'package:supply_safe_brolier_meat/app/modules/plant/plant_home/plant_home_view.dart';
import 'package:supply_safe_brolier_meat/app/modules/plant/plant_profile/plant_profile_controller.dart';
import 'package:supply_safe_brolier_meat/app/modules/plant/plant_profile/plant_profile_view.dart';
import 'package:supply_safe_brolier_meat/app/modules/plant/plant_request/plant_request_controller.dart';
import 'package:supply_safe_brolier_meat/app/modules/plant/plant_request/plant_request_view.dart';

import '../../../core/constants/app_constants.dart';
import '../../../core/widgets/app_widgets.dart';
import '../stock/plant_stocks/plant_stocks_controller.dart';
import '../stock/plant_stocks/plant_stocks_view.dart';
import 'plant_main_nav_controller.dart';

class PlantMainNavView extends GetView<PlantMainNavController> {
  const PlantMainNavView({super.key});
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return AppWidget().appExitConfirmation();
      },
      child: Scaffold(
        bottomNavigationBar: myNavBar(),
        body: Obx(() {
          return IndexedStack(
            index: controller.pageIndex.value,
            children: const [
              PlantStocksView(),
              PlantHomeView(),
              PhotoGalleryView(),
              PlantRequestView(),
              PlantProfileView(),
            ],
          );
        }),
      ),
    );
  }

  Widget myNavBar() {
    return Obx(() {
      return Container(
        height: 65.h,
        width: double.infinity,
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(40),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            navBarComponentDesign(0, homeImage, selectedHomeImage),
            navBarComponentDesign(1, farmIcon, farmIcon),
            navBarComponentDesign(2, photoIcon, photoIcon),
            navBarComponentDesign(3, packageIcon, packageIcon),
            navBarComponentDesign(4, userImage, selectedUserImage),
          ],
        ),
      );
    });
  }

  Widget navBarComponentDesign(
    int index,
    String assetNonSelected,
    String assetSelected,
  ) {
    double iconHeight = 30.w;
    bool isSelected = controller.pageIndex.value == index;
    return Container(
      height: 65.h,
      decoration: BoxDecoration(
        // color: controller.pageIndex.value == index
        //     ? MyColors.grey
        //     : Colors.transparent,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(
            index == 0 ? 0 : 10,
          ),
          topRight: Radius.circular(
            index == 3 ? 0 : 10,
          ),
        ),
      ),
      child: IconButton(
        splashRadius: 25.w,
        enableFeedback: false,
        onPressed: () {
          logger.i("PageLength: ${controller.pageIndex.value}");

          controller.pageIndex.value = index;
          switch (index) {
            case 0:
              {
                PlantStocksController().onInit();

              }
              break;
            case 1:
              {
                PlantHomeController().onInit();
              }
              break;
            case 2:
              {
                PhotoGalleryController().onInit();
              }
              break;
            case 3:
              {
                PlantRequestController().onInit();
              }
              break;
            case 4:
              {
                PlantProfileController().onInit();
              }
              break;
          }
        },
        icon: isSelected
            ? Image.asset(
                assetSelected,
                height: iconHeight,
                color: isSelected ? AppColor.plantPrimaryColor : Colors.white,
              )
            : Image.asset(
                assetNonSelected,
                height: iconHeight,
                color: Colors.grey,
              ),
      ),
    );
  }
}

import 'package:get/get.dart';

import '../../../core/state/global_state.dart';

class PlantMainNavController extends GetxController {
  final pageIndex = 0.obs, globalStateController = Get.find<GlobalState>();
  @override
  void onInit() {
    super.onInit();
  }
}

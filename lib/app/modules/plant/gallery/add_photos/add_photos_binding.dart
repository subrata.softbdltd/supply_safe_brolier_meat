import 'package:get/get.dart';

import 'add_photos_controller.dart';

class AddPhotosBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AddPhotosController>(
      () => AddPhotosController(),
    );
  }
}

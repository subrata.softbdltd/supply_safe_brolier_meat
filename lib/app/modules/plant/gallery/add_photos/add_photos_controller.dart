import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:supply_safe_brolier_meat/app/network/api_url.dart';
import 'package:dio/dio.dart' as dio;
import '../../../../core/constants/app_constants.dart';
import '../../../../data/pojo/Response_model.dart';
import '../../../../network/api_client.dart';

class AddPhotosController extends GetxController {
  final ScrollController scrollController = ScrollController();
  final multiImage = <String>[].obs;
  final ImagePicker picker = ImagePicker();

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }
  // Modified function to open camera or gallery dialog
  void getImageSourceOption() {
    Get.defaultDialog(
      titlePadding: mainPaddingAll,
      title: "Select Image Source",
      content: Column(
        children: [
          ListTile(
            leading: const Icon(Icons.camera_alt),
            title: const Text("Camera"),
            onTap: () {
              Get.back();
              getMultiImages(ImageSource.camera);
            },
          ),
          ListTile(
            leading: const Icon(Icons.photo_library),
            title: const Text("Gallery"),
            onTap: () {
              Get.back();
              getMultiImages(ImageSource.gallery);
            },
          ),
        ],
      ),
    );
  }

  // Updated function to support both camera and gallery
  Future<void> getMultiImages(ImageSource source) async {
    try {
      final List<XFile?> images = source == ImageSource.gallery
          ? await picker.pickMultiImage()
          : [await picker.pickImage(source: source)];

      if (images.isNotEmpty) {
        if (multiImage.length + images.length > 5) {
          appWidgets.showSimpleToast(
            "আপনি সর্বোচ্চ 5টি ছবি নির্বাচন করতে পারেন",
            isError: true,
            title: "সীমা ছাড়িয়ে গেছে",
          );
          return;
        }

        images.forEach((element) {
          if (element != null) {
            multiImage.add(element.path);
            logger.i("multiImagePath ${element.path}");
          }
        });
      } else {
        appWidgets.showSimpleToast(
          "No Image is Clicked or Selected",
          isError: true,
          title: "Image Not Found",
        );
      }
    } catch (e) {
      appWidgets.showSimpleToast(
        "Error picking image: $e",
        isError: true,
        title: "Image Error",
      );
      logger.e("Error picking image: $e");
    }
  }

  submitGalleryPhotos() async {
    var data = <String, dynamic>{};
    if (multiImage.isNotEmpty) {
      for (var i = 0; i < multiImage.length; i++) {
        data['images[$i]'] =
            dio.MultipartFile.fromFileSync(multiImage.value[i]);
      }
    }

    var response = await ApiClient().postAPI(
      galleryUrl,
      data,
      submitGalleryPhotos,
    );
    if (response != null) {
      ResponseModel responseModel = responseModelFromJson(response.toString());
      if (responseModel.responseStatus?.success ?? false) {
        Get.back(result: true);

        appWidgets.showSimpleToast(responseModel.responseStatus?.message,
            isSuccess: true);
      } else {
        appWidgets.showSimpleToast(
          responseModel.responseStatus?.error,
        );
      }
    }
  }
}

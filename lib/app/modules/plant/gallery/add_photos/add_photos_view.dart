import 'dart:io';

import 'package:dynamic_height_grid_view/dynamic_height_grid_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

import 'package:get/get.dart';
import 'package:soft_builder/constraints/my_constraints.dart' as my_const;
import 'package:supply_safe_brolier_meat/app/core/constants/app_constants.dart';

import '../../../../core/constants/app_colors.dart';
import '../../../../core/constants/my_text_style.dart';
import '../../../../core/widgets/app_defaults/app_app_bar.dart';
import 'add_photos_controller.dart';

class AddPhotosView extends GetView<AddPhotosController> {
  const AddPhotosView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundColor,
      appBar: AppAppBar(
        title: "ফটো যোগ করুন",
        titleColor: AppColor.white,
        titleFontSize: 20.sp,
        needWhiteBackButtonColor: true,
        needActionButtonList: true,
        backgroundColor: AppColor.plantPrimaryColor,
      ),
      floatingActionButton: Obx(() {
        return controller.multiImage.isEmpty
            ? Container()
            : Padding(
                padding: EdgeInsets.all(12.w),
                child: FloatingActionButton.extended(
                  backgroundColor: AppColor.plantPrimaryColor,
                  foregroundColor: AppColor.white,
                  onPressed: controller.submitGalleryPhotos,
                  label: const Text("ফটো জমা দিন"),
                ),
              );
      }),
      body: Padding(
        padding: my_const.mainPadding(20, 20),
        child: Column(
          children: [
            Obx(() {
              return Visibility(
                  visible: (controller.multiImage.length < 5),
                  child: Material(
                    child: InkWell(
                        onTap: () {
                          controller.getImageSourceOption();
                        },
                        child: Ink(
                          padding: my_const.mainPadding(12, 0),
                          child: Row(
                            children: [
                              const Icon(
                                Icons.image,
                              ),
                              gapW8,
                              Text(
                                "ছবি আপলোড করুন",
                                style: text16Style(),
                              ),
                            ],
                          ),
                        )),
                  ));
            }),
            gapH12,
            gapH12,
            Obx(() {
              return Visibility(
                visible: controller.multiImage.isNotEmpty,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "ছবি",
                      style: text18Style(),
                    ),
                    appWidgets.divider(color: AppColor.appBarTextColor),
                  ],
                ),
              );
            }),
            gapH12,
            Obx(() {
              if (controller.multiImage.isEmpty) {
                return Container();
              }

              return DynamicHeightGridView(
                controller: controller.scrollController,
                shrinkWrap: true,
                physics: const BouncingScrollPhysics(),
                crossAxisCount: 3,
                itemCount: controller.multiImage.length,
                builder: (context, index) {
                  final data = controller.multiImage[index];
                  return AnimationConfiguration.staggeredGrid(
                    position: index,
                    columnCount: 2,
                    duration: const Duration(milliseconds: 375),
                    child: ScaleAnimation(
                      delay: const Duration(milliseconds: 250),
                      child: FadeInAnimation(
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12)),
                          child: Stack(
                            children: [
                              Image.file(
                                File(data),
                                fit: BoxFit.cover,
                              ),
                              Positioned(
                                top: 5,
                                right: 5,
                                child: Material(
                                  child: InkWell(
                                    onTap: () {
                                      controller.multiImage.removeAt(index);
                                    },
                                    child: Ink(
                                      decoration: const BoxDecoration(
                                        color: AppColor.white,
                                      ),
                                      child: const Icon(
                                        size: 18,
                                        Icons.close_rounded,
                                        color: AppColor.primaryColor,
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
              );
            }),
          ],
        ),
      ),
    );
  }
}

import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_constants.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/plant/photo_gallery_model.dart';
import 'package:supply_safe_brolier_meat/app/network/api_url.dart';

class PhotoGalleryController extends GetxController {
  var count = 0.obs;
  final photoDataList = <Data>[].obs;

  @override
  void onInit() {
    logger.i("PhotoView");
    getPhotoGallery();
    super.onInit();
  }

  getPhotoGallery() async {
    var response = await apiClient.getAPI(
      galleryUrl,
      getPhotoGallery,
    );
    if (response != null) {
      PhotoGalleryModel photoGalleryModel =
          photoGalleryModelFromJson(response.toString());
      if (photoGalleryModel.responseStatus?.success ?? false) {
        photoDataList.value = photoGalleryModel.data ?? [];
      } else {
        appWidgets.showSimpleToast("No data found");
      }
    }
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/core/widgets/app_network_image.dart';
import 'package:supply_safe_brolier_meat/app/routes/app_pages.dart';

import '../../../../core/constants/app_colors.dart';
import '../../../../core/constants/app_constants.dart';
import '../../../../core/constants/asset_constants.dart';
import '../../../../core/utils/helper/app_helper.dart';
import '../../../../core/widgets/app_defaults/app_app_bar.dart';
import 'photo_gallery_controller.dart';

class PhotoGalleryView extends GetView<PhotoGalleryController> {
  const PhotoGalleryView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundColor,
      appBar: AppAppBar(
        title: "গ্যালারি",
        titleColor: AppColor.white,
        titleFontSize: 20.sp,
        needWhiteBackButtonColor: true,
        needActionButtonList: true,
        backgroundColor: AppColor.plantPrimaryColor,
      ),
      floatingActionButton: Padding(
        padding: EdgeInsets.all(12.w),
        child: FloatingActionButton.extended(
          backgroundColor: AppColor.plantPrimaryColor,
          foregroundColor: AppColor.white,
          onPressed: () {
            Get.toNamed(Routes.ADD_PHOTOS)?.then((value) {
              if (value == true) {
                controller.getPhotoGallery();
              }
            });
          },
          label: const Text("ছবি যোগ করুন"),
        ),
      ),
      body: Obx(() {
        logger.i("CountData: ${controller.count}");
        return Padding(
          padding: const EdgeInsets.all(4.0),
          child: controller.photoDataList.isNotEmpty
              ? GridView.custom(
                  gridDelegate: SliverQuiltedGridDelegate(
                    crossAxisCount: 4, // Number of total columns
                    mainAxisSpacing: 4, // Space between rows
                    crossAxisSpacing: 4, // Space between columns
                    repeatPattern: QuiltedGridRepeatPattern.inverted,
                    pattern: [
                      const QuiltedGridTile(2, 2),
                      const QuiltedGridTile(1, 1),
                      const QuiltedGridTile(1, 1),
                      const QuiltedGridTile(1, 2),
                    ],
                  ),
                  childrenDelegate: SliverChildBuilderDelegate(
                    (context, index) {
                      return imageTile(
                          imageUrl: appHelper.validateImageURL(
                              controller.photoDataList[index].value ?? ""),
                          onTap: () {
                            Get.toNamed(Routes.PHOTO_VIEW, arguments: {
                              "id": controller.photoDataList[index].id,
                              "imageUrl": controller.photoDataList[index].value,
                            })?.then((value) {
                              if (value == true) {
                                controller.getPhotoGallery();
                              }
                            });
                          });
                    },
                    childCount: controller
                        .photoDataList.length, // Number of items in the grid
                  ),
                )
              : appWidgets.noDataFoundMsg(),
        );
      }),
    );
  }

  Widget imageTile({
    String? imageUrl,
    VoidCallback? onTap,
  }) {
    return InkWell(
      onTap: onTap,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Ink(
          child: CachedNetworkImage(
            imageUrl: AppHelper().validateImageURL(imageUrl ?? ""),
            fit: BoxFit.cover,
            progressIndicatorBuilder: (
              context,
              child,
              loadingProgress,
            ) {
              return const Center(child: CircularProgressIndicator.adaptive());
            },
            errorWidget: (
              BuildContext context,
              String url,
              dynamic error,
            ) {
              return SizedBox(
                height: 100.w, // Adjust the height as per your requirement
                width: 100.w, // Adjust the width as per your requirement
                child: Image.asset(
                  noImage ?? "", // Provide a default image if specified
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

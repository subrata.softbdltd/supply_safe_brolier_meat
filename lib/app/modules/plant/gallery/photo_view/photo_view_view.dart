import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import '../../../../core/constants/asset_constants.dart';
import '../../../../core/utils/helper/app_helper.dart';
import 'photo_view_controller.dart';

class PhotoViewView extends GetView<PhotoViewController> {
  const PhotoViewView({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        iconTheme: const IconThemeData(color: Colors.white),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.transparent,
        onPressed: () {
          controller.photoDelete();
        },
        child: Icon(
          Icons.delete,
          color: Colors.red,
          size: 25.w,
        ),
      ),
      body: Center(
        child: Hero(
          tag: controller.imageUrl, // Optional: Hero animation tag
          child: CachedNetworkImage(
            imageUrl: AppHelper().validateImageURL("${controller.imageUrl}"),
            fit: BoxFit.cover,
            progressIndicatorBuilder: (
              context,
              child,
              loadingProgress,
            ) {
              return const Center(child: CircularProgressIndicator.adaptive());
            },
            errorWidget: (
              BuildContext context,
              String url,
              dynamic error,
            ) {
              return SizedBox(
                height: 100.w, // Adjust the height as per your requirement
                width: 100.w, // Adjust the width as per your requirement
                child: Image.asset(
                  noImage ?? "", // Provide a default image if specified
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

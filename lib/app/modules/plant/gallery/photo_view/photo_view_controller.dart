import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/Response_model.dart';

import '../../../../core/constants/app_constants.dart';
import '../../../../network/api_url.dart';

class PhotoViewController extends GetxController {
  final imageUrl = "".obs;
  final imageID = 0.obs;

  final count = 0.obs;
  @override
  void onInit() {
    if (Get.arguments != null && Get.arguments["imageUrl"] != "") {
      imageUrl.value = Get.arguments["imageUrl"];
      imageID.value = Get.arguments["id"];
    }
    super.onInit();
  }

  photoDelete() async {
    var response = await apiClient.deleteAPI(
      "$galleryUrl/$imageID",
      photoDelete,
    );
    if (response != null) {
      ResponseModel responseModel = responseModelFromJson(response.toString());
      if (responseModel.responseStatus?.success ?? false) {
        Get.back(result: true);
        appWidgets.showSimpleToast(responseModel.responseStatus?.message,
            isSuccess: true, isShort: true);
      }else{
        appWidgets.showSimpleToast(responseModel.responseStatus?.message,
            );
      }
    }
  }
}

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_constants.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/plant/plant_stock_model.dart';
import 'package:supply_safe_brolier_meat/app/routes/app_pages.dart';

import '../../../../network/api_url.dart';

class PlantStocksController extends GetxController {
  final ScrollController scrollController = ScrollController();
  final stockData = <Data>[].obs;
  @override
  void onInit() {
    super.onInit();
    getPlantStock();
    requestNotificationPermission();
  }

  getPlantStock() async {
    var response = await apiClient.getAPI(packageStockUrl, getPlantStock);
    if (response != null) {
      PlantStockModel plantStockModel =
          plantStockModelFromJson(response.toString());
      if (plantStockModel.responseStatus?.success ?? false) {
        stockData.value = plantStockModel.data ?? [];
      } else {
        appWidgets.showSimpleToast(plantStockModel.responseStatus?.message);
      }
    }
  }
  requestNotificationPermission() async {
    FirebaseMessaging messaging = FirebaseMessaging.instance;
    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );
  }

  onGoNext(data) {
    Get.toNamed(
      Routes.PLANT_STOCK_DETAILS,
      arguments: data,
    )?.then((value) {
      getPlantStock();
    });
  }
}

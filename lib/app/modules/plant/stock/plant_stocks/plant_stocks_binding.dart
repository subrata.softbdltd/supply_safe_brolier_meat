import 'package:get/get.dart';

import 'plant_stocks_controller.dart';

class PlantStocksBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PlantStocksController>(
      () => PlantStocksController(),
    );
  }
}

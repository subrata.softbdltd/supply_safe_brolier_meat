import 'package:dynamic_height_grid_view/dynamic_height_grid_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/my_text_style.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/plant/plant_stock_model.dart';
import 'package:get/get.dart';

import '../../../../core/constants/app_colors.dart';
import '../../../../core/constants/app_constants.dart';
import '../../../../core/constants/asset_constants.dart';
import '../../../../core/widgets/app_defaults/app_app_bar.dart';
import '../../../../core/widgets/app_network_image.dart';
import 'plant_stocks_controller.dart';

class PlantStocksView extends GetView<PlantStocksController> {
  const PlantStocksView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundColor,
      appBar: AppAppBar(
        title: "স্টক",
        titleColor: AppColor.white,
        titleFontSize: 20.sp,
        needWhiteBackButtonColor: true,
        backgroundColor: AppColor.plantPrimaryColor,
      ),
      body: Padding(
          padding: mainPaddingAll,
          child: Column(
            children: [
              Obx(() {
                return Expanded(
                    child: AnimationLimiter(
                  child: DynamicHeightGridView(
                    controller: controller.scrollController,
                    shrinkWrap: true,
                    physics: const BouncingScrollPhysics(),
                    crossAxisCount: 2,
                    itemCount: controller.stockData.length,
                    builder: (context, index) {
                      final data = controller.stockData[index];
                      return AnimationConfiguration.staggeredGrid(
                        position: index,
                        columnCount: 2,
                        duration: const Duration(milliseconds: 375),
                        child: ScaleAnimation(
                          delay: const Duration(milliseconds: 250),
                          child: FadeInAnimation(
                            child: cardItems(context, data, () {
                              controller.onGoNext(data);
                            }),
                          ),
                        ),
                      );
                    },
                  ),
                ));
              }),
            ],
          )),
    );
  }

  Widget cardItems(BuildContext context, Data data, Function() onTap) {
    return Material(
      borderRadius: BorderRadius.circular(12),
      child: Ink(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
        ),
        child: InkWell(
          borderRadius: BorderRadius.circular(12),
          onTap: onTap,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              gapH12,
              AppNetworkImage(
                imageUrl: data.thumbnail ?? "",
                noImage: noDataFound,
                width: double.infinity,
                assetImageColor: Colors.grey,
                assetWidth: 80.0,
              ),
              gapH3,
              Padding(
                padding: EdgeInsets.only(left: 10.w, bottom: 10.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Center(
                      child: Text(
                        data.name ?? "",
                        textAlign: TextAlign.center,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: text16Style(
                          isWeight600: true,
                        ),
                      ),
                    ),
                    gapH12,
                    Text(
                      "স্টক: ${data.stocks?.count ?? 0} প্যাকেজ",
                      style: text12Style(),
                    ),
                    Text(
                      "ওজন: ${data.weight ?? 0} ${data.weightUnit}",
                      style: text12Style(),
                    ),
                    Text(
                      "মূল্য: ${data.price ?? 0}৳",
                      style: text12Style(),
                    ),
                    Text(
                      "ছাড়: ${data.discount ?? 0}৳",
                      style: text12Style(),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

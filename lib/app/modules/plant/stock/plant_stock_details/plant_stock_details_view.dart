import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:soft_builder/soft_builder.dart';
import 'package:supply_safe_brolier_meat/app/core/widgets/app_button.dart';
import '../../../../core/constants/app_colors.dart';
import '../../../../core/constants/app_constants.dart';
import '../../../../core/constants/asset_constants.dart';
import '../../../../core/widgets/app_defaults/app_app_bar.dart';
import '../../../../core/widgets/app_edit_text.dart';
import '../../../../core/widgets/app_network_image.dart';
import 'plant_stock_details_controller.dart';

class PlantStockDetailsView extends GetView<PlantStockDetailsController> {
  const PlantStockDetailsView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundColor,
      appBar: AppAppBar(
        title: controller.selectedData.value.name ?? "",
        titleColor: AppColor.white,
        titleFontSize: 20.sp,
        needWhiteBackButtonColor: true,
        backgroundColor: AppColor.plantPrimaryColor,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: mainPaddingAll,
          child: Obx(() {
            return Column(
              children: [
                gapH3,
                AppNetworkImage(
                  imageUrl: controller.selectedData.value.thumbnail ?? "",
                  noImage: noDataFound,
                  height: 300,
                  width: double.infinity,
                  assetImageColor: Colors.grey,
                  assetWidth: 80.0,
                ),
                gapH12,
                Form(
                  key: controller.formKey,
                  child: MyEditText(
                    title: "আপনার প্যাকেজের পরিমাণ লিখুন",
                    controller: controller.countController,
                    isRequired: true,
                    isNumberKeyboard: true,

                  ),
                ),
                gapH20,
                Row(
                  children: [
                    Expanded(
                        child: AppButton(
                          backgroundColor: AppColor.plantPrimaryColor,
                          text: 'যোগ করুন',
                          onPressed: () {
                            controller.onSubmit("add");
                          },
                        )),
                    gapW8,
                    Expanded(
                        child: Obx(() {
                          return AppButton(

                            text: 'অপসারণ',
                            onPressed: controller.selectedData.value.stocks
                                ?.count != 0
                                ? () {
                              controller.onSubmit("remove");
                            }
                                : null,
                            textColor: AppColor.white,
                            backgroundColor: Colors.red,
                          );
                        })),
                  ],
                )
              ],
            );
          }),
        ),
      ),
    );
  }
}

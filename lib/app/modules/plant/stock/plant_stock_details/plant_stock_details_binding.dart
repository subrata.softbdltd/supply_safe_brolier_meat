import 'package:get/get.dart';

import 'plant_stock_details_controller.dart';

class PlantStockDetailsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PlantStockDetailsController>(
      () => PlantStockDetailsController(),
    );
  }
}

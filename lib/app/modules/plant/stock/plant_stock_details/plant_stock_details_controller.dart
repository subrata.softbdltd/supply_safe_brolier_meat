import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_constants.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/Response_model.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/plant/plant_stock_model.dart';

import '../../../../network/api_url.dart';

class PlantStockDetailsController extends GetxController {
  TextEditingController countController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  final selectedData = Data().obs;

  @override
  void onInit() {
    super.onInit();
    if (Get.arguments != null) {
      selectedData.value = Get.arguments;
    }
  }

  onSubmit(type) {
    if (formKey.currentState?.validate() ?? false) {
      stockAddDelete(type);
    }
  }

  stockAddDelete(
    type,
  ) async {
    var data = <String, dynamic>{};
    data["package_id"] = selectedData.value.id;
    data["type"] = type;
    data["quantity"] = countController.text;

    var response = await apiClient.postAPI(
      stockUpdateDelete,
      data,
      stockAddDelete,
      isApplicationJson: true
    );
    if (response != null) {
      ResponseModel responseModel = responseModelFromJson(response.toString());
      if (responseModel.responseStatus?.success ?? false) {
        Get.back();
        appWidgets.showSimpleToast(
          responseModel.responseStatus?.message,
          isSuccess: true,
        );
      } else {
        appWidgets.showSimpleToast(
          responseModel.responseStatus?.error,
        );
      }
    }
  }
}

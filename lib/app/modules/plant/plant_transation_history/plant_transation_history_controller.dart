import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_constants.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/plant/plant_transation_history.dart';
import 'package:supply_safe_brolier_meat/app/network/api_url.dart';

class PlantTransationHistoryController extends GetxController {
  final historyData = <Data>[].obs;
  final isLoading = false.obs;
  @override
  void onInit() {
    super.onInit();
    getTransationHistory();
  }

  getTransationHistory() async {
    isLoading.value = true;
    var response = await apiClient.getAPI(
        plantTransactionsSummaryUrl, getTransationHistory);
    if (response != null) {
      PlantTransationHistory plantTransationHistory =
          plantTransationHistoryFromJson(response.toString());
      if (plantTransationHistory.responseStatus?.success ?? false) {
        historyData.value = plantTransationHistory.data ?? [];
        isLoading.value = false;
      } else {
        isLoading.value = false;
        Get.back();
        appWidgets
            .showSimpleToast(plantTransationHistory.responseStatus?.error);
      }
    }
  }
}

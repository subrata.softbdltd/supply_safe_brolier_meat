import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/plant/plant_transation_history.dart';
import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/my_text_style.dart';

import '../../../core/constants/app_colors.dart';
import '../../../core/constants/app_constants.dart';
import '../../../core/widgets/app_defaults/app_app_bar.dart';
import 'plant_transation_history_controller.dart';

class PlantTransationHistoryView
    extends GetView<PlantTransationHistoryController> {
  const PlantTransationHistoryView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundColor,
      appBar: AppAppBar(
        title: "ইতিহাস",
        titleColor: AppColor.white,
        titleFontSize: 20.sp,
        needWhiteBackButtonColor: true,
        backgroundColor: AppColor.plantPrimaryColor,
      ),
      body: Padding(
        padding: mainPaddingAll,
        child: Column(
          children: [
            Expanded(
              child: Obx(() {
                if (!controller.isLoading.value) {
                  return controller.historyData.isEmpty
                      ? Center(
                          child: Text(
                            "কোন ইতিহাস পাওয়া যায়নি",
                            style:
                                text20Style(color: AppColor.red, fontSize: 30),
                          ),
                        )
                      : ListView.separated(
                          itemCount: controller.historyData.length,
                          physics: const BouncingScrollPhysics(),
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            final data = controller.historyData[index];
                            return transactionItem(
                                serialNumber: "${index + 1} ", data: data);
                          },
                          separatorBuilder: (context, index) {
                            return gapH16;
                          },
                        );
                } else {
                  return appWidgets.circularProgressBar();
                }
              }),
            ),
          ],
        ),
      ),
    );
  }

  Widget transactionItem({required String serialNumber, required Data data}) {
    return Container(
      padding: defaultPadding,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 2,
            blurRadius: 5,
            offset: const Offset(0, 3),
          ),
        ],
      ),
      child: Row(
        children: [
          CircleAvatar(
            radius: 25.w,
            backgroundColor: AppColor.plantPrimaryColor,
            child: Text(
              serialNumber,
              style: text20Style(color: AppColor.white),
            ),
          ),
          gapW12,
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  data.farmer?.name ?? "",
                  style: text20Style(isWeight500: true),
                ),
                gapH3,
                Text(
                  "ফোন নম্বর: ${data.farmer?.phone ?? ""}",
                  style: text14Style(),
                ),
                Text(
                  "মোট ব্রয়লার: ${data.totalBroiler ?? 0}",
                  style: text14Style(),
                ),
                Text(
                  "সম্পূর্ণ ওজন: ${data.totalWeight ?? 0}",
                  style: text14Style(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

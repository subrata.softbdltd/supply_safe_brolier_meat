import 'package:get/get.dart';

import 'plant_transation_history_controller.dart';

class PlantTransationHistoryBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PlantTransationHistoryController>(
      () => PlantTransationHistoryController(),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/Response_model.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/broiler_list_model.dart'
    as broiler_list;
import 'package:supply_safe_brolier_meat/app/data/pojo/user_details_model.dart'
    as user_details;
import 'package:supply_safe_brolier_meat/app/network/api_url.dart';
import 'package:supply_safe_brolier_meat/app/routes/app_pages.dart';

import '../../../core/constants/app_constants.dart';
import '../../../core/state/global_state.dart';
import '../../../network/api_client.dart';

class PlantHomeController extends GetxController {
  TextEditingController weightController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  final selectedDate = DateTime.now().obs;
  final storeDate = "".obs;
  final globalState = Get.find<GlobalState>();
  final broilerListData = <broiler_list.Data>[].obs;
  final isLoading = false.obs;
  final userDetailsData = user_details.Data().obs;
  final isLoggedIn = false.obs;
  @override
  void onInit() {
    super.onInit();
    getBroilerListData();
    getUserData();
  }

  getBroilerListData() async {
    isLoading.value = true;
    var response = await ApiClient().getAPI(broilerListUrl, getBroilerListData);
    if (response != null) {
      print("Hello");
      broiler_list.BroilerListModel broilerListModel =
          broiler_list.broilerListModelFromJson(response.toString());

      if (broilerListModel.responseStatus?.success ?? false) {
        broilerListData.value = broilerListModel.data ?? [];
        isLoading.value = false;
      } else {
        print("Hello");
        appWidgets.showSimpleToast(broilerListModel.responseStatus?.error,
            isError: true);
        isLoading.value = false;
        Get.offAllNamed(Routes.LOGIN_SCREEN);
      }
    }
  }

  getUserData() async {
    await appHelper.getToken().then((token) async {
      if (token != null && token != "") {
        isLoggedIn.value = true;
        var response = await ApiClient().getAPI(getUserDetailsUrl, getUserData);
        if (response != null) {
          user_details.UserDetailsModel userDetailsModel =
              user_details.userDetailsModelFromJson(response.toString());
          if (userDetailsModel.responseStatus?.success ?? false) {
            userDetailsData.value =
                userDetailsModel.data ?? user_details.Data();
           await getBroilerListData();
          } else {
            appWidgets.showSimpleToast(userDetailsModel.responseStatus?.message,
                isError: true);
          }
        }
      } else {
        isLoggedIn.value = false;
      }
    });
  }

  Future<void> selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: selectedDate.value,
      firstDate: DateTime(1900),
      lastDate: DateTime(2101),
    );
    if (picked != null && picked != selectedDate.value) {
      logger.i("Selected Date: $picked");
      selectedDate.value = picked;

      storeDate.value = DateFormat('yyyy-MM-dd').format(selectedDate.value);
    }
  }

  onSubmit(selectedBatch, selectedFarmerID) {
    if (formKey.currentState?.validate() ?? false) {
      onGoTransactions(selectedBatch, selectedFarmerID);
    }
  }

  onGoTransactions(selectedBatch, selectedFarmerID) async {
    var data = <String, dynamic>{};
    data["weight"] = weightController.text;
    data["batch_id"] = selectedBatch;
    data["farmer_id"] = selectedFarmerID;
    if (storeDate.value != "") {
      data["receive_date"] = storeDate.value;
    } else {
      data["receive_date"] = DateFormat('yyyy-MM-dd').format(DateTime.now());
    }

    var response =
        await ApiClient().postAPI(plantTransactionsUrl, data, onGoTransactions);
    if (response != null) {
      ResponseModel responseModel = responseModelFromJson(response.toString());
      if (responseModel.responseStatus?.success ?? false) {
        getBroilerListData();
        Get.back();
        weightController.clear();
        appWidgets.showSimpleToast(
          responseModel.responseStatus?.message,
          isSuccess: true,
        );
      } else {
        appWidgets.showSimpleToast(
          responseModel.responseStatus?.error,
        );
      }
    }
  }
}

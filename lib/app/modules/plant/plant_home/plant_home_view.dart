import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/asset_constants.dart';
import 'package:supply_safe_brolier_meat/app/core/widgets/app_button.dart';
import 'package:supply_safe_brolier_meat/app/core/widgets/app_edit_text.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/broiler_list_model.dart';
import 'package:supply_safe_brolier_meat/app/routes/app_pages.dart';

import '../../../core/constants/app_colors.dart';
import '../../../core/constants/app_constants.dart';
import '../../../core/constants/my_text_style.dart';
import '../../../core/widgets/app_defaults/app_app_bar.dart';
import 'plant_home_controller.dart';

class PlantHomeView extends GetView<PlantHomeController> {
  const PlantHomeView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundColor,
      appBar: AppAppBar(
        title: "কৃষক বয়লার",
        titleColor: AppColor.white,
        titleFontSize: 20.sp,
        needWhiteBackButtonColor: true,
        needActionButtonList: true,
        actionImage: transHisImage,
        needActionButton: true,
        backgroundColor: AppColor.plantPrimaryColor,
        actionOnPressed: () {
          Get.toNamed(Routes.PLANT_TRANSATION_HISTORY);
        },
      ),
      body: Padding(
        padding: mainPaddingAll,
        child: Column(
          children: [
            Expanded(
              child: Obx(() {
                if (!controller.isLoading.value) {
                  return controller.broilerListData.isEmpty
                      ? appWidgets.noData()
                      : ListView.separated(
                          itemCount: controller.broilerListData.length,
                          shrinkWrap: true,
                          physics: const BouncingScrollPhysics(),
                          itemBuilder: (context, index) {
                            final data = controller.broilerListData[index];
                            return boilerCardWidget(
                              onTap: () {
                                showBottomSheet(context, data);
                              },
                              farmerName: data.farmer?.fatherName ?? "",
                              batchNo: data.batchNo ?? "",
                              totalChicken: appHelper
                                  .replaceNumber("${data.totalBroiler ?? 0}"),
                            );
                          },
                          separatorBuilder: (context, index) {
                            return gapH16;
                          },
                        );
                } else {
                  return appWidgets.circularProgressBar();
                }
              }),
            ),
          ],
        ),
      ),
    );
  }

  Widget boilerCardWidget({
    required String farmerName,
    required String batchNo,
    required String totalChicken,
    required Function() onTap,
  }) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: defaultRadius,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.4),
            spreadRadius: 0.5,
            blurRadius: 1,
            offset: const Offset(0, 0.5), // changes position of shadow
          ),
        ],
      ),
      child: Material(
        borderRadius: defaultRadius,
        child: InkWell(
          borderRadius: defaultRadius,
          onTap: onTap,
          child: Ink(
            decoration: BoxDecoration(
              color: const Color(0xffAFD198).withOpacity(0.5),
              borderRadius: defaultRadius,
            ),
            padding: EdgeInsets.all(20.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "কৃষকের নাম: $farmerName",
                  style: text20Style(fontWeight: FontWeight.w600),
                ),
                Text(
                  "ব্যাচ নাম্বার: $batchNo",
                  style: text14Style(),
                ),
                Text(
                  "টোটাল চিকেন: $totalChicken",
                  style: text14Style(),
                ),
                appWidgets.divider(
                  color: AppColor.gray.withOpacity(0.3),
                ),
                gapH12,
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget chickenCountWidget({
    required String count,
    required String imageName,
    required Color borderColor,
    required Color imageColor,
  }) {
    return Expanded(
      child: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(8.w),
        decoration: BoxDecoration(
          borderRadius: defaultBorder,
          border: Border.all(color: borderColor),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              imageName,
              height: 20,
              color: imageColor,
            ),
            gapW3,
            Text(
              count,
              style: text14Style(fontWeight: FontWeight.w500),
            )
          ],
        ),
      ),
    );
  }

  void showBottomSheet(BuildContext context, Data data) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Obx(() {
          return Container(
            height: Get.height * 0.55,
            color: AppColor.backgroundColor,
            child: Padding(
              padding: mainPaddingAll,
              child: Form(
                key: controller.formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    AppEditText(
                      title: "ওজন",
                      controller: controller.weightController,
                      isRequired: true,
                    ),
                    gapH12,
                    Text(
                      'নির্বাচিত তারিখ:',
                      style: text14Style(fontSize: 20.0),
                    ),
                    gapH12,
                    Text(
                      '${controller.selectedDate.value.year}-${controller.selectedDate.value.month}-${controller.selectedDate.value.day}',
                      style: text20Style(),
                    ),
                    gapH12,
                    ActionChip(
                      backgroundColor: AppColor.plantPrimaryColor,
                      label: Text(
                        "তারিখ নির্বাচন করুন",
                        style: text16Style(color: AppColor.white),
                      ),
                      onPressed: () {
                        controller.selectDate(context);
                      },
                    ),
                    gapH20,
                    Padding(
                      padding: mainPaddingAll,
                      child: AppButton(
                        text: "দাখিল করা",
                        onPressed: () {
                          controller.onSubmit(data.id, data.farmerId);
                        },
                        backgroundColor: AppColor.plantPrimaryColor,
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        });
      },
    );
  }
}

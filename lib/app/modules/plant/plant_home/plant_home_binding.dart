import 'package:get/get.dart';

import 'plant_home_controller.dart';

class PlantHomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PlantHomeController>(
      () => PlantHomeController(),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/core/utils/helper/app_helper.dart';

import '../../core/constants/app_constants.dart';
import '../../routes/app_pages.dart';

class OnBoardScreenController extends GetxController {
  PageController? pageController = PageController();

  final currentPage = 0.obs;
  final onBoarding = false.obs;

  @override
  void onInit() {
    super.onInit();
    SystemChannels.textInput.invokeMethod('TextInput.hide'); // Hide Keyboard
  }

  onNextGo() async {
    logger.i("Hello");
    await AppHelper().saveBoolPref(onBoard, true);
    logger.i("Hello");
    Get.offAndToNamed(Routes.LOGIN_SCREEN);
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';
import 'package:lottie/lottie.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_constants.dart';
import 'package:supply_safe_brolier_meat/app/modules/on_board_screen/on_board_screen_controller.dart';

import '../../core/constants/app_colors.dart';
import '../../core/constants/asset_constants.dart';
import '../../core/constants/my_text_style.dart';
import '../../core/widgets/app_scroll_behavior.dart';


class OnBoardScreenView extends GetView<OnBoardScreenController> {
  const OnBoardScreenView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: ScrollConfiguration(
            behavior: AppBehavior(),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Obx(() {
                return Stack(
                  children: [
                    PageView(
                      scrollDirection: Axis.horizontal,
                      controller: controller.pageController,
                      children: <Widget>[
                        item(
                          "Looking for a Chicken?",
                          "Find the right Chicks for your ",
                          imageOne,
                        ),
                        item(
                          "Looking for a Chicken?",
                          "Find the right Chicks for your ",
                          imageTwo,
                        ),
                        item(
                          "Looking for a Chicken?",
                          "Find the right Chicks for your ",
                          imageOne,
                        ),
                      ],
                    ),
                    Positioned(
                      bottom: 0,
                      width: MediaQuery.of(context).size.width,
                      child: Container(
                        padding:
                        const EdgeInsets.only(right: 30, left: 10, bottom: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 28, top: 10, bottom: 10),
                              child: SmoothPageIndicator(
                                controller: controller.pageController!,
                                count: 3,
                                effect: const ExpandingDotsEffect(
                                    activeDotColor: AppColor.primaryColor,
                                    dotColor: Color(0XFFababab),
                                    dotHeight: 6.8,
                                    dotWidth: 8,
                                    spacing: 4.8),
                              ),
                            ),
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: AppColor.plantPrimaryColor,
                                //onPrimary: Colors.black,
                              ),
                              onPressed: controller.onNextGo,
                              child: Text(
                                controller.currentPage.value < 2 ? "Next" : "Start",
                                style: text14Style(color: AppColor.white),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    // controller.currentPage.value == 2
                    //     ? Positioned(
                    //         right: 10,
                    //         child: TextButton(
                    //             onPressed: () {
                    //             // setFirstTimeOpen();
                    //             Get.toNamed(
                    //               Routes.WELCOME_SCREEN,
                    //             );
                    //           },
                    //           child: const Text(
                    //             "Skip",
                    //             style: TextStyle(color: AppColor.black),
                    //           )),
                    //     )
                    //     : SizedBox()
                  ],
                );
              }),
            ),
          ),
        ));
  }
  item(title, subTitle, imagePath) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Lottie.asset(imagePath,
            height: 300.w,

            fit: BoxFit.fill
        ),
      gapH24,
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 15,
          ),
          child: Text(
            title,
            textAlign: TextAlign.center,
            style: text20Style(
              fontSize: 24,
            ),
          ),
        ),
        gapH8,
        Text(
          subTitle,
          textAlign: TextAlign.center,
          style: text12Style(color: AppColor.grey),
        ),
      ],
    );
  }
}


// OnboardScreen(
// imageUrl: imageOne,
// // title: 'Welcome to MyApp',
// description: 'সুস্থ ব্রয়লার, দ্রুত ও নিরাপদ ডেলিভারি',
// onNextPressed: () {
// controller.pageController.nextPage(
// duration: Duration(milliseconds: 500),
// curve: Curves.ease);
// },
// ),
// OnboardScreen(
// imageUrl: imageTwo,
// // title: 'Discover New Features',
// description:
// 'সুস্থ পশু, সুস্থ আয়, ব্রয়লার ফার্মিং এগিয়ে যায়',
// onNextPressed: () {
// controller.pageController.nextPage(
// duration: const Duration(milliseconds: 500),
// curve: Curves.ease);
// },
// ),
// OnboardScreen(
// imageUrl: imageThree,
// // title: 'Get Started Now',
// description: 'পুরোপুরি সুস্থ, স্বাদে সার্থক ব্রয়লার.',
// onNextPressed: controller.onNextGo,
// ),
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:lottie/lottie.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_colors.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/asset_constants.dart';
import 'package:supply_safe_brolier_meat/app/core/widgets/app_button.dart';

import '../../../core/constants/app_constants.dart';
import '../../../core/constants/my_text_style.dart';
import 'otp_screen_controller.dart';

class OtpScreenView extends GetView<OtpScreenController> {
  const OtpScreenView({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.splashBackgroundColor,
      appBar: AppBar(
        backgroundColor: AppColor.plantPrimaryColor,

        title: const Text("ওটিপি যাচাই করুন"),
        // needBackButton: true,
      ),
      body: Padding(
        padding: mainPaddingAll,
        child: Obx(() {
          return SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                gapH16,
                SizedBox(
                    height: 200.h,
                    width: double.infinity,
                    child: Lottie.asset(otpImage)),
                Form(
                  key: controller.formKey,
                  child: Padding(
                    padding: mainPaddingAll,
                    child: PinCodeTextField(
                      appContext: context,
                      pastedTextStyle: TextStyle(
                        color: Colors.green.shade600,
                        fontWeight: FontWeight.bold,
                      ),
                      length: 6,
                      animationType: AnimationType.fade,
                      validator: (v) {
                        if (v!.length < 5) {
                          return "";
                        } else {
                          return null;
                        }
                      },
                      pinTheme: PinTheme(
                        activeColor: AppColor.grey,
                        selectedColor: AppColor.black,
                        inactiveColor: AppColor.primaryColor,
                        disabledColor: AppColor.primaryColor,
                        activeFillColor: AppColor.primaryColor,
                        selectedFillColor: AppColor.primaryColor,
                        inactiveFillColor: AppColor.primaryColor,
                        errorBorderColor: AppColor.red,
                        shape: PinCodeFieldShape.box,
                        borderRadius: BorderRadius.circular(12),
                        borderWidth: 1,
                        activeBorderWidth: 2,
                        selectedBorderWidth: 1,
                        inactiveBorderWidth: 1,
                        disabledBorderWidth: 1,
                        errorBorderWidth: 1,
                        fieldHeight: 45,
                        fieldWidth: 40,
                      ),
                      cursorColor: Colors.black,
                      animationDuration: const Duration(milliseconds: 300),
                      // enableActiveFill: true,
                      errorAnimationController: controller.errorController,
                      controller: controller.otpController,
                      keyboardType: TextInputType.number,
                      boxShadows: [
                        BoxShadow(
                          color: Colors.grey.shade600,
                          blurRadius: 10,
                          spreadRadius: 10,
                        )
                      ],
                      onCompleted: (v) {},
                      onChanged: (value) {
                        controller.otpOnChanged(value);
                      },
                      beforeTextPaste: (text) {
                        debugPrint("Allowing to paste $text");
                        return true;
                      },
                    ),
                  ),
                ),
                Visibility(
                  visible: (controller.disabled.value),
                  child: Center(
                    child: Text(
                      'পুনরায় পাঠান ${controller.timerValue ~/ 60}:${controller.timerValue % 60}',
                      style: text20Style(color: AppColor.primaryColor),
                    ),
                  ),
                ),
                Visibility(
                  visible: !(controller.disabled.value),
                  child: Center(
                    child: RichText(
                        text: TextSpan(
                            text: "এসএমএস পাইনি ? ",
                            style: text14Style(
                              color: AppColor.black,
                            ),
                            children: [
                          TextSpan(
                            text: " কোড আবার পাঠান",
                            style: text14Style(
                              color: AppColor.primaryColor,
                            ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                controller.resendOTP();
                              },
                          )
                        ])),
                  ),
                ),
                appWidgets.gapH(40),
                AppButton(
                  backgroundColor: AppColor.plantPrimaryColor,
                  text: "যাচাই",
                  onPressed: controller.otpVerification,
                )
              ],
            ),
          );
        }),
      ),
    );
  }
}

import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:supply_safe_brolier_meat/app/core/state/global_state.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/otp_verification_model.dart';
import 'package:supply_safe_brolier_meat/app/modules/auth/login_screen/login_screen_controller.dart';

import '../../../core/constants/app_constants.dart';
import '../../../network/api_client.dart';
import '../../../network/api_url.dart';
import '../../../routes/app_pages.dart';

class OtpScreenController extends GetxController {
  final formKey = GlobalKey<FormState>();
  final loginController = Get.find<LoginScreenController>();
  TextEditingController phoneNumberController = TextEditingController(),
      otpController = TextEditingController();
  StreamController<ErrorAnimationType>? errorController;

  final disabled = false.obs, currentText = "".obs, timerValue = 300.obs;
  @override
  void onInit() {
    super.onInit();
    if (Get.arguments != null && Get.arguments["phone"] != "") {
      phoneNumberController.text = Get.arguments["phone"];
      logger.i("MyPhone: ${phoneNumberController.text}");
    }

    errorController = StreamController<ErrorAnimationType>();
    startTimer();
  }

  startTimer() {
    disabled.value = true;

    Timer.periodic(Duration(seconds: 1), (timer) {
      if (timerValue.value > 0) {
        timerValue.value--;
      } else {
        disabled.value = false;
        timer.cancel();
      }
    });
  }

  otpOnChanged(String value) {
    currentText.value = value;
  }

  resendOTP() {
    timerValue.value = 300;

    startTimer();
    loginController.sentOtp();
  }

  otpVerification() async {
    logger.i("Change Pass");
    var data = <String, dynamic>{};

    data["otp"] = otpController.text;
    data["phone"] = phoneNumberController.text;

    var response =
        await ApiClient().putAPI(otpVerificationUrl, data, otpVerification);

    if (response != null) {
      OtpVerificationModel responseModel =
          otpVerificationModelFromJson(response.toString());
      if (responseModel.responseStatus?.success ?? false) {
        appHelper.deleteRollId();
        if (responseModel.data?.roleId == 2) {
          print("Roll ID: ${responseModel.data?.roleId}");
          Get.offAllNamed(Routes.HOME);
          Get.find<GlobalState>().getUserData();
          appHelper.saveToken(responseModel.data?.token ?? "");
          appHelper.saveUserRoll(responseModel.data?.roleId);
          await appHelper.subscribeFirebaseTopic("farmer");
        } else if (responseModel.data?.roleId == 3) {
          print("Roll ID: ${responseModel.data?.roleId}");
          Get.offAllNamed(Routes.PLANT_MAIN_NAV);
          Get.find<GlobalState>().getUserData();
          appHelper.saveToken(responseModel.data?.token ?? "");
          appHelper.saveUserRoll(responseModel.data?.roleId);
          await appHelper.subscribeFirebaseTopic("plant");
        } else if (responseModel.data?.roleId == 4) {
          print("Roll ID: ${responseModel.data?.roleId}");
          Get.offAllNamed(Routes.OUTLET_MAIN_NAV);
          Get.find<GlobalState>().getUserData();
          appHelper.saveToken(responseModel.data?.token ?? "");
          appHelper.saveUserRoll(responseModel.data?.roleId);
          await appHelper.subscribeFirebaseTopic("outlet");
        }
        appWidgets.showSimpleToast(
          responseModel.responseStatus?.message,
          isSuccess: true,
        );
      } else {
        appWidgets.showSimpleToast(
          responseModel.responseStatus?.error,
          isError: true,
        );
      }
    }
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_constants.dart';
import 'package:supply_safe_brolier_meat/app/data/pojo/Response_model.dart';
import 'package:supply_safe_brolier_meat/app/routes/app_pages.dart';

import '../../../network/api_client.dart';
import '../../../network/api_url.dart';

class LoginScreenController extends GetxController {
  TextEditingController phoneNumberController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  @override
  void onInit() {
    super.onInit();
  }

  onSubmit() {
    if (formKey.currentState?.validate() ?? false) {
      sentOtp();
    }
  }

  sentOtp() async {
    var data = <String, dynamic>{};
    data["phone"] = phoneNumberController.text;

    var response = await ApiClient().postAPI(sendOtpUrl, data, sentOtp);

    if (response != null) {
      ResponseModel responseModel = responseModelFromJson(response.toString());

      if (responseModel.responseStatus?.success ?? false) {
        await appHelper.subscribeFirebaseTopic("all");
        appWidgets.showSimpleToast(responseModel.responseStatus?.message,
            isSuccess: true);
        Get.toNamed(Routes.OTP_SCREEN, arguments: {
          "phone": phoneNumberController.text,
        });
      } else {
        appWidgets.showSimpleToast(responseModel.responseStatus?.error,
            isError: true);
      }
    }
  }
}

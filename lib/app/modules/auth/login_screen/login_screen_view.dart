import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:soft_builder/widget/my_edit_text.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_colors.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_constants.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/asset_constants.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/my_text_style.dart';
import 'package:supply_safe_brolier_meat/app/core/widgets/app_button.dart';

import 'login_screen_controller.dart';

class LoginScreenView extends GetView<LoginScreenController> {
  const LoginScreenView({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.splashBackgroundColor,
      body: SingleChildScrollView(
        child: Form(
          key: controller.formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              gapH20,
              Lottie.asset(imageThree, height: 300.h),
              gapH20,
              Center(
                  child: Text(
                "আপনার মোবাইল নাম্বার প্রবেশ করুন",
                style: text20Style(),
              )),
              const Center(
                  child: Text(
                "আপনি একটি এসএমএস হিসাবে একটি ওয়ান টাইম পিন পাবেন",
                style: TextStyle(),
              )),
              gapH30,
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: MyEditText(
                  prefixWidget: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 8.w),
                    child: const Text("+88"),
                  ),
                  title: "",
                  hintText: "Enter Your Phone Number",
                  controller: controller.phoneNumberController,
                  isMobileNumber: true,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding: mainPaddingAll,
                child: AppButton(text: "লগইন", onPressed: controller.onSubmit),
              ),
              const SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

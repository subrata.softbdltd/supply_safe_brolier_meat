import 'dart:async';

import 'package:get/get.dart';
import 'package:soft_builder/constraints/my_constraints.dart';
import 'package:soft_builder/soft_builder.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_colors.dart';
import 'package:supply_safe_brolier_meat/app/core/utils/helper/app_helper.dart';
import 'package:supply_safe_brolier_meat/app/routes/app_pages.dart';

import '../../core/constants/app_constants.dart';

class SplashScreenController extends GetxController {
  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
    SoftBuilder().init(
      primaryColor: AppColor.primaryColor,
      doNeedScreenUtil: false,
      baseURL: '',
      isEditTextBorderPrimaryColor: true,
      isDropDownBorderPrimaryColor: true,
      isDropDownBackgroundTransparent: true,
      isEditTextBackgroundTransparent: true,
    );
    Timer(Duration(seconds: 2), () {
      getData();
    });
  }

  getData() async {
    await myHelper.getBoolPref(onBoard).then((value) {
      print('getIsShowOnboarding $value');
      if (value == true) {
        myHelper.getToken().then((token) async {
          if (token != null && token != "") {
            AppHelper().getRollId().then((value) {
              print("All Value: ${value}");
              if (value == "2") {
                Get.offAllNamed(Routes.HOME);
              } else if (value == "3") {
                Get.offAllNamed(Routes.PLANT_MAIN_NAV);
              } else if (value == "4") {
                Get.offAllNamed(Routes.OUTLET_MAIN_NAV);
              }
            });
          } else {
            Get.offAllNamed(Routes.LOGIN_SCREEN);
          }
        });
      } else {
        Get.offAndToNamed(Routes.ON_BOARD_SCREEN);
      }
    });
  }
}

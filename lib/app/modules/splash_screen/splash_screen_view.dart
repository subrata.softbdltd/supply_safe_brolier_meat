import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/app_colors.dart';
import 'package:supply_safe_brolier_meat/app/core/constants/asset_constants.dart';

import '../../core/constants/app_constants.dart';
import 'splash_screen_controller.dart';

class SplashScreenView extends GetView<SplashScreenController> {
  const SplashScreenView({super.key});
  @override
  Widget build(BuildContext context) {
    logger.i(controller.count);
    return Scaffold(
      backgroundColor: AppColor.splashBackgroundColor,
      body: Column(
        children: [
          const Spacer(),
          Image.asset(logo),
          const Spacer(),
        ],
      ),
    );
  }
}

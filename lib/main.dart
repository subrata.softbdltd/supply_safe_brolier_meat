import 'dart:async';
import 'dart:ui';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:soft_builder/widget/my_widgets.dart';
import 'package:supply_safe_brolier_meat/app/core/widgets/app_widgets.dart';

import 'app/core/binding/initial_binding.dart';
import 'app/core/constants/app_colors.dart';
import 'app/core/constants/app_constants.dart';
import 'app/core/constants/app_style.dart';
import 'app/routes/app_pages.dart';
import 'firebase_options.dart';

FirebaseMessaging messaging = FirebaseMessaging.instance;

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
}
late AndroidNotificationChannel channel;
late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
late StreamSubscription<InternetConnectionStatus>? listener;

Future<void> main()async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  FirebaseAnalytics analytics = FirebaseAnalytics.instance;
  FlutterError.onError = (errorDetails) {
    FirebaseCrashlytics.instance.recordFlutterFatalError(errorDetails);
  };

  PlatformDispatcher.instance.onError = (error, stack) {
    FirebaseCrashlytics.instance.recordError(error, stack, fatal: true);
    return true;
  };

  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  FirebaseMessaging.onMessageOpenedApp.listen(
        (message) {
      logger.d("FirebaseMessaging.onMessageOpenedApp.listen");
      if (message.notification != null) {
        logger.d(message.notification!.title);
        logger.d(message.notification!.body);
        logger.d("message.data22 ${message.data['_id']}");
      }
    },
  );

  if (!kIsWeb) {
    channel = const AndroidNotificationChannel(
      'SSDM',
      'High Importance Notifications',
      description: 'This channel is used for important notifications.',
      importance: Importance.max,
      enableLights: true,
      enableVibration: true,
    );

    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
        AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );
  }

  bool showOnline = false;
  listener = InternetConnectionChecker().onStatusChange.listen((status) {
    switch (status) {
      case InternetConnectionStatus.connected:
        if (showOnline) {
          Get.back();
          MyWidgets().showSimpleToast(
            "Back Online",
            title: "Connection Status",
            isSuccess: true,
          );
        }
        break;
      case InternetConnectionStatus.disconnected:
        showOnline = true;
        AppWidget().noInternetDialog();
        break;
    }
  });
  runApp(
    ScreenUtilInit(
      designSize: const Size(375, 812),
      minTextAdapt: true,
      useInheritedMediaQuery: true,
      builder: (BuildContext context, Widget? child) {
        return GetMaterialApp(
          title: "Supply Safe Broiler Meat",
          initialRoute: AppPages.INITIAL,
          initialBinding: InitialBinding(),
          getPages: AppPages.routes,
          debugShowCheckedModeBanner: false,
          builder: EasyLoading.init(),
          themeMode: ThemeMode.dark,
          locale: Get.locale,
          theme: ThemeData(
            useMaterial3: false,
            primarySwatch: AppColor.colorPrimarySwatch,
            primaryColor: AppColor.primaryColor,
            colorScheme: ColorScheme.fromSwatch(
                    primarySwatch: AppColor.colorPrimarySwatch)
                .copyWith(background: AppColor.white),
            appBarTheme: AppBarTheme(
              color: AppColor.white,
              centerTitle: true,
              titleTextStyle: textRegularStyle(
                fontSize: 20,
                color: AppColor.white,
              ),
              iconTheme: const IconThemeData(
                color: AppColor.white,
              ),
            ),
          ),
        );
      },
    ),
  );
}
